cmake_minimum_required (VERSION 3.1)
project(graduation_project)
option(build_unit_tests "Build the unit tests for the library" ON)
option(build_server "Build the server" ON)
INCLUDE_DIRECTORIES(src)
# Solve dependencies
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/dependencies.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/build_library.cmake)

if(build_server)
  include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/build_server.cmake)
endif(build_server)

if(build_unit_tests)
  include_directories(${depend_gtest_include_path})
  include_directories(${depend_gmock_include_path})
  include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/build_unit_tests.cmake)
endif(build_unit_tests)

