# Build library
# ============================================================================ #
include_directories(${depend_mysql_include_path})
include_directories(${depend_rapidxml_include_path})
include_directories(${depend_protobuf_include_path})


set(FILE_FILES
  src/graduation_project/file/interf/file.cpp
  src/graduation_project/file/interf/file.hpp
  src/graduation_project/file/mock/file.hpp
  src/graduation_project/file/fake/file.hpp
  src/graduation_project/file/fake/file.cpp
  src/graduation_project/file/real/file.hpp
  src/graduation_project/file/real/file.cpp

  src/graduation_project/settings/settings.hpp
  src/graduation_project/settings/settings.cpp

  src/graduation_project/database/mock/database_connection.hpp
  src/graduation_project/database/mock/result_set.hpp
  src/graduation_project/database/mock/database_system.hpp
  src/graduation_project/database/fake/result_set.cpp
  src/graduation_project/database/fake/result_set.hpp
  src/graduation_project/database/real/database_connection.cpp
  src/graduation_project/database/real/database_connection.hpp
  src/graduation_project/database/real/result_set.cpp
  src/graduation_project/database/real/result_set.hpp
  src/graduation_project/database/real/database_system.cpp
  src/graduation_project/database/real/database_system.hpp
  src/graduation_project/database/exception.hpp
  src/graduation_project/database/exception.cpp
  src/graduation_project/database/interf/database_system.cpp
  src/graduation_project/database/interf/database_system.hpp
  src/graduation_project/database/interf/database_connection.hpp
  src/graduation_project/database/interf/database_connection.cpp
  src/graduation_project/database/interf/result_set.hpp
  src/graduation_project/database/interf/result_set.cpp
  src/graduation_project/database/result/cell.hpp
  src/graduation_project/database/result/cell.cpp
  src/graduation_project/database/result/row.hpp
  src/graduation_project/database/result/row.cpp
  src/graduation_project/database/quick_connection.cpp
  src/graduation_project/database/quick_connection.hpp
  src/graduation_project/database/database_connection_info.cpp
  src/graduation_project/database/database_connection_info.hpp

  src/graduation_project/requests/message_processor.cpp
  src/graduation_project/requests/message_processor.hpp
  src/graduation_project/request_processor.cpp
  src/graduation_project/request_processor.hpp
  src/graduation_project/utils.hpp
  src/graduation_project/utils.cpp

  src/graduation_project/requests/request.cpp
  src/graduation_project/requests/request.hpp
  src/graduation_project/requests/protocols/xml.hpp
  src/graduation_project/requests/protocols/xml.cpp
  src/graduation_project/requests/protocols/protobuf.cpp
  src/graduation_project/requests/protocols/protobuf.hpp
  src/graduation_project/requests/authenticate/xml.cpp
  src/graduation_project/requests/authenticate/xml.hpp
  src/graduation_project/requests/authenticate/protobuf.cpp
  src/graduation_project/requests/authenticate/protobuf.hpp
  src/graduation_project/requests/authenticate/interf/authenticate.hpp
  src/graduation_project/requests/authenticate/interf/authenticate.cpp
  src/graduation_project/requests/authenticate/mock/authenticate.hpp

  src/graduation_project/responses/response.cpp
  src/graduation_project/responses/response.hpp
  src/graduation_project/responses/xml.hpp
  src/graduation_project/responses/xml.cpp
  src/graduation_project/responses/protobuf.hpp
  src/graduation_project/responses/protobuf.cpp

  src/graduation_project/proto/messages.pb.cc
  src/graduation_project/proto/messages.pb.h

  src/graduation_project/socket/address.cpp
  src/graduation_project/socket/address.hpp
  src/graduation_project/socket/socket.cpp
  src/graduation_project/socket/socket.hpp
  src/graduation_project/socket/socket_errors.cpp
  src/graduation_project/socket/socket_errors.hpp
  src/graduation_project/socket/socket_stream.cpp
  src/graduation_project/socket/socket_stream.hpp
  src/graduation_project/socket/socket_utils.cpp
  src/graduation_project/socket/socket_utils.hpp
  src/graduation_project/socket/atomic.h
  )
source_group(file FILES ${FILE_FILES})

add_library(graduation_project_library
  ${FILE_FILES}
)

if(TARGET mysql) # Wait if we resolve this
  add_dependencies(graduation_project_library mysql)
endif(TARGET mysql)

if(TARGET protobuf)
  add_dependencies(graduation_project_library protobuf)
endif(TARGET protobuf)

if(TARGET rapidxml) # Wait if we resolve this
  add_dependencies(graduation_project_library rapidxml)
endif(TARGET rapidxml)

