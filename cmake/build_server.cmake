# Build server executable
# ============================================================================ #
# Declare

add_executable(server
  src/graduation_project/server.hpp
  src/graduation_project/server.cpp
  src/graduation_project/main_server.cpp
)

if(TARGET graduation_project_library) # Wait if we resolve this
  add_dependencies(server graduation_project_library)
endif(TARGET graduation_project_library)

target_link_libraries(server
  graduation_project_library
  ${depend_protobuf_library_path}
  ${depend_mysql_library_path} # Required by cvs_query
  dl # Required by mysql library
  ${ep_pthread_library} # PThread required for gtest
)
