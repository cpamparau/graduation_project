# Build unit tests executable
# ============================================================================ #
# Declare

add_executable(unit_tests
  src/test/graduation_project/file/file_user.hpp
  src/test/graduation_project/file/file_user.cpp
  src/test/graduation_project/file/mock/file.cpp
  src/test/graduation_project/file/fake/file.cpp
  src/test/graduation_project/settings.cpp
  src/test/graduation_project/database/cell.cpp
  src/test/graduation_project/database/example_layer1.cpp
  src/test/graduation_project/database/example_layer2.cpp

  src/test/graduation_project/requests/authenticate/xml.cpp
  src/test/graduation_project/requests/authenticate/protobuf.cpp
  src/test/graduation_project/requests/message_processor.cpp
  src/test/main_unit_tests.cpp
)

if(TARGET gtest) # Wait if we resolve this
	add_dependencies(unit_tests gtest)
endif(TARGET gtest)

if(TARGET graduation_project_library) # Wait if we resolve this
	add_dependencies(unit_tests graduation_project_library)
endif(TARGET graduation_project_library)

target_link_libraries(unit_tests
  graduation_project_library
  ${depend_protobuf_library_path}
  ${depend_gtest_library_path}
  ${depend_gmock_library_path}
  ${depend_mysql_library_path} # Required by cvs_query
  dl # Required by mysql library
  ${ep_pthread_library} # PThread required for gtest
)
