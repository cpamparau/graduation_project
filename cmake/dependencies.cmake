# Author : Cristian Pamparau <cpamparau@stud.usv.ro>
# ============================================================================ #
# Input parameters:
# - external_projects_base_dir -required-working directory for external projects
#
# This script tries to get the git url (ssh or http[s]) used to clone this
# project. This is usefull for using the same authentication credentials when
# downloading other projects(dependencies) from the same git remote source.
#
# ============================================================================ #

message(STATUS "# =========================== DEPENDENCIES ===================="
  "=============== #")

set(dependencies_dir ${CMAKE_BINARY_DIR}/dependencies)
set(external_projects_base_dir ${dependencies_dir}/external_projects)
set_property(DIRECTORY PROPERTY "EP_BASE" ${external_projects_base_dir})

# Always included, library always built
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/lib/resolve_mysql.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/lib/resolve_rapidxml.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/lib/resolve_google_protobuf.cmake)

# =============================== GoogleTest ================================= #
if(build_unit_tests)
  include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/lib/resolve_google_test.cmake)
endif(build_unit_tests)
# ============================ END Google Test =============================== #

# ================================ PThread =================================== #
set( ep_pthread_library pthread )
# ============================== END PThread ================================= #

message(STATUS "# ========================= END DEPENDENCIES =================="
  "=============== #")
