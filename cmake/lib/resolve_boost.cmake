if((NOT DEFINED depend_boost_include_path) OR
   (NOT DEFINED depend_boost_system_library_path) OR
   (NOT DEFINED depend_boost_thread_library_path) OR
   (NOT DEFINED depend_boost_regex_library_path) OR
   (NOT DEFINED depend_boost_filesystem_library_path) 
   )

  message(STATUS "No boost dependency paths given. Will resolve dependency.")

  # Define download URL, MD5 verify hash and library file name
set(ep_boost_system_library_name "libboost_system.a")
set(ep_boost_thread_library_name "libboost_thread.a")
set(ep_boost_regex_library_name "libboost_regex.a")
set(ep_boost_filesystem_library_name "libboost_filesystem.a")

#list(APPEND boost_build_components system regex)
# Download, verify and extract library
include(ExternalProject) # Enable ExternalProject CMake module
ExternalProject_Add(boost
  URL   ${CMAKE_SOURCE_DIR}/dependencies/Source/boost
	#    URL_MD5            ${ep_boost_md5}
  BUILD_IN_SOURCE    OFF
  BINARY_DIR         ${external_projects_base_dir}/Source/boost
  INSTALL_DIR        ${external_projects_base_dir}/Install/boost
	CONFIGURE_COMMAND ./bootstrap.sh --with-libraries=system,thread,regex,filesystem --prefix=<INSTALL_DIR>
  BUILD_COMMAND     ./b2  threading=multi
	INSTALL_COMMAND ./b2 install
	LOG_CONFIGURE ON
	LOG_BUILD ON
	LOG_INSTALL ON
  )
  ExternalProject_Get_Property(boost install_dir) # Store value in binary_dir

  set(depend_boost_include_path ${install_dir}/include)
  set(depend_boost_system_library_path ${CMAKE_BINARY_DIR}/${install_dir}/lib/${ep_boost_system_library_name})  
  set(depend_boost_regex_library_path ${CMAKE_BINARY_DIR}/${install_dir}/lib/${ep_boost_regex_library_name})
  set(depend_boost_filesystem_library_path ${CMAKE_BINARY_DIR}/${install_dir}/lib/${ep_boost_filesystem_library_name})

  # Unset declared variables
  unset(ep_boost_url)
  unset(ep_boost_md5)
  unset(ep_boost_system_library_name)
  unset(ep_boost_regex_library_name)
  unset(ep_boost_filesystem_library_name)
  unset(binary_dir)

else((NOT DEFINED depend_boost_include_path) OR
  (NOT DEFINED depend_boost_system_library_path) OR
  (NOT DEFINED depend_boost_thread_library_path) OR
  (NOT DEFINED depend_boost_regex_library_path) OR
  (NOT DEFINED depend_boost_filesystem_library_path)
  )

  message(STATUS "boost dependency paths given. Will not resolve dependency")

endif((NOT DEFINED depend_boost_include_path) OR
   (NOT DEFINED depend_boost_system_library_path) OR
   (NOT DEFINED depend_boost_thread_library_path) OR
   (NOT DEFINED depend_boost_regex_library_path) OR
   (NOT DEFINED depend_boost_filesystem_library_path)
 )

# Print values that will be used
message("depend_boost_include_path: " ${depend_boost_include_path})
if(DEFINED depend_boost_system_library_path)
  message("depend_boost_system_library_path: " ${depend_boost_system_library_path})
endif(DEFINED depend_boost_system_library_path)
if(DEFINED depend_boost_thread_library_path)
  message("depend_boost_thread_library_path: " ${depend_boost_thread_library_path})
endif(DEFINED depend_boost_thread_library_path)
if(DEFINED depend_boost_regex_library_path)
  message("depend_boost_regex_library_path: " ${depend_boost_regex_library_path})
endif(DEFINED depend_boost_regex_library_path)
if(DEFINED depend_boost_filesystem_library_path)
  message("depend_boost_filesystem_library_path: " ${depend_boost_filesystem_library_path})
endif(DEFINED depend_boost_filesystem_library_path)


