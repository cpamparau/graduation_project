if((NOT DEFINED depend_protobuf_include_path) OR
  (NOT DEFINED depend_protobuf_library_path))

  message(STATUS "No protobuf dependency paths were given. "
    "Will download dependency.")

  set(ep_protobuf_protoc_binary protoc)
  set(ep_protobuf_library_name libprotobuf.a)

  # Include a certain version of ExternalProject CMake,a version that introduced
  # The SOURCE_SUBDIR option, required by protobuf. File downloaded from:
  # https://gitlab.kitware.com/matthew-woehlke/cmake/blob/
  # a8345d65f359d75efb057d22976cfb92b4d477cf/Modules/ExternalProject.cmake
  # This option is part of CMake 3.7
  include(${CMAKE_SOURCE_DIR}/cmake/lib/ExternalProject.cmake)
  ExternalProject_Add(protobuf
    URL               ${CMAKE_SOURCE_DIR}/dependencies/Source/protobuf
    SOURCE_DIR        ${external_projects_base_dir}/Source/protobuf
    BINARY_DIR        ${external_projects_base_dir}/Install/protobuf
    CMAKE_ARGS        -DCMAKE_BUILD_TYPE=Release
           -DCMAKE_INSTALL_PREFIX=${external_projects_base_dir}/Install/protobuf
                      -Dprotobuf_BUILD_TESTS=OFF
    SOURCE_SUBDIR     cmake # Defined in ./ExternalProject.cmake
    UPDATE_COMMAND    "" # Do not run update command
  )

  # Get external project source and binary directories
  ExternalProject_Get_Property(protobuf binary_dir)

  set(depend_protobuf_include_path ${binary_dir}/include)
  set(depend_protobuf_library_path
    ${binary_dir}/${ep_protobuf_library_name})

  # ===================== Compile with protocol compiler ===================== #
  message(STATUS "Using Google Protobuf compiler: "
    ${binary_dir}/${ep_protobuf_protoc_binary})

  # Generate CPP sources
  if(NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.cc OR
    NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.h)
    message(STATUS "Will generate C++ sources with protobuf - they don't exist")
    # Created empty at CMake run time (PRE_BUILD) - protoc does not exist yet
    # Needed because CMake checks if all source file exist (in build_server)
    file(WRITE ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.cc "")
    file(WRITE ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.h
      "#error \"Please generate CPP sources with protoc\"")
    # Populated with protoc, at make run time (POST_BUILD) - protoc was compiled
    add_custom_command(TARGET protobuf POST_BUILD
      COMMAND ${binary_dir}/${ep_protobuf_protoc_binary}
        -I=${CMAKE_SOURCE_DIR}/proto
        # will be generated in src/logging_server/proto
        --cpp_out=${CMAKE_SOURCE_DIR}/src/graduation_project/proto
        ${CMAKE_SOURCE_DIR}/proto/messages.proto
      DEPENDS ${CMAKE_SOURCE_DIR}/proto/messages.proto
      COMMENT "Generate C++ sources from proto file"
    )
  else(NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.cc OR
    NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.h)
    file(READ ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.cc
      cpp_cc 10)
    file(READ ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.h cpp_h
      10)
    if(cpp_cc STREQUAL "" OR cpp_h STREQUAL "")
      message(STATUS "C++ sources are empty, will generate them.")
      # Populated with protoc, at make run time (POST_BUILD)-protoc was compiled
      add_custom_command(TARGET protobuf POST_BUILD
        COMMAND ${binary_dir}/${ep_protobuf_protoc_binary}
          -I=${CMAKE_SOURCE_DIR}/proto
          # will be generated in src/logging_server/proto
          --cpp_out=${CMAKE_SOURCE_DIR}/src/graduation_project/proto
          ${CMAKE_SOURCE_DIR}/proto/messages.proto
      )
    else(cpp_cc STREQUAL "" OR cpp_h STREQUAL "")
      message(STATUS "C++ sources are not empty, will not generate them.")
    endif(cpp_cc STREQUAL "" OR cpp_h STREQUAL "")
  endif(NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.cc
    OR NOT EXISTS ${CMAKE_SOURCE_DIR}/src/graduation_project/proto/messages.pb.h)

  # Unset used variables
  unset(binary_dir)
  unset(ep_protobuf_protoc_binary)
  unset(ep_protobuf_library_name)

else((NOT DEFINED depend_protobuf_include_path) OR
  (NOT DEFINED depend_protobuf_library_path))

  message(STATUS "Protobuf dependency paths are set. Will not resolve "
    "dependency")

endif((NOT DEFINED depend_protobuf_include_path) OR
  (NOT DEFINED depend_protobuf_library_path))

# Print values that will be used
message("depend_protobuf_include_path: ${depend_protobuf_include_path}")
message("depend_protobuf_library_path: ${depend_protobuf_library_path}")
