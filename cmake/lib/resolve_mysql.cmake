# Author : Cristian Pamparau <cpamparau@stud.usv.ro>
# ============================================================================ #
#
# Resolve: MySQL C Connector Library

  set(depend_mysql_include_path
    ${CMAKE_SOURCE_DIR}/dependencies/Source/mysql/include)
  set(depend_mysql_library_path
    ${CMAKE_SOURCE_DIR}/dependencies/Source/mysql/lib/libmysqlclient.a)

# Print values that will be used
message("depend_mysql_include_path: " ${depend_mysql_include_path})
if(DEFINED depend_mysql_library_path)
  message("depend_mysql_library_path: " ${depend_mysql_library_path})
endif(DEFINED depend_mysql_library_path)
