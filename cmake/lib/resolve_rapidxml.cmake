# Resolve: Rapidxml library
#
# Note: Rapidxml is a header only library

if(NOT DEFINED depend_rapidxml_include_path)

  message(STATUS
    "No rapidxml dependency paths given. Will resolve dependency.")

  # Download, verify and extract library
  include(ExternalProject) # Enable ExternalProject CMake module
  ExternalProject_Add(rapidxml
    URL    ${CMAKE_SOURCE_DIR}/dependencies/Source/rapidxml
    BUILD_IN_SOURCE    ON
    SOURCE_DIR         ${external_projects_base_dir}/Install/rapidxml
    CMAKE_ARGS         ""
    BUILD_COMMAND      ""
    UPDATE_COMMAND     ""
    CONFIGURE_COMMAND  ""
    INSTALL_COMMAND    ""
    LOG_DOWNLOAD       ON

  )
  # Store value in binary_dir
  ExternalProject_Get_Property(rapidxml source_dir)
  set(depend_rapidxml_include_path ${source_dir}/src)

else(NOT DEFINED depend_rapidxml_include_path)

  message(STATUS
    "rapidxml dependency paths given. Will not resolve dependency")

endif(NOT DEFINED depend_rapidxml_include_path)

# Print values that will be used
message("depend_rapidxml_include_path: " ${depend_rapidxml_include_path})

