if(NOT DEFINED depend_websocketpp_include_path)

  message(STATUS
    "No websocketpp dependency paths given. Will resolve dependency.")


  # Download, verify and extract library
  include(ExternalProject) # Enable ExternalProject CMake module
  ExternalProject_Add(websocketpp
    URL                "${CMAKE_SOURCE_DIR}/dependencies/Source/websocketpp"
    BUILD_IN_SOURCE    ON
    SOURCE_DIR         ${external_projects_base_dir}/Install/websocketpp
    CMAKE_ARGS         ""
    BUILD_COMMAND      ""
    UPDATE_COMMAND     ""
    CONFIGURE_COMMAND  ""
    INSTALL_COMMAND    ""
    LOG_DOWNLOAD       ON

  )
  # Store value in binary_dir
  ExternalProject_Get_Property(websocketpp source_dir)
  set(depend_websocketpp_include_path ${source_dir})
  if(TARGET z) #Wait to resolve
    add_dependencies(websocketpp z)
  endif(TARGET z)

  # Unset declared variables
  unset(binary_dir)

else(NOT DEFINED depend_websocketpp_include_path)

  message(STATUS
    "websocketpp dependency paths given. Will not resolve dependency")

endif(NOT DEFINED depend_websocketpp_include_path)

# Print values that will be used
message("depend_websocketpp_include_path: " ${depend_websocketpp_include_path})
