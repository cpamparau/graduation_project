var annotated_dup =
[
    [ "graduation_project", null, [
      [ "database", null, [
        [ "fake", null, [
          [ "result_set", "classgraduation__project_1_1database_1_1fake_1_1result__set.html", "classgraduation__project_1_1database_1_1fake_1_1result__set" ]
        ] ],
        [ "interf", null, [
          [ "database_connection", "classgraduation__project_1_1database_1_1interf_1_1database__connection.html", "classgraduation__project_1_1database_1_1interf_1_1database__connection" ],
          [ "database_system", "classgraduation__project_1_1database_1_1interf_1_1database__system.html", "classgraduation__project_1_1database_1_1interf_1_1database__system" ],
          [ "result_set", "classgraduation__project_1_1database_1_1interf_1_1result__set.html", "classgraduation__project_1_1database_1_1interf_1_1result__set" ]
        ] ],
        [ "mock", null, [
          [ "database_connection", "classgraduation__project_1_1database_1_1mock_1_1database__connection.html", "classgraduation__project_1_1database_1_1mock_1_1database__connection" ],
          [ "database_system", "classgraduation__project_1_1database_1_1mock_1_1database__system.html", "classgraduation__project_1_1database_1_1mock_1_1database__system" ],
          [ "result_set", "classgraduation__project_1_1database_1_1mock_1_1result__set.html", "classgraduation__project_1_1database_1_1mock_1_1result__set" ]
        ] ],
        [ "real", null, [
          [ "database_connection", "classgraduation__project_1_1database_1_1real_1_1database__connection.html", "classgraduation__project_1_1database_1_1real_1_1database__connection" ],
          [ "mysql_system", "classgraduation__project_1_1database_1_1real_1_1mysql__system.html", "classgraduation__project_1_1database_1_1real_1_1mysql__system" ],
          [ "result_set", "classgraduation__project_1_1database_1_1real_1_1result__set.html", "classgraduation__project_1_1database_1_1real_1_1result__set" ]
        ] ],
        [ "result", "namespacegraduation__project_1_1database_1_1result.html", "namespacegraduation__project_1_1database_1_1result" ],
        [ "database_connection_info", "classgraduation__project_1_1database_1_1database__connection__info.html", "classgraduation__project_1_1database_1_1database__connection__info" ],
        [ "exception", "classgraduation__project_1_1database_1_1exception.html", "classgraduation__project_1_1database_1_1exception" ],
        [ "quick_connection", "classgraduation__project_1_1database_1_1quick__connection.html", "classgraduation__project_1_1database_1_1quick__connection" ]
      ] ],
      [ "file", null, [
        [ "fake", null, [
          [ "file", "classgraduation__project_1_1file_1_1fake_1_1file.html", "classgraduation__project_1_1file_1_1fake_1_1file" ]
        ] ],
        [ "interf", null, [
          [ "file", "classgraduation__project_1_1file_1_1interf_1_1file.html", "classgraduation__project_1_1file_1_1interf_1_1file" ]
        ] ],
        [ "mock", null, [
          [ "file", "classgraduation__project_1_1file_1_1mock_1_1file.html", "classgraduation__project_1_1file_1_1mock_1_1file" ]
        ] ],
        [ "real", null, [
          [ "file", "classgraduation__project_1_1file_1_1real_1_1file.html", "classgraduation__project_1_1file_1_1real_1_1file" ]
        ] ]
      ] ],
      [ "proto", null, [
        [ "authenticate_request", "classgraduation__project_1_1proto_1_1authenticate__request.html", "classgraduation__project_1_1proto_1_1authenticate__request" ],
        [ "request_msg", "classgraduation__project_1_1proto_1_1request__msg.html", "classgraduation__project_1_1proto_1_1request__msg" ],
        [ "request_response_msg", "classgraduation__project_1_1proto_1_1request__response__msg.html", "classgraduation__project_1_1proto_1_1request__response__msg" ],
        [ "response_msg", "classgraduation__project_1_1proto_1_1response__msg.html", "classgraduation__project_1_1proto_1_1response__msg" ],
        [ "response_with_message", "classgraduation__project_1_1proto_1_1response__with__message.html", "classgraduation__project_1_1proto_1_1response__with__message" ]
      ] ],
      [ "requests", null, [
        [ "authenticate", null, [
          [ "interf", null, [
            [ "authenticate", "classgraduation__project_1_1requests_1_1authenticate_1_1interf_1_1authenticate.html", "classgraduation__project_1_1requests_1_1authenticate_1_1interf_1_1authenticate" ]
          ] ],
          [ "mock", null, [
            [ "authenticate", "classgraduation__project_1_1requests_1_1authenticate_1_1mock_1_1authenticate.html", "classgraduation__project_1_1requests_1_1authenticate_1_1mock_1_1authenticate" ]
          ] ],
          [ "protobuf", "classgraduation__project_1_1requests_1_1authenticate_1_1protobuf.html", "classgraduation__project_1_1requests_1_1authenticate_1_1protobuf" ],
          [ "xml", "classgraduation__project_1_1requests_1_1authenticate_1_1xml.html", "classgraduation__project_1_1requests_1_1authenticate_1_1xml" ]
        ] ],
        [ "protocols", null, [
          [ "protobuf", "classgraduation__project_1_1requests_1_1protocols_1_1protobuf.html", "classgraduation__project_1_1requests_1_1protocols_1_1protobuf" ],
          [ "xml", "classgraduation__project_1_1requests_1_1protocols_1_1xml.html", "classgraduation__project_1_1requests_1_1protocols_1_1xml" ]
        ] ],
        [ "message_processor", "classgraduation__project_1_1requests_1_1message__processor.html", "classgraduation__project_1_1requests_1_1message__processor" ],
        [ "request", "classgraduation__project_1_1requests_1_1request.html", "classgraduation__project_1_1requests_1_1request" ]
      ] ],
      [ "responses", null, [
        [ "protobuf", "classgraduation__project_1_1responses_1_1protobuf.html", "classgraduation__project_1_1responses_1_1protobuf" ],
        [ "response", "classgraduation__project_1_1responses_1_1response.html", "classgraduation__project_1_1responses_1_1response" ],
        [ "xml", "classgraduation__project_1_1responses_1_1xml.html", "classgraduation__project_1_1responses_1_1xml" ]
      ] ],
      [ "socket", null, [
        [ "CVSBuffer", "classgraduation__project_1_1socket_1_1CVSBuffer.html", "classgraduation__project_1_1socket_1_1CVSBuffer" ],
        [ "HandleProxy", "classgraduation__project_1_1socket_1_1HandleProxy.html", "classgraduation__project_1_1socket_1_1HandleProxy" ],
        [ "InetHost", "classgraduation__project_1_1socket_1_1InetHost.html", "classgraduation__project_1_1socket_1_1InetHost" ],
        [ "InetPort", "classgraduation__project_1_1socket_1_1InetPort.html", "classgraduation__project_1_1socket_1_1InetPort" ],
        [ "InetSocketAddress", "classgraduation__project_1_1socket_1_1InetSocketAddress.html", "classgraduation__project_1_1socket_1_1InetSocketAddress" ],
        [ "InetSocketType", "classgraduation__project_1_1socket_1_1InetSocketType.html", "classgraduation__project_1_1socket_1_1InetSocketType" ],
        [ "NetError", "classgraduation__project_1_1socket_1_1NetError.html", "classgraduation__project_1_1socket_1_1NetError" ],
        [ "RefCounter", "classgraduation__project_1_1socket_1_1RefCounter.html", "classgraduation__project_1_1socket_1_1RefCounter" ],
        [ "RefPtrProxy", "classgraduation__project_1_1socket_1_1RefPtrProxy.html", "classgraduation__project_1_1socket_1_1RefPtrProxy" ],
        [ "ServiceNotAvailable", "classgraduation__project_1_1socket_1_1ServiceNotAvailable.html", "classgraduation__project_1_1socket_1_1ServiceNotAvailable" ],
        [ "Socket", "classgraduation__project_1_1socket_1_1Socket.html", "classgraduation__project_1_1socket_1_1Socket" ],
        [ "SocketError", "classgraduation__project_1_1socket_1_1SocketError.html", "classgraduation__project_1_1socket_1_1SocketError" ],
        [ "SocketInputOutputStream", "classgraduation__project_1_1socket_1_1SocketInputOutputStream.html", "classgraduation__project_1_1socket_1_1SocketInputOutputStream" ],
        [ "SocketInputStream", "classgraduation__project_1_1socket_1_1SocketInputStream.html", "classgraduation__project_1_1socket_1_1SocketInputStream" ],
        [ "SocketOutputStream", "classgraduation__project_1_1socket_1_1SocketOutputStream.html", "classgraduation__project_1_1socket_1_1SocketOutputStream" ],
        [ "SocketStreamBuffer", "classgraduation__project_1_1socket_1_1SocketStreamBuffer.html", "classgraduation__project_1_1socket_1_1SocketStreamBuffer" ],
        [ "SocketType", "classgraduation__project_1_1socket_1_1SocketType.html", "classgraduation__project_1_1socket_1_1SocketType" ],
        [ "UnknownHost", "classgraduation__project_1_1socket_1_1UnknownHost.html", "classgraduation__project_1_1socket_1_1UnknownHost" ],
        [ "UnknownSocketFamily", "classgraduation__project_1_1socket_1_1UnknownSocketFamily.html", "classgraduation__project_1_1socket_1_1UnknownSocketFamily" ],
        [ "UnknownSocketType", "classgraduation__project_1_1socket_1_1UnknownSocketType.html", "classgraduation__project_1_1socket_1_1UnknownSocketType" ],
        [ "WinSockInitFailed", "classgraduation__project_1_1socket_1_1WinSockInitFailed.html", null ]
      ] ],
      [ "connection", "structgraduation__project_1_1connection.html", "structgraduation__project_1_1connection" ],
      [ "request_processor", "classgraduation__project_1_1request__processor.html", "classgraduation__project_1_1request__processor" ],
      [ "server", "classgraduation__project_1_1server.html", "classgraduation__project_1_1server" ],
      [ "settings", "classgraduation__project_1_1settings.html", "classgraduation__project_1_1settings" ]
    ] ],
    [ "test", null, [
      [ "graduation_project", null, [
        [ "database", null, [
          [ "example", "namespacetest_1_1graduation__project_1_1database_1_1example.html", "namespacetest_1_1graduation__project_1_1database_1_1example" ]
        ] ],
        [ "file", null, [
          [ "file_user", "classtest_1_1graduation__project_1_1file_1_1file__user.html", "classtest_1_1graduation__project_1_1file_1_1file__user" ]
        ] ]
      ] ]
    ] ],
    [ "atomic_t", "structatomic__t.html", "structatomic__t" ],
    [ "exception", "classexception.html", null ],
    [ "ios", "classios.html", null ],
    [ "istream", "classistream.html", null ],
    [ "ostream", "classostream.html", null ],
    [ "streambuf", "classstreambuf.html", null ]
];