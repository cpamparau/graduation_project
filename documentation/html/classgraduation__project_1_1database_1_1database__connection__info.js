var classgraduation__project_1_1database_1_1database__connection__info =
[
    [ "secure_auth_type", "classgraduation__project_1_1database_1_1database__connection__info.html#ae1e439b2e6634a5c50037894333faa1c", [
      [ "SECURE_AUTH_OFF", "classgraduation__project_1_1database_1_1database__connection__info.html#ae1e439b2e6634a5c50037894333faa1ca3543d9de3392fde0c712f2012b6b58ea", null ],
      [ "SECURE_AUTH_ON", "classgraduation__project_1_1database_1_1database__connection__info.html#ae1e439b2e6634a5c50037894333faa1cae3462783eb6ca5c6c9d9d55092673d91", null ],
      [ "SECURE_AUTH_SYSTEM_DEFAULT", "classgraduation__project_1_1database_1_1database__connection__info.html#ae1e439b2e6634a5c50037894333faa1caae6c0f32250e10fd9d068b2a98d25723", null ]
    ] ],
    [ "database_connection_info", "classgraduation__project_1_1database_1_1database__connection__info.html#ac2d5897a188ebfcc84048e01a27c4b81", null ],
    [ "set_client_compress", "classgraduation__project_1_1database_1_1database__connection__info.html#a2cb8e6775cbe02d1ff22409497e325e8", null ],
    [ "client_flags", "classgraduation__project_1_1database_1_1database__connection__info.html#a20cb4f558d06a0d843b97716ed70d3d3", null ],
    [ "db", "classgraduation__project_1_1database_1_1database__connection__info.html#af4f2c53bf0d95e43a3542bf225b6eab5", null ],
    [ "host", "classgraduation__project_1_1database_1_1database__connection__info.html#a45c5e3982bf372228e8169cbf442d3d8", null ],
    [ "passwd", "classgraduation__project_1_1database_1_1database__connection__info.html#a0858bb771bc98c3d49f265e407ea81d9", null ],
    [ "port", "classgraduation__project_1_1database_1_1database__connection__info.html#a1add3e66e8f71f96f2440c8d42010bfd", null ],
    [ "secure_auth", "classgraduation__project_1_1database_1_1database__connection__info.html#ae097f5c6208c834496a6e26516647034", null ],
    [ "should_reconnect", "classgraduation__project_1_1database_1_1database__connection__info.html#aefb098f901d5b7274f584b943e4cbb87", null ],
    [ "timeout_seconds", "classgraduation__project_1_1database_1_1database__connection__info.html#adb2ef834629001b702f904ac3673fe5a", null ],
    [ "unix_socket", "classgraduation__project_1_1database_1_1database__connection__info.html#af551fc489da9727a3071b7dc95494173", null ],
    [ "user", "classgraduation__project_1_1database_1_1database__connection__info.html#a5ac64903a7f41d74e9739fc7d5d91bc8", null ]
];