var classgraduation__project_1_1database_1_1exception =
[
    [ "error_code", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaed", [
      [ "UNKNOWN_ERROR", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaedaef5a9938fa47b0cbdf52901770879a7a", null ],
      [ "CONNECTION_FAILED", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaeda32541a88011b61e62b8c8a87d8c1ea0d", null ],
      [ "SECURE_AUTH", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaedac57aca36c8c1fabf6048e1cf87cee1aa", null ],
      [ "RECONNECT", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaeda1a91e41364b9419032f60b8014012c59", null ],
      [ "CONNECT_TIMEOUT", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaeda01f74a3f21db3aba2fe57046c558467b", null ],
      [ "READ_TIMEOUT", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaedaba00af240a3a8594bf4bb2dd559ffd24", null ],
      [ "WRITE_TIMEOUT", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaedaad0903ebc95f49aaf77534e0c4ea8254", null ],
      [ "STORE_FAILED", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaedaaa0efa186b68d5d1b0173bb80d81672a", null ],
      [ "QUERY_FAILED", "classgraduation__project_1_1database_1_1exception.html#aec3d666b8486f27daffa275b1c3bbaeda192797e632b9cdb573646b4bfeaeb5eb", null ]
    ] ],
    [ "exception", "classgraduation__project_1_1database_1_1exception.html#a62423db00809d653c35b8b4333ab4655", null ],
    [ "error_number", "classgraduation__project_1_1database_1_1exception.html#a307382b9cb37be6b0dce0af846b18140", null ],
    [ "mysql_error_code", "classgraduation__project_1_1database_1_1exception.html#ad5aa4c7825a104fd35647aca7a2cc139", null ]
];