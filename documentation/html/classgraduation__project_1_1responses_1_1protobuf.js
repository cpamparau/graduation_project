var classgraduation__project_1_1responses_1_1protobuf =
[
    [ "~protobuf", "classgraduation__project_1_1responses_1_1protobuf.html#a330051270b2b8ca114d09e1efade9ce6", null ],
    [ "protobuf", "classgraduation__project_1_1responses_1_1protobuf.html#adabcc5f2471a290bc3f6dbcb16ddd78e", null ],
    [ "get_id", "classgraduation__project_1_1responses_1_1protobuf.html#a143791fcedc014c9e1ec68cc33c59d72", null ],
    [ "get_message", "classgraduation__project_1_1responses_1_1protobuf.html#aabcc853e9297e56be33d40d208ff04d9", null ],
    [ "get_status", "classgraduation__project_1_1responses_1_1protobuf.html#a37a289922225705ffd7973e7f3e4b267", null ],
    [ "serialize", "classgraduation__project_1_1responses_1_1protobuf.html#abd7a8f7ab479e050ea9db6163ecbbda6", null ],
    [ "set_id", "classgraduation__project_1_1responses_1_1protobuf.html#a34647e5fd67228402c39935d0695d118", null ],
    [ "set_message", "classgraduation__project_1_1responses_1_1protobuf.html#aaa6994e3ee5130fc3476fd70097f8c09", null ],
    [ "set_status", "classgraduation__project_1_1responses_1_1protobuf.html#a877385f37d715f01499278c7c411983c", null ],
    [ "to_human_readable", "classgraduation__project_1_1responses_1_1protobuf.html#a7befe5ba0809763c796a2fca68270a8e", null ],
    [ "response_msg", "classgraduation__project_1_1responses_1_1protobuf.html#afa85cd3e5f12051316243e13228688ee", null ]
];