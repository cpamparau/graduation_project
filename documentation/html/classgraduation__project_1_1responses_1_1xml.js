var classgraduation__project_1_1responses_1_1xml =
[
    [ "xml", "classgraduation__project_1_1responses_1_1xml.html#a04b545f0cb9ae614d96327bca1593868", null ],
    [ "~xml", "classgraduation__project_1_1responses_1_1xml.html#a7c2e9d38491ad2f69a6c0b188fe878ca", null ],
    [ "get_id", "classgraduation__project_1_1responses_1_1xml.html#a435214040b6ea4cde326af95c8193387", null ],
    [ "get_message", "classgraduation__project_1_1responses_1_1xml.html#a21f1062a49b7c629a97bd9297e14aea8", null ],
    [ "get_status", "classgraduation__project_1_1responses_1_1xml.html#a3779c81112e4e135935a14842445792a", null ],
    [ "serialize", "classgraduation__project_1_1responses_1_1xml.html#a2295bb18285d69e294a555a428f10948", null ],
    [ "set_id", "classgraduation__project_1_1responses_1_1xml.html#afee07b408e46e3d2765d2b6c90c36620", null ],
    [ "set_message", "classgraduation__project_1_1responses_1_1xml.html#a75b064a523c4d87ca9f59cebab623197", null ],
    [ "set_status", "classgraduation__project_1_1responses_1_1xml.html#a950c18aaa8e7c3d32302d0c1baf0d9ee", null ],
    [ "to_human_readable", "classgraduation__project_1_1responses_1_1xml.html#a83e121014505b4e47b8de968e397b615", null ],
    [ "message", "classgraduation__project_1_1responses_1_1xml.html#a92f773957caeee073ddd87b84dcc1efa", null ],
    [ "request_id", "classgraduation__project_1_1responses_1_1xml.html#a8816e6134fbfcd6b5f0b03c410c57dbf", null ],
    [ "status", "classgraduation__project_1_1responses_1_1xml.html#a1433c4f3a3f5afedfa336407dd4f3fff", null ],
    [ "xml_doc", "classgraduation__project_1_1responses_1_1xml.html#acfd5c60291d29b7a36c7629aa8a1938d", null ]
];