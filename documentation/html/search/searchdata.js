var indexSectionsWithContent =
{
  0: "_acdefghilmnopqrstuvwx~",
  1: "_acdefhimnopqrsuwx",
  2: "gt",
  3: "acdefghilmnopqrstwx~",
  4: "cdefhilmoprstuvwx",
  5: "dr",
  6: "es",
  7: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Friends"
};

