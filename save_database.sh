#!/bin/sh

mysqldump -uroot -plicenta --max_allowed_packet=1G --default_character_set=utf8\
    --single_transaction=TRUE --skip-extended-insert\
    --databases "graduation_project" > graduation_project.sql
