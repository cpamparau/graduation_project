#include <mysql.h>
#include "graduation_project/database/database_connection_info.hpp"

namespace graduation_project
{
namespace database
{

//----------------------------------------------------------------------------//
database_connection_info::database_connection_info()
{
  host = "";
  user = "";
  passwd = "";
  db = "";
  unix_socket = "";
  port = 0;
  timeout_seconds = 0;
  should_reconnect = true;
  secure_auth = SECURE_AUTH_SYSTEM_DEFAULT;
  client_flags = 0;
}
//----------------------------------------------------------------------------//
void database_connection_info::set_client_compress(bool compressed_connection)
{
  if (compressed_connection)
  {
    client_flags |= CLIENT_COMPRESS;
  }
  else
  {
    client_flags &= ~(static_cast< unsigned long >(CLIENT_COMPRESS));
  }
}
//----------------------------------------------------------------------------//
}// namespace database
} // namespace graduation_project
