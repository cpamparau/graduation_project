#ifndef GRADUATION_PROJECT_DATABASE_DATABASE_CONNECTION_INFO
#define GRADUATION_PROJECT_DATABASE_DATABASE_CONNECTION_INFO

#include <string>

namespace graduation_project
{
namespace database
{

/**
 * @brief The database_connection_info class stores all dates we need to make
 * a database connection
 */
class database_connection_info
{
public:
  /**
   * @brief mysql_connection_info - Default constructor
   */
  database_connection_info();

  /**
   * @brief The secure_auth_type enum
   */
  enum secure_auth_type
  {
    SECURE_AUTH_OFF = 0, SECURE_AUTH_ON = 1, SECURE_AUTH_SYSTEM_DEFAULT = 2
  };

  /**
   * @brief host - The value of host may be either a host name or an IP address.
   */
  ::std::string host;

  /**
   * @brief user - Contains the user's MySQL login ID. If user is the empty
   * string "", the current user is assumed.
   */
  ::std::string user;

  /**
   * @brief passwd - Contains the password for user.
   */
  ::std::string passwd;

  /**
   * @brief db - Contains the database name.
   */
  ::std::string db;

  /**
   * @brief port - If not 0, the value is used as the port number for the TCP/IP
   * connection. Note that the host parameter determines the type of the
   * connection.
   */
  unsigned int port;

  /**
   * @brief unix_socket - The string specifies the socket or named pipe to
   * use.
   * @note The host parameter determines the type of the connection.
   * @note If this is set to empty string, nullptr is passed to layer1.
   */
  std::string unix_socket;

  /**
   * @brief client_flags - the flags allows various connection options
   * to be set. The value of client_flag is usually 0, but can be set to
   * a combination of the following flags to enable certain features.
   */
  unsigned long client_flags;

  /**
   * @brief timeout_seconds !=0 it will be used to set the following options:
   * MYSQL_OPT_CONNECT_TIMEOUT
   * MYSQL_OPT_READ_TIMEOUT
   * MYSQL_OPT_WRITE_TIMEOUT
   */
  unsigned int timeout_seconds;

  /**
   * @brief should_reconnect The MySQL client library can perform an automatic
   * reconnection to the server if it finds that the connection is down when
   * you attempt to send a statement to the server to be executed. If
   * should_reconnect is enabled, the library tries once to reconnect to the
   * server and send the statement again.
   *
   * @note set to true if automatic reconnect
   */
  bool should_reconnect;

  /**
   * @brief secure_auth This prevents connections except for servers that use
   * the newer password format. Before MySQL 5.7.5, this option is enabled
   * by default but can be disabled.
   * When secure_auth==SECURE_AUTH_ON then set MYSQL_SECURE_AUTH option to true
   * When secure_auth==SECURE_AUTH_OFF then set MYSQL_SECURE_AUTH
   * option to false
   * When secure_auth==SECURE_AUTH_SYSTEM_DEFAULT then do not set
   * MYSQL_SECURE_AUTH option (it will be set to system default)
   */
  secure_auth_type secure_auth;

  /**
   * @brief set_client_compress Use the compressed client/server protocol.
   * When compressed_connection == true then set the client_flag parameter
   * of the mysql_real_connect call to CLIENT_COMPRESS.
   * When compressed_connection == false then set the client_flag parameter
   * of the mysql_real_connect api call to zero.
   * @param[in] compressed_connection - Flag set to true if you want to compress
   * the connection traffic, and false otherwise.
   */
  void set_client_compress(bool compressed_connection);
};
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_DATABASE_CONNECTION_INFO
