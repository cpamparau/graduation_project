#include "graduation_project/database/exception.hpp"

namespace graduation_project
{
namespace database
{
//----------------------------------------------------------------------------//
exception::exception(const ::std::string &message, const error_code &err_no,
  const int mysql_error_code) :
  runtime_error(message), error_number(err_no), mysql_error_code(
    mysql_error_code)
{
}
//----------------------------------------------------------------------------//
} // namespace database
} // namespace graduation_project
