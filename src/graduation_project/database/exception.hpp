#ifndef GRADUATION_PROJECT_DATABASE_EXCEPTiION_HPP
#define GRADUATION_PROJECT_DATABASE_EXCEPTiION_HPP

#include <string>
#include <stdexcept>

namespace graduation_project
{
namespace database
{
/**
 * @brief The exception class used to handle exceptional cases in real
 * implementation
 */
class exception: public ::std::runtime_error
{
public:

  /**
   * @brief The error_code enum used to handle different exceptions
   */
  enum error_code
  {
    UNKNOWN_ERROR = 0,
    CONNECTION_FAILED = 1,
    SECURE_AUTH = 2,
    RECONNECT = 3,
    CONNECT_TIMEOUT = 4,
    READ_TIMEOUT = 5,
    WRITE_TIMEOUT = 6,
    STORE_FAILED = 7,
    QUERY_FAILED = 8
  };

  /**
   * @brief Constructor that will handle different exception, depending on the
   * given message and the value of error_code, which will be sets by err_no
   * param
   * @param[in] message - The message that describes a situation for which an
   * exception it will be thrown.
   * @param[in] err_no - The value for error_code.
   * @param[in] mysql_error_code - The original mysql error code returned by
   * a call to the mysql library that triggered the current exception
   */
  exception(const ::std::string &message, const error_code &err_no =
    UNKNOWN_ERROR, const int mysql_error_code = 0);

  /**
   * @brief An error code value returned by different functions, used to handle
   * different exceptions. The value will be set in the constructor
   *
   * @note
   *  0 - Unknown error
   *  1 - The connection with database failed
   *  2 - The set of MYSQL_SECURE_AUTH option failed
   *  3 - The set of MYSQL_OPT_RECONNECT option failed
   *  4 - The set of MYSQL_OPT_CONNECT_TIMEOUT option failed
   *  5 - The set of MYSQL_OPT_READ_TIMEOUT option failed
   *  6 - The set of MYSQL_OPT_WRITE_TIMEOUT option failed
   *  7 - A store_result failed
   *  8 - A query failed
   */
  error_code error_number
  { error_code::UNKNOWN_ERROR };

  /**
   * @brief If this exception was thrown due to a failed api call to the mysql
   * library, the mysql_error_code field contains the original mysql error code
   * returned by the failed API call
   */
  int mysql_error_code
  { 0 };
};
} // namespace database
} // namespace graduation_project

#endif // GRADUATION_PROJECT_DATABASE_EXCEPTiION_HPP
