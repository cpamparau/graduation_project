#include <cstring> // strlen
#include "graduation_project/database/fake/result_set.hpp"

namespace graduation_project
{
namespace database
{
namespace fake
{
//----------------------------------------------------------------------------//
void result_set::initialize(
  ::std::vector< ::std::vector< const char* > > v)
{
  size_t rows = v.size();
  size_t columns = ULLONG_MAX;
  size_t row_size = 0;
  for (size_t i = 0; i < rows; i++)
  {
    row_size = v[i].size();
    if (row_size < columns)
    {
      columns = row_size;
    }
  }

  // allocate char*** for data
  const char*** table = new const char**[rows];
  for (size_t i = 0; i < rows; i++)
  {
    table[i] = new const char*[columns]();
    for (size_t j = 0; j < columns; j++)
    {
      table[i][j] = v[i][j];
    }
  }

  // calculate data lengths
  unsigned long ** lengths = new unsigned long*[rows];
  for (size_t i = 0; i < rows; i++)
  {
    lengths[i] = new unsigned long[columns];
    for (size_t j = 0; j < columns; j++)
    {
      if (table[i][j] != nullptr)
      {
        lengths[i][j] = strlen(table[i][j]);
      }
      else
      {
        lengths[i][j] = 0;
      }
    }
  }

  result_set::result_pair temp_result = make_pair(
    ::std::make_pair(table, lengths), ::std::make_pair(rows, columns));
  init(&temp_result); // destructor will delete all allocated ptrs
}
//----------------------------------------------------------------------------//
void result_set::init(void *result)
{
  result_pair &r_pair = *(static_cast< result_pair* >(result));
  data = r_pair.first.first;
  lengths = r_pair.first.second;
  rows = r_pair.second.first;
  columns = r_pair.second.second;
}
//----------------------------------------------------------------------------//
result_set::result_set()
{
  data = nullptr;
  lengths = nullptr;
}
//----------------------------------------------------------------------------//
result_set::~result_set()
{
  for (size_t i = 0; i < rows; i++)
  {
    delete[] data[i];
    delete[] lengths[i];
  }
  delete[] lengths;
  delete[] data;
}
//----------------------------------------------------------------------------//
unsigned long long result_set::num_rows()
{
  return rows;
}
//----------------------------------------------------------------------------//
char **result_set::fetch_row()
{
  if (increment)
  {
    if (current_row_position == ULLONG_MAX)
    {
      current_row_position = 0; // should only enter here at first call
    }
    else if (current_row_position < ULLONG_MAX)
    {
      current_row_position += 1;
    }
    else
    {
      // should never enter here :-)
    }
  }
  else
  {
    increment = true;
  }
  if (current_row_position < rows)
  {
    return const_cast< char** >(data[current_row_position]);
  }
  else
  {
    return nullptr;
  }
}
//----------------------------------------------------------------------------//
unsigned long *result_set::fetch_lengths()
{
  if (current_row_position < rows)
  {
    return lengths[current_row_position];
  }
  else
  {
    return nullptr;
  }
}
//----------------------------------------------------------------------------//
::graduation_project::database::result::row result_set::get_row()
{
  char** r = fetch_row();
  return ::graduation_project::database::result::row(r, fetch_lengths());
}
//----------------------------------------------------------------------------//
unsigned long long result_set::tell()
{
  return current_row_position;
}
//----------------------------------------------------------------------------//
void result_set::seek(unsigned long long index)
{
  current_row_position = index;
  increment = false;
}
//----------------------------------------------------------------------------//
} // namespace fake
} // namespace database
} // namespace graduation_project
