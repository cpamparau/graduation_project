#ifndef GRADUATION_PROJECT_DATABASE_FAKE_RESULT_SET_HPP
#define GRADUATION_PROJECT_DATABASE_FAKE_RESULT_SET_HPP

#include <climits> // ULLONG_MAX
#include <vector> // vector
#include "graduation_project/database/interf/result_set.hpp"

namespace graduation_project
{
namespace database
{
namespace fake
{
/**
 * @brief The result_set class implements the result_set interface
 * but can be initialized using a 2D array of C-strings. Then the user can
 * iterate on this result set as if it was received from a DB server:
 * @code
 *   ::graduation_project::database::fake::result_set r;
 *   r.initialize({
 *     { NULL, "non-empty string", 0, "", nullptr}
 *   });
 *
 *   ::graduation_project::result::row row = r.get_row();
 *   if(r[0].is_null()){} // ...
 * @endcode
 */

class result_set: public interf::result_set
{
  /**
   * @brief Holds data contained in result set. User will iterate over this.
   */
  const char ***data = nullptr;

  /**
   * @brief Contains the cell length of each value from data result set
   */
  unsigned long **lengths = nullptr;

  /**
   * @brief Number of rows in result set
   */
  size_t rows = 0;

  /**
   * @brief Number of columns in result set
   */
  size_t columns = 0;

  /**
   * @brief Current position while iterating through rows.
   * @note this value is incremented by 1 when calling fetch_row() or get_row().
   * @note this value is changed when calling seek().
   */
  unsigned long long current_row_position = ULLONG_MAX;

  /**
   * Because fetch_row() does not always move to the next row, it is normal to
   * increment the current_row_position only when it advances to the next row.
   *
   * This starts as true, and because most of the times, fetch_row() moves to
   * the next row, current_row_position will be incremented. However, if we
   * previously called seek() then the call to fetch_row() will not move to
   * another row, instead it will return the row we are currently on (set by
   * seek). Because of this reason we will not increment the
   * current_row_position value.
   */
  bool increment = true;

  /**
   * @brief Gets data, lengths, rows and columns from result.
   * @param[in] result - Interpreted as a pointer to a result_pair.
   */
  void init(void *result);

public:
  /**
   * @brief Data and lengths. first pointer is a 2D matrix of C-strings
   * containing data received from server. The second pointer contains a 2D
   * matrix of numbers, each number represents the number of characters in each
   * cell.
   */
  typedef ::std::pair< const char ***, unsigned long ** > data_pair;

  /**
   * @brief pair of number of rows and number of columns.
   */
  typedef ::std::pair< size_t, size_t > row_cols_pair;

  /**
   * @brief pair of data and number of rows/cols.
   */
  typedef ::std::pair< data_pair, row_cols_pair > result_pair;

  /**
   * @brief Allocates a char*** for the data and a unsigned long ** for the
   * lengths and passes them to the private init method.
   * @note will only take first columns columns from each row. And will only use
   * values from first rows rows.
   * @param[in] data - C-strings in result
   */
  void initialize(::std::vector< ::std::vector< const char* > > data);

  /**
   * @brief result_set Default constructor
   */
  result_set();

  /**
   * @brief Remove copy constructor to avoid copying.
   */
  result_set(result_set const&) = delete;

  /**
   * @brief Remove assignment operator to avoid copying.
   */
  void operator=(result_set const&) = delete;

  ~result_set();

  unsigned long long num_rows();

  char **fetch_row();

  unsigned long *fetch_lengths();

  ::graduation_project::database::result::row get_row();

  unsigned long long tell();

  void seek(unsigned long long index);
};
} // namespace fake
} // namespace database
} // namespace graduation_project

#endif // GRADUATION_PROJECT_DATABASE_FAKE_RESULT_SET_HPP
