#ifndef GRADUATION_PROJECT_DATABASE_INTERF_DATABASE_SYSTEM_HPP
#define GRADUATION_PROJECT_DATABASE_INTERF_DATABASE_SYSTEM_HPP

#include "graduation_project/database/quick_connection.hpp" // for ptr_quick_connection
namespace graduation_project
{
namespace database
{
namespace interf
{
/**
 * @brief The mysql_system class used to send SQL commands (queries)
 */
class database_system
{
public:
  /**
   * @brief ~mysql_result_set destructor.
   */
  virtual ~database_system() = 0;

  /**
   * @brief get_new_connection make a new connection
   * @param[in] mysql_info - reference to mysql_connection class
   *
   * @throw If something went wrong (Creating quick_connection, for example), an
   * exception is thrown.
   * @return A shared pointer to a new connection.
   */
  virtual ::graduation_project::database::ptr_quick_connection get_new_connection(
    const database_connection_info& mysql_info) const = 0;

  /**
   * @brief query This function will execute a query with no return value
   * ( is used for INSERT, DELETE, UPDATE, s.a).
   *
   * @param[in] mysql_info - a reference to mysql_conenction_info who contains
   * all information that connection need.
   * @param[in] query - The SQL INSERT INTO command
   *
   * @throw If the query fails, an exception is thrown.
   */
  virtual void query(const database_connection_info& mysql_info,
    const ::std::string &query) const = 0;

  /**
   * @brief query This function will execute a query with no return value
   * (for example INSERT, DELETE, UPDATE, etc.) using the quick_connection
   * provided as a parameter
   * @param[in] connection - a shared pointer to a quick_connection to be used
   * when running the query
   * @param[in] query - a string representing an sql query
   *
   * @throw Throws an exception if the query fails
   */
  virtual void query(::graduation_project::database::ptr_quick_connection connection,
    const ::std::string &query) const = 0;

  /**
   * @brief query_with_result This function will select informations
   * from database
   *
   * @param[in] mysql_info - a reference to mysql_conenction_info who contains
   * all information that connection need.
   * @param[in] query - The SQL INSERT INTO command
   * @return - returns a shared pointer to a mysql_result_set containing the
   * data selected by the query
   *
   * @throw If the query fails, an exception is thrown.
   */
  virtual ::graduation_project::database::interf::ptr_result_set query_with_result(
    const database_connection_info& mysql_info,
    const ::std::string &query) const = 0;

  /**
   * @brief query_with_result This function runs a select query using the
   * database connection provided as a parameter.
   * @param[in] connection - the connection to the database that needs to be
   * used
   * @param[in] query - a valid sql select query
   * @return - returns a shared pointer to a mysql_result_set containing the
   * data selected by the query
   *
   * @throw Throws an exception if the query fails
   */
  virtual ::graduation_project::database::interf::ptr_result_set query_with_result(
    ::graduation_project::database::ptr_quick_connection connection,
    const ::std::string &query) const = 0;

};
} // namespace interf
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_INTERF_DATABASE_SYSTEM_HPP
