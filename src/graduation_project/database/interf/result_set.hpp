#ifndef GRADUATION_PROJECT_DATABASE_INTERF_RESULT_SET_HPP
#define GRADUATION_PROJECT_DATABASE_INTERF_RESULT_SET_HPP

#include <memory> // ::std::shared_ptr
#include "graduation_project/database/result/row.hpp"

namespace graduation_project
{
namespace database
{
namespace interf
{
/**
 * @brief This interface describes a result set received from the database
 * server. This can be seen as a two-dimensional array of C-strings (char***),
 * or as a 3D matrix of characters :-).
 */
class result_set
{
  /**
   * @brief Initializes result object, using the given pointer.
   *
   * @note This method is called by mysql_connection class, when calling
   * store_result(), to set the result into this instance.
   *
   * @note Do not call this method, manually, because once it was called, a
   * second call is ignored. To prevent users from calling this method manually,
   * the method was made private, and this class was made a friend of
   * mysql_connection.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-store-result.html
   *
   * @param[in] result - A mysql result structure initialized by store_result()
   */
  void init(void *result);

public:
  /**
   * @brief ~mysql_result_set destructor.
   */
  virtual ~result_set() = 0;

  /**
   * @brief Returns the number of rows in the result set.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-num-rows.html
   *
   * @return The number of rows in the result set.
   */
  virtual unsigned long long num_rows() = 0;

  /**
   * @brief Retrieves the next row of a result set.
   *
   * When used after store_result(), fetch_row() returns nullptr when there are
   * no more rows to retrieve.
   *
   * The number of values in the row is given by num_fields(). If row holds the
   * return value from a call to fetch_row(), pointers to the values are
   * accessed as row[0] to row[mysql_num_fields(result)-1]. nullptr values in
   * the row are indicated by nullptr pointers.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-fetch-row.html
   *
   * @return A char** structure for the next row. nullptr if there are no more
   * rows to retrieve or if an error occurred.
   */
  virtual char **fetch_row() = 0;

  /**
   * @brief Returns the lengths of the columns ofcvs_query the current row within a
   * result set.
   *
   * @note fetch_lengths() is valid only for the current row of the result set.
   * It returns nullptr if you call it before calling fetch_row() or after
   * retrieving all rows in the result.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-fetch-lengths.html
   *
   * @return An array of unsigned long integers representing the size of each
   * column (not including any terminating null bytes). nullptr if an error
   * occurred.
   */
  virtual unsigned long *fetch_lengths() = 0;

  /**
   * @brief Retrieves the next row instance of a result set.
   *
   * A row class need a char** obtained with fetch_row & a unsigned long*
   * obtained with fetch_lengths.
   *
   * @return a row instance.
   */
  virtual ::graduation_project::database::result::row get_row() = 0;

  /**
   * @brief Returns the current position (row number) in a query result set.
   * @return a row number. A value in the range from 0 to num_rows()-1.
   */
  virtual unsigned long long tell() = 0;

  /**
   * @brief Seeks to an arbitrary row in a query result set.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-data-seek.html
   *
   * @param[in] index - a row number. Specify a value in the range from 0 to
   * num_rows()-1.
   * @note This function requires that the result set structure contains the
   * entire result of the query, so seek() may be used only in conjunction with
   * store_result(), not with use_result().
   */
  virtual void seek(unsigned long long index) = 0;
};

/**
 * Shared pointer to result set. Auto deleting it from memory.
 */
typedef ::std::shared_ptr< result_set > ptr_result_set;
} // namespace interf
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_INTERF_RESULT_SET_HPP
