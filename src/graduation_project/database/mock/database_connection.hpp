#ifndef GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_CONNECTION_HPP
#define GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_CONNECTION_HPP

#include <gmock/gmock.h>

#include "graduation_project/database/interf/database_connection.hpp"

namespace graduation_project
{
namespace database
{
namespace mock
{
/**
 * Implements a mock database_connection.
 */
class database_connection: public interf::database_connection
{
public:
  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(init, bool());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD7(connect, bool(const char *host, const char *user,
      const char *passwd, const char *db, unsigned int port,
      const char *unix_socket, unsigned long client_flag));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD2(options, int(const enum mysql_option &option, const void *arg));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(ping, int());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD1(query, int(const char *stmt_str));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(field_count, unsigned int());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(affected_rows, unsigned long long());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(close, void());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(error, const char*());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(error_number, unsigned int());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD1(store_result, bool(interf::result_set &result_set));
};
} // namespace mock
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_CONNECTION_HPP
