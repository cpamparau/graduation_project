#ifndef GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_SYSTEM_HPP
#define GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_SYSTEM_HPP

#include <gmock/gmock.h>

#include "graduation_project/database/interf/database_system.hpp"

namespace graduation_project
{
namespace database
{
namespace mock
{
/**
 * Implements a mock mysql_system.
 */
class database_system: public interf::database_system
{
public:
  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD1(get_new_connection,
      ::graduation_project::database::ptr_quick_connection (
      const database_connection_info& mysql_info));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD2(query, void (const database_connection_info& mysql_info,
      const ::std::string &query));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD2(query, void (
      ::graduation_project::database::ptr_quick_connection connection,
      const ::std::string &query));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD2(query_with_result,
    ::graduation_project::database::interf::ptr_result_set(
      const database_connection_info& mysql_info, const ::std::string &query));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD2(query_with_result,
    ::graduation_project::database::interf::ptr_result_set(
      const ::graduation_project::database::ptr_quick_connection connection,
      const ::std::string &query));

};
} // namespace mock
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_MOCK_DATABASE_SYSTEM_HPP
