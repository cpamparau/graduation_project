#ifndef GRADUATION_PROJECT_DATABASE_MOCK_RESULT_SET_HPP
#define GRADUATION_PROJECT_DATABASE_MOCK_RESULT_SET_HPP

#include <gmock/gmock.h>

#include "graduation_project/database/interf/result_set.hpp"

namespace graduation_project
{
namespace database
{
namespace mock
{
/**
 * Implements a mock mysql result set.
 */
class result_set: public interf::result_set
{
public:
  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD1(init, void(void *result));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(num_rows, unsigned long long());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(fetch_row, char **());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(fetch_lengths, unsigned long *());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(get_row, ::graduation_project::database::result::row());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(tell, unsigned long long());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD1(seek, void(unsigned long long index));
};
} // namespace mock
} // namespace cvs_query
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_MOCK_RESULT_SET_HPP
