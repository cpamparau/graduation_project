#include <memory> // shared ptr
#include "graduation_project/database/quick_connection.hpp"
#include "graduation_project/database/exception.hpp"

using namespace ::std;

namespace graduation_project
{
namespace database
{
//----------------------------------------------------------------------------//
quick_connection::quick_connection(const database_connection_info &mysql_info)
{
  // init is called in the super constructor
  if (!connect(mysql_info))
  {
    throw exception("Could not connect to database",
      exception::CONNECTION_FAILED);
  }
}
//----------------------------------------------------------------------------//
bool quick_connection::connect(const database_connection_info &mysql_info)
{
  // set SECURE_AUTH
  if (mysql_info.secure_auth != mysql_info.SECURE_AUTH_SYSTEM_DEFAULT)
  {
    if (options(MYSQL_SECURE_AUTH, &mysql_info.secure_auth) != 0)
    {
      throw exception("Could not set MYSQL_SECURE_AUTH option.",
        exception::SECURE_AUTH);
    }
  }

  // set MYSQL_OPT_RECONNECT
  if (options(MYSQL_OPT_RECONNECT, &mysql_info.should_reconnect) != 0)
  {
    throw exception("Could not set MYSQL_OPT_RECONNECT option.",
      exception::RECONNECT);
  }

  if (mysql_info.timeout_seconds != 0)
  {
    if (options(MYSQL_OPT_CONNECT_TIMEOUT, &mysql_info.timeout_seconds) != 0)
    {
      throw exception("Could not set MYSQL_OPT_CONNECT_TIMEOUT option.",
        exception::CONNECT_TIMEOUT);
    }
    if (options(MYSQL_OPT_READ_TIMEOUT, &mysql_info.timeout_seconds) != 0)
    {
      throw exception("Could not set MYSQL_OPT_READ_TIMEOUT option.",
        exception::READ_TIMEOUT);
    }
    if (options(MYSQL_OPT_WRITE_TIMEOUT, &mysql_info.timeout_seconds) != 0)
    {
      throw exception("Could not set MYSQL_OPT_WRITE_TIMEOUT option.",
        exception::WRITE_TIMEOUT);
    }
  }

  return database_connection::connect(
    (mysql_info.host.empty() ? nullptr : mysql_info.host.c_str()),
    (mysql_info.user.empty() ? nullptr : mysql_info.user.c_str()),
    (mysql_info.passwd.empty() ? nullptr : mysql_info.passwd.c_str()),
    (mysql_info.db.empty() ? nullptr : mysql_info.db.c_str()), mysql_info.port,
    (mysql_info.unix_socket.empty() ? nullptr : mysql_info.unix_socket.c_str()),
    mysql_info.client_flags);
}
//----------------------------------------------------------------------------//
interf::ptr_result_set quick_connection::store_result()
{
  ::std::shared_ptr < real::result_set > r(new real::result_set());
  if (!database_connection::store_result(*r))
  {
    throw exception("Could not store result.", exception::STORE_FAILED,
      static_cast< int >(error_number()));
  }
  return r;
}
//----------------------------------------------------------------------------//
}// namespace database
} // namespace graduation_project
