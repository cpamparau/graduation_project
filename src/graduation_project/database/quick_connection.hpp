#ifndef GRADUATION_PROJECT_DATABASE_QUICK_CONNECTION_HPP
#define GRADUATION_PROJECT_DATABASE_QUICK_CONNECTION_HPP

#include <memory>

#include "graduation_project/database/real/database_connection.hpp"
#include "graduation_project/database/real/result_set.hpp"

namespace graduation_project
{
namespace database
{
/**
 * @brief This implementation implements a real::mysql_connection
 */
class quick_connection: public real::database_connection
{
public:
  /**
   * @brief Call init and sets is_initialized and then make a connection
   * @param[in] mysql_info - reference to mysql_connection class
   *
   * @throw If something went wrong(the connection could not be established) ,
   * an exception is thrown.
   */
  quick_connection(const database_connection_info &mysql_info);

  /**
   * @brief Attempts to establish a connection to a MySQL database engine
   * running on given host.
   *
   * @note It seems that multiple calls to connect, without calling close, will
   * not lead to memory leaks.
   *
   * @note For more info see:
   * https://dev.mysql.com/doc/refman/5.7/en/mysql-connect.html
   *
   * @note Unknown behaviour if you call this connect method, multiple times, on
   * the same instance. This happens because this method sets options on the
   * current connection, and the options should be set after init and before
   * connect. A second call to this method would set the options between two
   * connect calls.
   *
   * @throw If something went wrong, an exception is thrown.
   *
   * @param[in] mysql_info - a reference to mysql_connection_info who contains
   * all the informations about connection
   * @return True if and only if the connection was successful, and false
   * otherwise.
   * @note Call init before calling this method.
   */
  bool connect(const database_connection_info &mysql_info); // this warning is O.K. - layer2

  /**
   * @note Reads the entire result of a query to the client, allocates a
   * MYSQL_RES structure, and return him with a shared_ptr.
   * @throw If the store_result fails, an exception is thrown.
   * @return Return shared pointer to mysql result set.
   */
  interf::ptr_result_set store_result(); // this warning is O.K. - layer2
};

/**
 * @brief shared pointer to quick connection.
 */
typedef ::std::shared_ptr< ::graduation_project::database::quick_connection >
  ptr_quick_connection;
} // namespace database
} // namespace graduation_project
#endif // CVS_QUERY_QUICK_CONNECTION_HPP
