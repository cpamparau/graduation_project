//#include "graduation_project/real/mysql_driver.hpp"
#include "graduation_project/database/real/database_connection.hpp"
#include "graduation_project/database/real/result_set.hpp"
namespace graduation_project
{
namespace database
{
namespace real
{
//----------------------------------------------------------------------------//
database_connection::database_connection()
{
  is_initialized = init(); // allocates memory
}
//----------------------------------------------------------------------------//
database_connection::~database_connection()
{
  if (is_initialized)
  {
    close(); // free memory allocated in constructor
    mysql_thread_end();
  }
}
//----------------------------------------------------------------------------//
int database_connection::options(const enum mysql_option &option, const void *arg)
{
  return mysql_options(&conn, option, arg);
}
//----------------------------------------------------------------------------//
bool database_connection::init()
{
  MYSQL *ret = mysql_init(&conn);
  if (ret == nullptr)
  {
    // could not initialize. Probably because there was insufficient memory to
    // allocate a new object.
    return false;
  }
  else
  {
    // ret is the same as &conn. A pointer to the same initialized instance.
    return true;
  }
}
//----------------------------------------------------------------------------//
bool database_connection::connect(const char *host, const char *user,
  const char *passwd, const char *db, unsigned int port,
  const char *unix_socket, unsigned long client_flag)
{
  MYSQL *connection_attempt = mysql_real_connect(&conn, host, user, passwd, db,
    port, unix_socket, client_flag);
  if (connection_attempt == nullptr)
  {
    // Could not connect
    return false;
  }
  else
  {
    // Connection successful.
    return true;
  }
}
//----------------------------------------------------------------------------//
int database_connection::ping()
{
  return mysql_ping(&conn);
}
//----------------------------------------------------------------------------//
int database_connection::query(const char *stmt_str)
{
  return mysql_query(&conn, stmt_str);
}
//----------------------------------------------------------------------------//
unsigned int database_connection::field_count()
{
  return mysql_field_count(&conn);
}
//----------------------------------------------------------------------------//
unsigned long long database_connection::affected_rows()
{
  return mysql_affected_rows(&conn);
}
//----------------------------------------------------------------------------//
void database_connection::close()
{
  mysql_close (&conn);
}
//----------------------------------------------------------------------------//
const char *database_connection::error()
{
  return mysql_error(&conn);
}
//----------------------------------------------------------------------------//
unsigned int database_connection::error_number()
{
  return mysql_errno(&conn);
}
//----------------------------------------------------------------------------//
bool database_connection::store_result(interf::result_set &result_set_obj)
{
  // Get result from server
  MYSQL_RES *result = mysql_store_result(&conn);

  // Set it into mysql_result_set
  result_set& r = dynamic_cast< result_set& >(result_set_obj);
  r.init(result);

  // Return bool
  if (result == nullptr)
  {
    // an error occurred while reading/downloading the result from server.
    return false;
  }
  else
  {
    // Result set read completed. Might have zero or more rows inside.
    return true;
  }
}
//----------------------------------------------------------------------------//
}// namespace real
} // namespace database
} // namespace graduation_project
