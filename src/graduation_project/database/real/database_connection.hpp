#ifndef GRADUATION_PROJECT_DATABASE_REAL_DATABASE_CONNECTION_HPP
#define GRADUATION_PROJECT_DATABASE_REAL_DATABASE_CONNECTION_HPP

#include <memory>
#include <mysql.h>

#include "graduation_project/database/interf/database_connection.hpp"

namespace graduation_project
{
namespace database
{
namespace real
{
/**
 * @brief This implementation uses MYSQL struct to handle the connection.
 */
class database_connection: public interf::database_connection
{
protected:
  /**
   * @brief True if conn was initialized successfully. This happens during
   * constructor. If this is true, at destruction time, close will be called and
   * memory will be freed.
   */
  bool is_initialized;

  /**
   * @note Private because it will be called automatically by constructor. We
   * do not want the user to call it, because a memory leak will occur.
   * @return True if the connection was initialized and false otherwise. This
   * value is stored in is_initialized. If a value of false is returned,
   * probably there was insuficient memory.
   */
  bool init();

  /**
   * @brief MySQL Structure used to hold information about connection.
   */
  MYSQL conn;

public:
  /**
   * @brief Calls init and sets is_initialized.
   */
  database_connection();

  /**
   * If is_initialized is true, calls close.
   */
  ~database_connection();

  bool connect(const char *host, const char *user, const char *passwd,
    const char *db = nullptr, unsigned int port = 0, const char *unix_socket =
      nullptr, unsigned long client_flag = 0);

  int ping();

  int query(const char *stmt_str);

  unsigned int field_count();

  unsigned long long affected_rows();

  void close();

  const char *error();

  unsigned int error_number();

  /**
   * @note Reads the entire result of a query to the client, allocates a
   * MYSQL_RES structure, and places the result into this structure.
   * @param[in,out] result_set - result set used to store result received from
   * server. The given instance will be initialized to contain the new values.
   * @return True if the result was successfully read from server(even if
   * contains 0 rows) and false if there was an error while reading the response
   * from server.
   */
  bool store_result(interf::result_set &result_set);

  int options(const enum mysql_option &option, const void *arg);
};
} // namespace real
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_REAL_DATABASE_CONNECTION_HPP
