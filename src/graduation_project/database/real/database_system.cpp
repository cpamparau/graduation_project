#include "graduation_project/database/real/database_system.hpp"
#include "graduation_project/database/exception.hpp"

using namespace graduation_project::database;
namespace graduation_project
{
namespace database
{
namespace real
{
//----------------------------------------------------------------------------//
::ptr_quick_connection mysql_system::get_new_connection(
const database_connection_info& mysql_info) const
{
  return ::ptr_quick_connection(new quick_connection(mysql_info));
}
//----------------------------------------------------------------------------//
void mysql_system::query(const database_connection_info& mysql_info,
  const ::std::string &query) const
{
  ::ptr_quick_connection c = get_new_connection(mysql_info);
  this->query(c, query);
}
//----------------------------------------------------------------------------//
void mysql_system::query(::ptr_quick_connection connection,
  const ::std::string &query) const
{
  if (!connection)
  {
    throw exception(
      "Invalid connection when trying to run query [" + query + "]",
      exception::CONNECTION_FAILED);
  }

  if (connection->query(query.c_str()) != 0)
  {
    throw exception(
      "The query : " + query + " failed with message : \"" + connection->error()
        + "\".", exception::QUERY_FAILED,
      static_cast< int >(connection->error_number()));
  }
}
//----------------------------------------------------------------------------//
::interf::ptr_result_set mysql_system::query_with_result(
  const database_connection_info& mysql_info, const ::std::string &query) const
{
  ::ptr_quick_connection c = get_new_connection(mysql_info);
  return query_with_result(c, query);
}
//----------------------------------------------------------------------------//
::interf::ptr_result_set mysql_system::query_with_result(
  ::ptr_quick_connection connection,
  const ::std::string &query) const
{
  if (!connection)
  {
    throw exception(
      "Invalid connection when trying to run query [" + query + "]",
      exception::CONNECTION_FAILED);
  }

  if (connection->query(query.c_str()) != 0)
  {
    throw exception(
      "The query : " + query + " failed with message : \"" + connection->error()
        + "\".", exception::QUERY_FAILED,
      static_cast< int >(connection->error_number()));
  }
  return connection->store_result();
}
//----------------------------------------------------------------------------//
}// namespace real
} // namespace database
} // namespace graduation_project
