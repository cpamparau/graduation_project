#ifndef GRADUATION_PROJECT_DATABASE_REAL_DATABASE_SYSTEM_HPP
#define GRADUATION_PROJECT_DATABASE_REAL_DATABASE_SYSTEM_HPP

#include <string>

#include "graduation_project/database/interf/database_system.hpp"
#include "graduation_project/database/result/row.hpp"

namespace graduation_project
{
namespace database
{
namespace real
{
/**
 * @brief This implementation uses a mysql_connection_info instance & a
 * quick_connection instance
 */
class mysql_system: public interf::database_system
{
public:
  ::graduation_project::database::ptr_quick_connection get_new_connection(
    const database_connection_info& mysql_info) const override;

  void query(const database_connection_info& mysql_info,
    const ::std::string &query) const override;

  void query(::graduation_project::database::ptr_quick_connection connection,
    const ::std::string &query) const override;

  ::graduation_project::database::interf::ptr_result_set query_with_result(
    const database_connection_info& mysql_info, const ::std::string &query) const
      override;

  ::graduation_project::database::interf::ptr_result_set query_with_result(
    ::graduation_project::database::ptr_quick_connection connection,
    const ::std::string &query) const override;
};
} // namespace real
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_REAL_DATABASE_SYSTEM_HPP
