#include "graduation_project/database/real/result_set.hpp"
namespace graduation_project
{
namespace database
{
namespace real
{
//----------------------------------------------------------------------------//
result_set::result_set() :
  result_set_ptr(nullptr)
{
}
//----------------------------------------------------------------------------//
result_set::~result_set()
{
  // No problem if result_set is nullptr, the implementation will only do
  // something if not nullptr.
  mysql_free_result (result_set_ptr);
}
//----------------------------------------------------------------------------//
void result_set::init(void *result)
{
  if (result_set_ptr == nullptr)
  {
    result_set_ptr = static_cast< MYSQL_RES* >(result);
  }
  else
  {
    /* do not overwrite result_set, because we would get a memory leak, losing
     * the address of the allocated memory to hold the result. This memory space
     * could be very large.
     */
  }
}
//----------------------------------------------------------------------------//
unsigned long long result_set::num_rows()
{
  return mysql_num_rows(result_set_ptr);
}
//----------------------------------------------------------------------------//
char **result_set::fetch_row()
{
  if (increment)
  {
    if (current_row_position == ULLONG_MAX)
    {
      current_row_position = 0; // should only enter here at first call
    }
    else if (current_row_position < ULLONG_MAX)
    {
      current_row_position += 1;
    }
    else
    {
      // should never enter here :-)
    }
  }
  else
  {
    increment = true;
  }
  return mysql_fetch_row(result_set_ptr);
}
//----------------------------------------------------------------------------//
unsigned long *result_set::fetch_lengths()
{
  return mysql_fetch_lengths(result_set_ptr);
}
//----------------------------------------------------------------------------//
::graduation_project::database::result::row result_set::get_row()
{
  char** r = fetch_row();
  return ::graduation_project::database::result::row(r, fetch_lengths());
}
//----------------------------------------------------------------------------//
unsigned long long result_set::tell()
{
  return current_row_position;
}
//----------------------------------------------------------------------------//
void result_set::seek(unsigned long long index)
{
  current_row_position = index;
  increment = false;
  mysql_data_seek(result_set_ptr, index);
}
//----------------------------------------------------------------------------//
}// namespace real
} // namespace database
} // namespace graduation_project
