#ifndef GRADUATION_PROJECT_DATABASE_REAL_RESULT_SET_HPP
#define GRADUATION_PROJECT_DATABASE_REAL_RESULT_SET_HPP

#include <climits> // ULLONG_MAX
#include <mysql.h> // MYSQL_RES
#include "graduation_project/database/interf/result_set.hpp"

namespace graduation_project
{
namespace database
{
namespace real
{

class database_connection;

/**
 * @brief This implementation uses MYSQL struct to handle the connection.
 */
class result_set: public interf::result_set
{
  /**
   * This class has to be a friend, in order to call the private member init().
   * The init() member has to be private to prevent users from calling it.
   */
  friend class database_connection;

  /**
   * @brief MySQL Structure used to hold data received from the database server.
   */
  MYSQL_RES *result_set_ptr = nullptr;

  /**
   * @brief Current position while iterating through rows.
   * @note this value is incremented by 1 when calling fetch_row() or get_row().
   * @note this value is changed when calling seek().
   */
  unsigned long long current_row_position = ULLONG_MAX;

  /**
   * Because fetch_row() does not always move to the next row, it is normal to
   * increment the current_row_position only when it advances to the next row.
   *
   * This starts as true, and because most of the times, fetch_row() moves to
   * the next row, current_row_position will be incremented. However, if we
   * previously called seek() then the call to fetch_row() will not move to
   * another row, instead it will return the row we are currently on (set by
   * seek). Because of this reason we will not increment the
   * current_row_position value.
   */
  bool increment = true;

  /**
   * @brief init
   * @param[in] result - Interpreted as MYSQL_RES* and set in result_set if the
   * result_set is empty. If the result_set is not empty the pointer will not be
   * changed, to prevent mem-leak.
   */
  void init(void *result);

public:
  /**
   * Sets result set pointer to nullptr
   */
  result_set();

  /**
   * @brief Deletes copy constructor to make sure a singleton instance can not
   * be copied.
   */
  result_set(result_set const&) = delete;

  /**
   * @brief Deletes assignment operator to make sure a singleton instance can
   * not be copied.
   */
  void operator=(result_set const&) = delete;

  /**
   * Frees result set.
   */
  ~result_set();

  unsigned long long num_rows();

  char **fetch_row();

  unsigned long *fetch_lengths();

  ::graduation_project::database::result::row get_row();

  unsigned long long tell();

  /**
   * @note Incredibly slow. Though
   * https://docs.oracle.com/cd/E17952_01/apis-php-en/apis-php-en.pdf states
   * that: "moves the internal row pointer of the MySQL result associated with
   * the specified result identifier to point to the specified row numbe"
   *
   * @note It's safe to use it if you need to loop twice through the result set.
   * That means, calling it once or twice per result set. But avoid calling it
   * a thousand times.
   *
   * @param[in] index - Row number to seek(jump) to.
   */
  void seek(unsigned long long index);
};
} // namespace real
} // namespace database
} // namespace graduation_project
#endif // GRADUATION_PROJECT_DATABASE_REAL_RESULT_SET_HPP
