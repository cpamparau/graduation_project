#include <cstring>
#include <stdlib.h> // for atoi, strtoull
#include <stdio.h> // sscanf
#include "graduation_project/database/result/cell.hpp"

namespace graduation_project
{
namespace database
{
namespace result
{
//----------------------------------------------------------------------------//
cell::cell(char* v, const unsigned long &s)
{
  value = v;
  size = s;
}
//----------------------------------------------------------------------------//
bool cell::is_null() const
{
  return value == nullptr && size == 0;
}
//----------------------------------------------------------------------------//
//-------------------------------Cast Operators-------------------------------//
//----------------------------------------------------------------------------//
cell::operator signed char() const
{
  if (value != nullptr)
  {
    return static_cast< char >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator unsigned char() const
{
  if (value != nullptr)
  {
    return static_cast< unsigned char >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator signed short int() const
{
  if (value != nullptr)
  {
    return static_cast< signed short int >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator unsigned short int() const
{
  if (value != nullptr)
  {
    return static_cast< unsigned short int >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator signed int() const
{
  if (value != nullptr)
  {
    return static_cast< signed int >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator unsigned int() const
{
  if (value != nullptr)
  {
    return static_cast< unsigned int >(atoi(value));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator signed long long int() const
{
  if (value != nullptr)
  {
    return static_cast< signed long long int >(strtoll(value, nullptr, 10));
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator unsigned long long int() const
{
  if (value != nullptr)
  {
    return strtoull(value, nullptr, 0);
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator double() const
{
  if (value != nullptr)
  {
    return atof(value);
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator float() const
{
  if (value != nullptr)
  {
    return strtof(value, nullptr);
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator bool() const
{
  return (*value != '0');
}
//----------------------------------------------------------------------------//
cell::operator long double() const
{
  if (value != nullptr)
  {
    return strtold(value, nullptr);
  }
  return 0;
}
//----------------------------------------------------------------------------//
cell::operator ::std::string() const
{
  if (value != nullptr)
  {
    return ::std::string(value, size);
  }
  return "";
}
//----------------------------------------------------------------------------//
//-----------------------------END Cast Operators-----------------------------//
//----------------------------------------------------------------------------//
unsigned char cell::from_unsigned_TINYINT() const
{
  return operator unsigned char();
}
//----------------------------------------------------------------------------//
char cell::from_signed_TINYINT() const
{
  return operator signed char();
}
//----------------------------------------------------------------------------//
unsigned short int cell::from_unsigned_SMALLINT() const
{
  return operator unsigned short();
}
//----------------------------------------------------------------------------//
signed short int cell::from_signed_SMALLINT() const
{
  return operator signed short();
}
//----------------------------------------------------------------------------//
int cell::from_MEDIUMINT() const
{
  return operator signed int();
}
//----------------------------------------------------------------------------//
int cell::from_signed_INT() const
{
  return operator signed int();
}
//----------------------------------------------------------------------------//
unsigned int cell::from_unsigned_INT() const
{
  return operator unsigned int();
}
//----------------------------------------------------------------------------//
long long int cell::from_signed_BIGINT()
{
  return operator signed long long();
}
//----------------------------------------------------------------------------//
unsigned long long int cell::from_unsigned_BIGINT()
{
  return operator unsigned long long();
}
//----------------------------------------------------------------------------//
float cell::from_FLOAT() const
{
  return operator float();
}
//----------------------------------------------------------------------------//
double cell::from_DOUBLE() const
{
  return operator double();
}
//----------------------------------------------------------------------------//
long double cell::from_DECIMAL() const
{
  return static_cast< long double >(operator double());
}
//----------------------------------------------------------------------------//
::std::string cell::from_TEXT() const
{
  return operator ::std::string();
}
//----------------------------------------------------------------------------//
::std::string cell::from_CHAR() const
{
  return operator ::std::string();
}
//----------------------------------------------------------------------------//
::std::string cell::from_VARCHAR() const
{
  return operator ::std::string();
}
//----------------------------------------------------------------------------//
char cell::to_ascii_code() const
{
  if (size <= 0 || value == nullptr)
  {
    return 0;
  }
  return *value;
}
//----------------------------------------------------------------------------//
::std::string cell::from_BLOB() const
{
  return operator ::std::string();
}
//----------------------------------------------------------------------------//
::std::ostream& operator<<(::std::ostream& os, const cell& obj)
{
  ::std::string str = obj;
  os.write(str.c_str(), static_cast<::std::streamsize>(str.size()));
  return os;
}
//----------------------------------------------------------------------------//
}// namespace result
} // namespace database
} // namespace graduation_project
