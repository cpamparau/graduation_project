#ifndef GRADUATION_PROJECT_DATABASE_RESULT_CELL_HPP
#define GRADUATION_PROJECT_DATABASE_RESULT_CELL_HPP

#include <ostream> // for overloading <<
#include <string>
#include <ctime> // for date

namespace graduation_project
{
namespace database
{
/**
 * @brief This implementation uses cell & row instances
 */
namespace result
{
/**
 * @brief The cell class stores a one cell from a database table.
 */
class cell
{
  /**
   * @brief value of the cell from a database table
   */
  char * value;

  /**
   * @brief size of the cell from a database table
   */
  unsigned long size;

public:
  /**
   * @brief Check for NULL value.
   * @return True if and only if the cell contains a NULL value.
   */
  bool is_null() const;

  /**
   * @brief cell constructor who sets values for value & size
   * @param[in] v - Stores a char* for value
   * @param[in] s - Stores a value for size
   */
  cell(char *v, const unsigned long &s);

  /**
   * @brief from_unsigned_tinyint - Convert a TINYINT() who is very small
   * integer. The unsigned range is 0 to 255.
   * @return his conversion into unsigned char
   */
  unsigned char from_unsigned_TINYINT() const;

  /**
   * @brief from_signed_tinyint - Convert a TINYINT() who is very small integer.
   * The signed range is -128 to 127.
   * @return his conversion into char
   */
  char from_signed_TINYINT() const;

  /**
   * @brief from_signed_smallint - Convert a SMALLINT() who is a small integer.
   * The signed range is -32768 to 32767.
   * @return his conversion into a short int
   */
  short int from_signed_SMALLINT() const;

  /**
   * @brief from_unsigned_smallint - Convert a SMALLINT() who is a s
   * mall integer. The unsigned range is 0 to 65535.
   * @return his conversion into a int
   */
  unsigned short int from_unsigned_SMALLINT() const;

  /**
   * @brief from_mediumint - Convert a MEDIUMINT() who is a medium-sized
   * integer. The signed range is -8388608 to 8388607.
   * The unsigned range is 0 to 16777215.
   * @return his conversion into a int
   */
  int from_MEDIUMINT() const;

  /**
   * @brief from_signed_int - Convert a INT() who is a normal-size integer.
   * The signed range is -2147483648 to 2147483647.
   * @return his conversion into a int
   */
  int from_signed_INT() const;

  /**
   * @brief from_unsigned_int - Convert a INT() who is a normal-size integer.
   * The unsigned range is 0 to 4294967295.
   * @return his conversion into a unsigned int
   */
  unsigned int from_unsigned_INT() const;

  /**
   * @brief from_signed_BIGINT - Convert a BIGINT() who is a big integer value.
   * Signed values range from -9223372036854775808 to 9223372036854775807.
   * @return his conversion into a long long int
   */
  long long int from_signed_BIGINT();

  /**
   * @brief from_unsigned_BIGINT - Convert a BIGINT() who is a big integer
   * value. Unsigned values range from 0 to 18446744073709551615.
   * @return his conversion into a unsigned long long int
   */
  unsigned long long int from_unsigned_BIGINT();

  /**
   * @brief from_float - Convert a FLOAT who is a small (single-precision)
   * floating-point number corresponding with a float from C++ language
   * @return his conversion into a float
   */
  float from_FLOAT() const;

  /**
   * @brief from_double - Convert a DOUBLE who is a A normal-size
   * (double-precision) floating-point number. Permissible values are
   * -1.7976931348623157E+308 to -2.2250738585072014E-308, 0, and
   * 2.2250738585072014E-308 to 1.7976931348623157E+308 corresponding with a
   * double from C++ language.
   * @return his conversion into a double
   */
  double from_DOUBLE() const;

  /**
   * @brief decimal - Convert a DECIMAL. The maximum number of digits for
   * DECIMAL is 65, but the actual range for a given DECIMAL column can be
   * constrained by the precision or scale for a given column.
   * @return his conversion into a long double
   */
  long double from_DECIMAL() const;

  /**
   * @brief from_text - Convert a TEXT in a string.
   * @return conversion into a string
   */
  ::std::string from_TEXT() const;

  /**
   * @brief from_text - Convert a CHAR in a string.
   * @return conversion into a string
   */
  ::std::string from_CHAR() const;

  /**
   * @brief from_text - Convert a VARCHAR in a string.
   * @return conversion into a string
   */
  ::std::string from_VARCHAR() const;

  /**
   * @brief to_ascii_code - Convert an sql type that contains one or more ASCII
   * characters into a C char type.
   * @return conversion into char
   */
  char to_ascii_code() const;

  /**
   * @brief from_text - Convert a BLOB in a string.
   * @return conversion into a string
   */
  ::std::string from_BLOB() const;

//-------------------------------Cast Operators-------------------------------//
  /**
   * @brief operator signed char
   */
  operator signed char() const;

  /**
   * @brief operator unsigned char
   */
  operator unsigned char() const;

  /**
   * @brief operator signed short
   */
  operator signed short int() const;

  /**
   * @brief operator unsigned short
   */
  operator unsigned short int() const;

  /**
   * @brief operator signed int
   */
  operator signed int() const;

  /**
   * @brief operator unsigned int
   */
  operator unsigned int() const;

  /**
   * @brief operator signed long long
   */
  operator signed long long int() const;

  /**
   * @brief operator unsigned long long
   */
  operator unsigned long long int() const;

  /**
   * @brief operator double
   */
  operator double() const;

  /**
   * @brief operator float
   */
  operator float() const;

  /**
   * @brief operator bool
   */
  operator bool() const;

  /**
   * @brief operator long double
   */
  operator long double() const;

  /**
   * @brief operator std::string
   */
  operator ::std::string() const;

//-----------------------------END Cast Operators-----------------------------//
};

/**
 * The overloads of operator<< that take a std::ostream& as the left hand
 * argument are known as extraction operators.
 * @param[in,out] out - Output stream to write the object to.
 * @param[in] c - cell to be written to output stream.
 * @return Output stream where the object was written.
 */
::std::ostream& operator<<(::std::ostream& out, const cell& c);

} // namespace result
} // namespace database
} // namespace graduation_project
#endif // CVS_QUERY_RESULT_CELL_HPP
