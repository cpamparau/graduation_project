#include "graduation_project/database/result/row.hpp"

namespace graduation_project
{
namespace database
{
namespace result
{
//----------------------------------------------------------------------------//
row::row(char **d, unsigned long *s)
{
  data = d;
  size = s;
}
//----------------------------------------------------------------------------//
const cell row::operator[](const ::std::size_t &index) const
{
  return cell(data[index], size[index]);
}
//----------------------------------------------------------------------------//
bool row::is_null()
{
  return data == nullptr || size == nullptr;
}
//----------------------------------------------------------------------------//
}// namespace result
} // namespace database
}
