#ifndef GRADUATION_PROJECT_DATABASE_RESULT_ROW_HPP
#define GRADUATION_PROJECT_DATABASE_RESULT_ROW_HPP

#include "graduation_project/database/result/cell.hpp"

namespace graduation_project
{
namespace database
{
namespace result
{
/**
 * @brief The row class stores a row from a database table.
 */
class row
{
  /**
   * @brief data - stores a row from a database table. To access a cell from
   * the row you can use data[index], where index is cell number in row.
   * data[index] is a char*
   */
  char** data;

  /**
   * @brief size - stores length for each cell from row. To acces length for
   * data[index] who is a cell, you need to use size[index] (same index) witch
   * is a unsigned long :)
   */
  unsigned long * size;

public:
  /**
   * @brief row - Default constructor. Sets value for data & size
   * @param[in] d - Stores value for data
   * @param[in] s - Stores value for size
   */
  row(char ** d = nullptr, unsigned long * s = nullptr);

  /**
   * @brief operator [] - Overloading of operator []. It is used then we need
   * to acces a particular cell from the row.
   * @param[in] index - cell number in row
   * @return a cell class
   */
  const cell operator[](const ::std::size_t &index) const;

  /**
   * @brief is_null - check if data & size is different by 0
   * @return true, if data or size is null.
   */
  bool is_null();
};
} // namespace result
} // namespace database
} // namespace graduation_project
#endif // CVS_QUERY_RESULT_ROW_HPP
