#include "graduation_project/file/fake/file.hpp"
namespace graduation_project
{
namespace file
{
namespace fake
{
//----------------------------------------------------------------------------//
file::file()
{
}
//----------------------------------------------------------------------------//
void file::open(const ::std::string filename, ::std::ios_base::openmode mode)
{
  file_is_open = true;
  this->filename = filename;
  if (mode == ::std::ios::out)
  {
    stream.clear();
    //stream.seekp(0, stream.beg);
    //stream.seekg(0, stream.beg);
    stream.str(::std::string()); // clear contents
  }
}
//----------------------------------------------------------------------------//
::std::stringstream& file::get_stream()
{
  return stream;
}
//----------------------------------------------------------------------------//
void file::close()
{
  file_is_open = false;
}
//----------------------------------------------------------------------------//
bool file::is_open()
{
  return file_is_open;
}
//----------------------------------------------------------------------------//
int file::get()
{
  return stream.get();
}
//----------------------------------------------------------------------------//
file& file::getline(char* s, ::std::streamsize n)
{
  stream.getline(s,n);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::getline(char* s, ::std::streamsize n, char delim)
{
  stream.getline(s,n,delim);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::read(char* s, ::std::streamsize n)
{
  stream.read(s, n);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::write(const char* s, ::std::streamsize n)
{
  stream.write(s,n);
  return *this;
}
//----------------------------------------------------------------------------//
bool file::eof() const
{
  return stream.eof();
}
//----------------------------------------------------------------------------//
bool file::good() const
{
  return stream.good();
}
//----------------------------------------------------------------------------//
std::string file::read_all_contents()
{
  return stream.str();
}
//----------------------------------------------------------------------------//
bool file::fail() const
{
  return stream.fail();
}
//----------------------------------------------------------------------------//
void file::clear()
{
  stream.clear();
}
//----------------------------------------------------------------------------//
file& file::operator<<(::std::string val)
{
  stream << val;
  return *this;
}
//----------------------------------------------------------------------------//
file& file::operator<<(const char* val)
{
  stream << val;
  return *this;
}
//----------------------------------------------------------------------------//
file& file::operator<<(::std::ostream& (*pf)(::std::ostream&))
{
  stream << pf;
  return *this;
}
//----------------------------------------------------------------------------//
}
}
}
