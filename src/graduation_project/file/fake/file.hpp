#ifndef GRADUATION_PROJECT_FILE_FAKE_FILE_HPP
#define GRADUATION_PROJECT_FILE_FAKE_FILE_HPP

#include <sstream>
#include "graduation_project/file/interf/file.hpp"

namespace graduation_project
{
namespace file
{
namespace fake
{
/**
 * Implements a virtual(fake) file, working with a buffer.
 */

class file: public ::graduation_project::file::interf::file
{

  /**
   * @brief stream to hold the file content. When reading/writing from/to file,
   * we actually read/write from/to this buffer.
   */
  ::std::stringstream stream;

  /**
   * @brief file_is_open True if and only if the file is open.
   */
  bool file_is_open = false;

public:
  /**
   * @brief file Default destructor
   */
  file();

  /**
   * @brief Get internal stream
   * @note Should only be called whe using this object in unit tests!
   * @note You can use this to add contents to the file before opening it and/or
   * read the contents after the file is closed.
   * @return A reference to the string stream used by this class.
   */
  std::stringstream& get_stream();

  void open(const ::std::string filename,
    ::std::ios_base::openmode mode = ::std::ios_base::in
    | ::std::ios_base::out) override;

  void close() override;

  void clear() override;

  bool is_open() override ;

  int get() override;

  ::graduation_project::file::fake::file& getline(char* s, ::std::streamsize n)
    override;

  ::graduation_project::file::fake::file& getline(char* s, ::std::streamsize n,
    char delim) override;

  ::graduation_project::file::fake::file& read(char* s, ::std::streamsize n)
    override;

  ::graduation_project::file::fake::file& write(const char* s,
    ::std::streamsize n) override;

  bool eof() const override;

  bool good() const override;

  bool fail() const override;

  ::std::string read_all_contents() override;

  // << operators
  ::graduation_project::file::fake::file& operator<<(::std::string val)
    override;

  ::graduation_project::file::fake::file& operator<<(const char* val) override;

  ::graduation_project::file::fake::file& operator<<(
    ::std::ostream& (*pf)(::std::ostream&)) override;
};
}
}
}
#endif // GRADUATION_PROJECT_FILE_FAKE_FILE_HPP
