#ifndef GRADUATION_PROJECT_FILE_INTERFACE_FILE_HPP
#define GRADUATION_PROJECT_FILE_INTERFACE_FILE_HPP

#include <iostream>

namespace graduation_project
{
namespace file
{
namespace interf
{

/**
 * @brief The file class is an interface for file concept in order to
 * have a mock, fake and real implementation for files
 */
class file
{
protected:
  /**
   * The name of the file that will be populated by the classes that extend
   * this class on the open-method
   */
  ::std::string filename;

public:
  /**
   * @brief ~file The destructor of this class
   */
  virtual ~file();

  /**
   * @brief open Open a file.
   * Open the file identified by argument filename, associating it with the
   * stream object, so that in/out operations are performed on its content.
   * If the stream is already associated with a file,
   * @param[in] filename - String with the name of the file to open.
   * @param[in] mode  - Flag describing the request i/o mode for the file. For
   * further details, please visit
   * http://en.cppreference.com/w/cpp/io/basic_fstream/open .
   */
  virtual void open(const ::std::string filename,
    ::std::ios_base::openmode mode = ::std::ios_base::in
    | ::std::ios_base::out) = 0;

  /**
   * @brief close Close file.
   * Closes the file currently associated with the object, disassociating it
   * from the stream.
   * Any pending output sequence is written to the file.
   * If the stream is currently not associated with any file (i.e., no file has
   * successfully been open with it), calling this function fails.
   * The file association of a stream is kept by its internal stream buffer:
   * Internally, the function calls rdbuf()->close(), and sets failbit in case
   * of failure.
   * @note Note that any open file is automatically closed when the ifstream
   * object is destroyed.
   */
  virtual void close() = 0;

  /**
   * @brief clear Sets a new value for the stream's internal error state flags.
   */
  virtual void clear() = 0;

  /**
   * @brief is_open Check if a file is open.
   * @return Returns whether the stream is currently associated to a file. true
   * if a file is open and associated with this stream object and false
   * otherwise.
   */
  virtual bool is_open() = 0;

  /**
   * @brief get Get characters.
   * @return The character read, or the end-of-file value (EOF) if no characters
   * are available in the stream.
   */
  virtual int get() = 0;

  /**
   * @brief getline Get line.
   * Extracts characters from the stream as unformatted input and stores them
   * into s as a c-string, until either the extracted character is the
   * delimiting character, or n characters have been written to s (including the
   * terminating null character).
   * The delimiting charactit flag is not set (the extracted sequence
   * was exactly n characters long).
   * A null character ('\0') is automatically appended to the written sequence
   * if n is greater than zero, even if an empty string is extracted.
   * Internally, the function accesses the input sequence by first constructing
   * a sentry object (with noskipws set to true). Then (if good), it extracts
   * characters from its associated stream buffer object as if calling its
   * member functions sbumpc or sgetc, and finally destroys the sentry object
   * before returning.
   * The number of characters successfully read and stored by this function can
   * be accessed by calling member gcount.
   * This function is overloaded for string objects in header string:
   * See getline(string).
   * @param s - Pointer to an array of characters where extracted characters are
   * stored as a c-string.
   * @param n - Maximum number of characters to write to s (including the
   * terminating null character). If this is less than 2, the function does not
   * extract any characters and sets failbit. streamsize is a signed integral
   * type.
   * @return The istream object (*this).
   */
  virtual ::graduation_project::file::interf::file& getline(char* s,
    ::std::streamsize n) = 0;

  /**
   * @brief getline Get line.
   * Extracts characters from the stream as unformatted input and stores them
   * into s as a c-string, until either the extracted character is the
   * delimiting character, or n characters have been written to s (including the
   * terminating null character).
   * The delimiting character is the newline character ('\\n') for the first
   * form, and delim for the second: when found in the input sequence, it is
   * extracted from the input sequence, but discarded and not written to s.
   * The function will also stop extracting characters if the end-of-file is
   * reached. If this is reached prematurely (before either writing n characters
   * or finding delim), the function sets the eofbit flag.
   * The failbit flag is set if the function extracts no characters, or if the
   * delimiting character is not found once (n-1) characters have already been
   * written to s. Note that if the character that follows those (n-1)
   * characters in the input sequence is precisely the delimiting character, it
   * is also extracted and the failbit flag is not set (the extracted sequence
   * was exactly n characters long).
   * A null character ('\0') is automatically appended to the written sequence
   * if n is greater than zero, even if an empty string is extracted.
   * Internally, the function accesses the input sequence by first constructing
   * a sentry object (with noskipws set to true). Then (if good), it extracts
   * characters from its associated stream buffer object as if calling its
   * member functions sbumpc or sgetc, and finally destroys the sentry object
   * before returning.
   * The number of characters successfully read and stored by this function can
   * be accessed by calling member gcount.
   * This function is overloaded for string objects in header string:
   * See getline(string).
   * @param s - Pointer to an array of characters where extracted characters are
   * stored as a c-string.
   * @param n - Maximum number of characters to write to s (including the
   * terminating null character). If this is less than 2, the function does not
   * extract any characters and sets failbit. streamsize is a signed integral
   * type.
   * @param delim - Explicit delimiting character: The operation of extracting
   * successive characters stops as soon as the next character to extract
   * compares equal to this.
   * @return The istream object (*this).
   */

  virtual ::graduation_project::file::interf::file& getline(char* s,
    ::std::streamsize n, char delim) = 0;

  /**
   * @brief read Read block of data.
   * @param s - Pointer to an array where the extracted characters are stored.
   * @param n - Number of characters to extract. streamsize is a signed integral
   * type.
   * @return The istream object (*this).
   */
  virtual ::graduation_project::file::interf::file& read(char* s,
    ::std::streamsize n)= 0;

  /**
   * @brief write Write block of data.
   * @param s - Pointer to an array of at least n characters.
   * @param n - Number of characters to insert. Integer value of type streamsize
   * representing the size in characters of the block of data to write.
   * streamsize is a signed integral type.
   * @return The ostream object (*this).
   */
  virtual ::graduation_project::file::interf::file& write(const char* s,
    ::std::streamsize n) =0;

  /**
   * @brief eof Check whether eofbit is set.
   * @return true if the stream's eofbit error state flag is set (which signals
   * that the End-of-File has been reached by the last input operation).
   * false otherwise.
   */
  virtual bool eof() const = 0;

  /**
   * @brief Check if the eofbid, failbit or badbit was set
   * @return true if none of the stream's state flags are set.false if any of
   * the stream's state flags are set (badbit, eofbit or failbit)
   */
  virtual bool good() const = 0;

  /**
   * @brief Returns true if either (or both) the failbit or the badbit error
   * state flags is set for the stream.
   * @note Concurrent access to the same stream object may cause data races.
   * @return true if badbit and/or failbit are set. false otherwise.
   */
  virtual bool fail() const = 0;

  /**
   * @brief read_all_contents Read all contents from file.
   * @return A string containing all the contents of the file.
   */
  virtual ::std::string read_all_contents() = 0;

  /**
   * @brief Gets the current filename from the protected member filename.
   * @return The current filename.
   */
  virtual ::std::string get_file_name();

  // << operators

  /**
   * @brief operator << Insert formatted output.
   * @param val - Value to be formatted and inserted into the stream.
   * @return The ostream object (*this).
   */
  virtual ::graduation_project::file::interf::file& operator<<(
    ::std::string val) = 0;

  /**
   * @brief operator << Insert formatted output.
   * @param val - Value to be formatted and inserted into the stream.
   * @return The ostream object (*this).
   */
  virtual ::graduation_project::file::interf::file& operator<<(
    const char* val) = 0;

  /**
   * @brief operator << Insert formatted output.
   * @param pf - Value to be formatted and inserted into the stream.
   * @return The ostream object (*this).
   */
  virtual ::graduation_project::file::interf::file& operator<<(
    ::std::ostream& (*pf)(::std::ostream&)) = 0;

};
}
}
}
#endif // GRADUATION_PROJECT_FILE_INTERFACE_FILE_HPP
