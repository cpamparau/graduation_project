#ifndef GRAUATION_PROJECT_FILE_MOCK_FILE_HPP
#define GRAUATION_PROJECT_FILE_MOCK_FILE_HPP

#include <gmock/gmock.h>
#include "graduation_project/file/interf/file.hpp"

namespace graduation_project
{
namespace file
{
namespace mock
{
#define MOCK_OPERATOR(op_func_name, func_ret, func_name, func_params, params) \
  MOCK_METHOD1(op_func_name, void(func_params));\
  func_ret func_name(func_params) {\
    op_func_name(params);\
    return *this;\
  }

/**
 * Implements a virtual file, working with a buffer.
 */
class file : public graduation_project::file::interf::file
{
public:

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD2(open, void(const std::string filename,
    std::ios_base::openmode mode));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(close, void());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(clear, void());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(is_open, bool());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(get, int());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD2(getline, ::graduation_project::file::interf::file& (char* s,
    ::std::streamsize n));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD3(getline, ::graduation_project::file::interf::file& (char* s,
    ::std::streamsize n, char delim));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD2(read, ::graduation_project::file::interf::file& (char* s,
    ::std::streamsize n));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD2(write, ::graduation_project::file::interf::file& (const char* s,
    ::std::streamsize n));

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD0(eof, bool());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD0(good, bool());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_CONST_METHOD0(fail, bool());

  /**
   * @brief Auto generated mock implementation.
   */
  MOCK_METHOD0(read_all_contents, ::std::string());

  /**
   * @brief Auto generated mock implementation. It generates the operator that
   * redirects the call to a mock function. See MOCK_OPERATOR macro.
   */
  MOCK_OPERATOR(out_op_string, ::graduation_project::file::mock::file&,
    operator<<, ::std::string val, val)

  /**
   * @brief Auto generated mock implementation. It generates the operator that
   * redirects the call to a mock function. See MOCK_OPERATOR macro.
   */
  MOCK_OPERATOR(out_op_const_char_ptr, ::graduation_project::file::mock::file&,
    operator<<, const char* val, val)

  /**
   * @brief Auto generated mock implementation. It generates the operator that
   * redirects the call to a mock function. See MOCK_OPERATOR macro.
   */
  MOCK_OPERATOR(out_op_ostream_fnc_ostream,
    ::graduation_project::file::mock::file&,
    operator<<, ::std::ostream& (*val)(::std::ostream&), val)

};
}
}
}

#endif // GRAUATION_PROJECT_FILE_MOCK_FILE_HPP
