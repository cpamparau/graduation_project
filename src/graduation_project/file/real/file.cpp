#include <sstream>
#include "graduation_project/file/real/file.hpp"

namespace graduation_project
{
namespace file
{
namespace real
{
//----------------------------------------------------------------------------//
file::file()
{
}
//----------------------------------------------------------------------------//
void file::open(const ::std::string filename, ::std::ios_base::openmode mode)
{
  this->filename = filename;
  stream.open(filename.c_str(), mode);
}
//----------------------------------------------------------------------------//
void file::close()
{
  stream.close();
}
//----------------------------------------------------------------------------//
bool file::is_open()
{
  return stream.is_open();
}
//----------------------------------------------------------------------------//
int file::get()
{
  return stream.get();
}
//----------------------------------------------------------------------------//
file& file::getline(char* s, ::std::streamsize n)
{
  stream.getline(s,n);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::getline(char* s, ::std::streamsize n, char delim)
{
  stream.getline(s,n,delim);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::read(char* s, ::std::streamsize n)
{
  stream.read(s, n);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::write(const char* s, ::std::streamsize n)
{
  stream.write(s,n);
  return *this;
}
//----------------------------------------------------------------------------//
bool file::eof() const
{
  return stream.eof();
}
//----------------------------------------------------------------------------//
bool file::good() const
{
  return stream.good();
}
//----------------------------------------------------------------------------//
std::string file::read_all_contents()
{
  std::stringstream buffer;
  buffer << stream.rdbuf();

  ::std::string contents(buffer.str());
  return contents;

}
//----------------------------------------------------------------------------//
bool file::fail() const
{
  return stream.fail();
}
//----------------------------------------------------------------------------//
void file::clear()
{
  stream.clear();
}
//----------------------------------------------------------------------------//
file& file::operator<<(::std::string val)
{
  stream << val;
  return *this;
}
//----------------------------------------------------------------------------//
file& file::operator<<(::std::ostream& (*pf)(::std::ostream&))
{
  stream.operator<<(pf);
  return *this;
}
//----------------------------------------------------------------------------//
file& file::operator<<(const char* val)
{
  stream << val;
  return *this;
}
//----------------------------------------------------------------------------//
}
}
}
