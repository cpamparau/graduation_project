#ifndef GRADUATION_PROJECT_FILE_REAL_FILE_HPP
#define GRADUATION_PROJECT_FILE_REAL_FILE_HPP
#include <fstream>
#include <string>
#include "graduation_project/file/interf/file.hpp"

namespace graduation_project
{
namespace file
{
namespace real
{
/**
 * Implements a real file, working with a file from the file system.
 * @note To see the documentation for this class, simply go to the interf and
 * check out it's documentation, it's inherited from there.
 */
class file: public ::graduation_project::file::interf::file
{

  /**
   * @brief Stream to hold file content.
   */
  ::std::fstream stream;
public :
  /**
   * @brief file Default destructor
   */
  file();

  /**
   * @brief Get internal stream
   * @note Should only be called whe using this object in unit tests!
   * @note You can use this to add contents to the file before opening it and/or
   * read the contents after the file is closed.
   * @return A reference to the string stream used by this class.
   */
  std::stringstream& get_stream();

  void open(const ::std::string filename,
    ::std::ios_base::openmode mode = ::std::ios_base::in
    | ::std::ios_base::out) override;

  void close() override;

  void clear() override;

  bool is_open() override ;

  int get() override;

  ::graduation_project::file::real::file& getline(char* s, ::std::streamsize n)
    override;

  ::graduation_project::file::real::file& getline(char* s, ::std::streamsize n,
    char delim) override;

  ::graduation_project::file::real::file& read(char* s, ::std::streamsize n)
    override;

  ::graduation_project::file::real::file& write(const char* s,
    ::std::streamsize n) override;

  bool eof() const override;

  bool good() const override;

  bool fail() const override;

  ::std::string read_all_contents() override;

  // << operators
  ::graduation_project::file::real::file& operator<<(::std::string val)
    override;

  ::graduation_project::file::real::file& operator<<(
      const char* val) override;

  ::graduation_project::file::real::file& operator<<(
    ::std::ostream& (*pf)(::std::ostream&)) override;
};
}
}
}
#endif // GRADUATION_PROJECT_FILE_REAL_FILE_HPP
