#include <sstream>
#include "graduation_project/server.hpp"
#include "graduation_project/utils.hpp"
#include "graduation_project/settings/settings.hpp"
#include "graduation_project/file/real/file.hpp"
#include "graduation_project/database/real/database_system.hpp"

using namespace graduation_project;
using namespace graduation_project::database;

int main()
{
  init_default_values();
  settings& settings = settings.get();
  file::real::file file;

  settings.load_params_from_file(settings["config_file"], file);
  settings.save_to_file(settings["config_file"], file);
  real::mysql_system mysql_system;
  database_connection_info mysql_info;

  std::stringstream port(settings["port"]);
  unsigned int port_val;
  port>>port_val;

  init_database_info(mysql_info);
  server server_app(port_val, settings["host"], mysql_system, mysql_info);
  server_app.start();
  return 0;
}
