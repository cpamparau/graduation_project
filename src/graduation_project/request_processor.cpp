#include <iostream>
#include "graduation_project/request_processor.hpp"
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// ^ ZeroCopyInputStream and ArrayInputStream
#include <sstream>
#include <google/protobuf/util/json_util.h> // MessageToJsonString
#include "graduation_project/requests/request.hpp"
#include "graduation_project/responses/response.hpp"
#include "graduation_project/responses/xml.hpp"
#include "graduation_project/responses/protobuf.hpp"
#include "graduation_project/database/interf/result_set.hpp"
#include "graduation_project/database/exception.hpp"

namespace graduation_project
{
//----------------------------------------------------------------------------//
request_processor::~request_processor()
{
}
//----------------------------------------------------------------------------//
request_processor::request_processor(
  database::database_connection_info mysql_connection_info,
  database::interf::database_system &mysql_system,
  graduation_project::socket::Socket& server)
  : mysql_connection_info(mysql_connection_info), mysql_system(mysql_system),
  server(server)
{

}
//----------------------------------------------------------------------------//
void request_processor::handle_message(connection_hdl& connection,
  char buffer[1024])
{
  const char protocol = buffer[0];
  if(protocol == 'P')
  {
    // log message in hex format
  }
  else if (protocol == 'X')
  {
    std::cout << "[X]Client's request : " << buffer << std::endl;
  }
  char* contents_chopped = buffer + 1;
  request_ptr request = parser.parse(protocol, contents_chopped);
  if(request == nullptr)
  {
    // Log
    std::cout << "The connection was closed because the request "
      "couldn't be parsed" << endl;
    server.close();
    return;
  }
  response_ptr response = nullptr;
  switch(request->get_type())
  {
    case requests::AUTHENTICATION:
      response = process_authenticate_request(connection,
        dynamic_cast<requests::authenticate::interf::authenticate&>(*request));
      if (response != nullptr)
      {
        string answer = response->serialize();
        server.send_n(answer.c_str(), static_cast<int>(answer.size()),
          static_cast<int>(answer.size()));
        if (response->get_status() == false)
        {
          // LOG
          server.close();
        }
      }
      else
      {
        // LOG
        // the response was not created
        server.close();
      }
      break;
    case requests::EMAIL:
      std::cout << "EMAIL request " << endl;
      break;
  }
//  if(response != nullptr)
//  {
//    // the response was not created
//    basic_string_view answer = response->serialize();
//    // LOG THE RESPONSE
//    server.send_n(answer.data(), static_cast<int>(answer.length()),
//      static_cast<int>(answer.length()));
//  }
}
//----------------------------------------------------------------------------//
response_ptr request_processor::process_authenticate_request(
  connection_hdl &connection,
  requests::authenticate::interf::authenticate& request)
{
  try {
    bool was_authenticated = connection.is_authenticated.exchange(true);
    if(was_authenticated)
    {
      // this should never happen, it means that we processed an authentication
      // request for a client that has already been authenticated
      // LOG this on socket_log
      std::cout << "CLIENT DEJA AUTENTIFICAT!" << std::endl;
      return nullptr;
    }
    bool token_exist = true;
    std::string err_msg = "";
    const std::string token = request.get_token();
    request_id request_id = request.get_id();

    if(check_token(connection, token) == false)
    {
      token_exist = false;
      err_msg += "Invalid token.";
    }
    response_ptr ptr_response = nullptr;
    switch(connection.protocol)
    {
      case 'X':
        ptr_response = response_ptr(new graduation_project::responses::xml());
        break;
      case 'P':
        ptr_response = response_ptr(
          new graduation_project::responses::protobuf());
        break;
     default:
        return nullptr;
        // this should never happen, because when we parse the raw message we
        // also check the protocol.
    }
    if(ptr_response == nullptr)
    {
      err_msg += "Response creation failed!" ;
    }
    else
    {
      ptr_response->set_id(request_id);
      ptr_response->set_status(token_exist);
      string message = token_exist == true? "Authentication OK" : err_msg;
      ptr_response->set_message(message);
      if(token_exist)
      {
        connection.is_authenticated.store(true);
      }
      // LOG_INFO the human readable response;
    }
    if(!err_msg.empty())
    {
      // LOG_ERROR
    }
    return ptr_response;
  } catch (...)
  {
    cerr << __FILE__ << ":" << __LINE__ << ": Unexpected exception" << endl;
  }
  return nullptr;
}
//----------------------------------------------------------------------------//
bool request_processor::check_token(connection_hdl &connection,
  string token)
{
  std::string query = "SELECT user_id, user_name from authentication_token WHERE "
    "AES_DECRYPT(token, 'graduation_key') = '" + string(token.data(),
    token.size()) + "' ;";
  // get the current time - we will add a mock timer
  ::graduation_project::database::interf::ptr_result_set results = nullptr;

  try {
    results = mysql_system.query_with_result(mysql_connection_info, query);
  } catch (const ::graduation_project::database::exception& e) {
    // LOG_ERROR on messages log with "Failed to run query while getting the
    // token"
    cout << "MYSQL EXCEPTION :" << e.what() << endl;
    return false;
  }

  if (results!= nullptr && results->num_rows() == 1)
  {
    ::graduation_project::database::result::row row = results->get_row();
     connection.user_id = row[0];
     connection.user_name = static_cast<string>(row[1]);
     std::cout << "Am gasit clientul!" << endl;
     // LOG_INFO STARTED session for CLIENT user_id, get_host,
     // get_port, user_name
     return true;
  }
  // LOG_INFO "More than 1 row or no row identified with token =value << in db"
  return false;
}
//----------------------------------------------------------------------------//
} // namespace graduation_project
