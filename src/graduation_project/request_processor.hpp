#ifndef GRADUATION_PROJECT_REQUEST_PROCESSOR_HPP
#define GRADUATION_PROJECT_REQUEST_PROCESSOR_HPP

#include "graduation_project/requests/request.hpp"
#include "graduation_project/responses/response.hpp"
#include "graduation_project/requests/authenticate/interf/authenticate.hpp"
#include "graduation_project/requests/message_processor.hpp"
#include "graduation_project/database/interf/database_system.hpp"
#include "graduation_project/database/database_connection_info.hpp"
#include "graduation_project/socket/socket.hpp"

namespace graduation_project
{

class request_processor
{
  /**
   * @brief request_processor Default constructor private
   */
  request_processor();

  /**
   * @brief parser - A refference to the parser class that will parse the
   * message that comes from the user.
   */
   requests::message_processor parser;

public:

  request_processor(database::database_connection_info mysql_connection_info,
    database::interf::database_system& mysql_system,
    graduation_project::socket::Socket& server);

  virtual ~request_processor();

  /**
   * @brief handle_message Function that handle every message from the client
   * @param buffer The message received
   */
  void handle_message(connection_hdl& connection, char buffer[1024]);

  /**
   * @brief process_authenticate_request calls db_check_token() than
   * generates an authentication response to send back to the user.
   * @param[in] connection - A reference to the connection.
   * @param[in] request - A reference to an authentication request type.
   * @returns a message error or an "" if there is no message error.
   */
  response_ptr process_authenticate_request(
    connection_hdl& connection,
    requests::authenticate::interf::authenticate& request);

  /**
   * @brief check_token checks if the token exists in the database if it
   * exists then stores the user information in connection object.
   * @param[in,out] connection - A reference to the connection.
   * @param[in] token - The authenticate token which is gotten at
   * authentication.
   * @returns true if the authentication succeded, false otherwise.
   */
  bool check_token(connection_hdl& connection,
    string token);

protected:

  /**
   * @brief mysql_connection_info Used to get the informations about the
   * database credentials which are used to make a connection to the database in
   * order to store, get, update, delete data related to the aplication.
   */
  database::database_connection_info mysql_connection_info;

  /**
   * @brief mysql_system Object which is used to make a connection with the
   * database and get a ptr_quick_connection.
   */
  database::interf::database_system& mysql_system;

  graduation_project::socket::Socket& server;
};
} // namespace graduation_project

#endif // GRADUATION_PROJECT_REQUEST_PROCESSOR_HPP
