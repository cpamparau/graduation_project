#include "graduation_project/requests/authenticate/interf/authenticate.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
namespace interf
{
//----------------------------------------------------------------------------//
authenticate::~authenticate()
{
}
//----------------------------------------------------------------------------//
request_type authenticate::get_type()
{
  return graduation_project::requests::AUTHENTICATION;
}
//----------------------------------------------------------------------------//
} // namespace interf
} // namespace authenticate
} // namespace requests
} // namespace graduation_project
