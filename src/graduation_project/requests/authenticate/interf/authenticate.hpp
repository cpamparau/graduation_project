#ifndef GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_INTERF_HPP
#define GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_INTERF_HPP

#include "graduation_project/requests/request.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
namespace interf
{
/**
 * @brief The authenticate class request adds getter methods for fields
 * specific to the authenticate request.
 */
class authenticate: public virtual requests::request
{
public:
  /**
   * @brief ~authenticate Destructor.
   */
  virtual ~authenticate();

  /**
   * @brief get_token This method will return the token as a string from the
   * request.
   * @return Will return the token as a string.
   */
  virtual std::string get_token() = 0;

  request_type get_type() override;
};
} // namespace interf
} // namespace authenticate
} // namespace requests
/**
 * @brief request_authenticate_ptr Shared ptr to a authenticate request object.
 */
typedef std::shared_ptr<requests::authenticate::interf::authenticate>
  request_authenticate_ptr;
} // namespace graduation_project

#endif //GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_INTERF_HPP
