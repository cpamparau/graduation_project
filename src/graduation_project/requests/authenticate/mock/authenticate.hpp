#ifndef GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_MOCK_AUTHENTICATE_HPP
#define GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_MOCK_AUTHENTICATE_HPP

#include <gmock/gmock.h>

#include "graduation_project/requests/authenticate/interf/authenticate.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
namespace mock
{
/**
 * @brief The authenticate class is a mock implementation for interface
 * authenticate.
 */
class authenticate: public interf::authenticate
{
public:
  /**
   * @brief MOCK_METHOD0 Mock implementation.
   */
  MOCK_METHOD0(get_token, basic_string_view());

  /**
   * @brief MOCK_METHOD0 Mock implementation.
   */
  MOCK_METHOD0(get_id, request_id());

  /**
   * @brief MOCK_METHOD0 Mock implementation.
   */
  MOCK_METHOD0(to_human_readable, basic_string_view());
};
} // namespace mock
} // namespace authenticate
} // namespace requests
} // namespace logging_server
#endif // GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_MOCK_AUTHENTICATE_HPP
