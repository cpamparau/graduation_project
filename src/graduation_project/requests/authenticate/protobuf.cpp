#include "graduation_project/requests/authenticate/protobuf.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
//----------------------------------------------------------------------------//
protobuf::protobuf(const ::graduation_project::proto::request_msg& request_msg)
  : protocols::protobuf (request_msg)
{
}
//----------------------------------------------------------------------------//
std::string protobuf::get_token()
{
  return request_msg.authenticate_request().token();
}
//----------------------------------------------------------------------------//
} // namespace authenticate
} // namespace requests
} // namespace graduation_project
