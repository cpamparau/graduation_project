#ifndef GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_PROTOBUF_HPP
#define GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_PROTOBUF_HPP

#include "graduation_project/requests/authenticate/interf/authenticate.hpp"
#include "graduation_project/requests/protocols/protobuf.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
/**
 * @brief The protobuf class will be used for the authenticate requests that
 * use the protobuf protocol.
 */
class protobuf: public interf::authenticate, public protocols::protobuf
{
  /**
   * @brief protobuf Default constructor
   */
  protobuf();

public:
  std::string get_token() override;

  /**
   * @brief protobuf Constructor.
   * @param[in] request_msg - Reference to the request_msg object.
   */
  protobuf(const ::graduation_project::proto::request_msg& request_msg);
};
} // namespace authenticate
} // namespace requests
} // namespace graduation_project
#endif // GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_PROTOBUF_HPP
