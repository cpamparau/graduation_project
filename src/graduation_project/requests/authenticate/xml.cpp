#include "graduation_project/requests/authenticate/xml.hpp"
#include <iostream>
namespace graduation_project
{
namespace requests
{
namespace authenticate
{
//----------------------------------------------------------------------------//
std::string xml::get_token()
{
  return token;
}
//----------------------------------------------------------------------------//
xml::xml(rapidxml::xml_node<> &node) : protocols::xml (node)
{
  rapidxml::xml_node<> *node_authenticate = obj_request.first_node(
    NODE_AUTHENTICATE);
  rapidxml::xml_attribute<> *token = node_authenticate->first_attribute(
    ATTRIBUTE_TOKEN);

  this->token = token != nullptr ? std::string(token->value(),
    token->value_size()) : "";
}
//----------------------------------------------------------------------------//
} // namespace authenticate
} // namespace requests
} // namespace graduation_project
