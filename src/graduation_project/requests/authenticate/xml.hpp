#ifndef GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_XML_HPP
#define GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_XML_HPP

#include "graduation_project/requests/authenticate/interf/authenticate.hpp"
#include "graduation_project/requests/protocols/xml.hpp"

namespace graduation_project
{
namespace requests
{
namespace authenticate
{
static LS_ATTR_UNUSED const char* NODE_AUTHENTICATE = "authenticate";
static LS_ATTR_UNUSED const char* ATTRIBUTE_TOKEN = "token";

/**
 * @brief The xml class
 */
class xml: public interf::authenticate, public protocols::xml
{
  /**
   * @brief token String containing the autentication token.
   */
  std::string token;

  /**
   * @brief xml Default constructor.
   */
  xml();

public:

  std::string get_token() override;

  /**
   * @brief xml Constructor.
   * @param[in,out] node - An xml_node  that starts from node request.
   */
  xml(rapidxml::xml_node<>& node);
};
} // namespace authenticate
} // namespace requests
} // namespace logging_server
#endif // GRADUATION_PROJECT_REQUESTS_AUTHENTICATE_XML_HPP
