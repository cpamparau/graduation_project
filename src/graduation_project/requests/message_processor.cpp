#include <iostream>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// ^ ZeroCopyInputStream and ArrayInputStream
#include <sstream>
#include <google/protobuf/util/json_util.h> // MessageToJsonString
#include "graduation_project/requests/message_processor.hpp"
#include "graduation_project/requests/authenticate/xml.hpp"
#include "graduation_project/requests/authenticate/protobuf.hpp"

namespace graduation_project
{
namespace requests
{
//----------------------------------------------------------------------------//
request_ptr message_processor::parse(const char type,
  std::string message_content)
{
  switch (type)
  {
    case 'X':
      return xml(message_content);
    case 'P':
      return protobuf(message_content);
    default:
      //LOG_INFO(messages_log, "Unknown protocol!" << type);
      return nullptr;
  }
}
//----------------------------------------------------------------------------//
request_ptr message_processor::xml(const ::std::string& message_content)
{
  try
  {
    this->document.parse< rapidxml::parse_trim_whitespace |
      rapidxml::parse_non_destructive >( const_cast<char*>(
        message_content.c_str()) );
  }
  catch (std::exception& e)
  {
    //LOG_ERROR(messages_log, "[X]" << e.what() << ": "
    //  << message_content.c_str());
    std::cout << "[X] Parse XML Exception - " << e.what() << ": " <<
      message_content << std::endl;
    return nullptr;
  }
  catch(...)
  {
    //LOG_ERROR(messages_log, "[X]Unknown error: " << message_content.c_str());
    std::cout << "[X]Unknown error: " << message_content << std::endl;
    return nullptr;
  }

  rapidxml::xml_node<> *node = this->document.first_node("request");
  if(node == nullptr)
  {
    return nullptr;
  }

  rapidxml::xml_node<> *child_node = nullptr;
  if( (child_node = node->first_node("authenticate"))!=nullptr)
  {
    return request_ptr(new graduation_project::requests::authenticate::xml(
      *node));
  }
  // LOG - Unknown request_type
  return nullptr;
}
//----------------------------------------------------------------------------//
request_ptr message_processor::protobuf(const std::string &message_content)
{
  ::google::protobuf::io::ZeroCopyInputStream *input = new
    ::google::protobuf::io::ArrayInputStream(message_content.data(),
    static_cast<int>(message_content.size()));
  //::std::string hex_message;

  if ( !protobuf_content.ParseFromZeroCopyStream(input) )
  {
    //string_to_hex(message_content, hex_message);
    //LOG_INFO(messages_log, "[P]Request could not be parsed: 0x"<< hex_message);
    delete input;
    return nullptr;
  }

  ::std::string request_content;
  google::protobuf::util::JsonOptions options;
  options.add_whitespace = true;
  options.always_print_primitive_fields = true;

  delete input;
  if ( protobuf_content.has_authenticate_request() )
  {
    return request_ptr(new requests::authenticate::protobuf(protobuf_content));
  }

  ::google::protobuf::util::MessageToJsonString(protobuf_content,
    &request_content, options);
  //string_to_hex(message_content, hex_message);
  //LOG_INFO(messages_log, "[P]Request could not be parsed: Unknown request type:"
  //  " " << "\n" << request_content << "Hex: 0x" << hex_message);
  return nullptr;
}
//----------------------------------------------------------------------------//
} // namespace requests
} // namespace graduation_project
