#ifndef GRADUATION_PROJECT_MESSAGE_PROCESSOR_HPP
#define GRADUATION_PROJECT_MESSAGE_PROCESSOR_HPP

#include <rapidxml/rapidxml.hpp>
#include "graduation_project/requests/request.hpp"
#include "graduation_project/proto/messages.pb.h"

namespace graduation_project
{
namespace requests
{
/**
 * @brief The message_processor class offers methods that are used to identify
 * the request and parse the document.
 */
class message_processor
{
  /**
   * @brief document is an object from where the parsed xml string will be taken
   */
  ::rapidxml::xml_document<> document;

  /**
   * @brief protobuf_content Is the object where all informations is stored
   * after parsing a string received via protobuf protocol.
   */
  ::graduation_project::proto::request_msg protobuf_content;
public:
  /**
   * @brief parse checks the protocol in which format the data was written than
   * parses the data in order to be easier to access.
   * @param[in] protocol_type - is the code of the protocol.
   * @param[in,out] message_content - the document in serialized format.
   * @return a new instance of the request_type.
   */
  request_ptr parse(const char protocol_type,
    std::string message_content);

  /**
   * @brief xml parses the document and gets the request_type.
   * @param[in,out] message_content - document in format xml, it is modified
   * when an error appears.
   * @return a new instance of the request_type.
   */
  request_ptr xml(const std::string &message_content);

  /**
   * @brief protobuf Parses the string received and returns a shared_ptr to a
   * request.
   * @param[in] message_content - The message content received via websocket.
   * @return Returns a shared pointer to requests::authentication::protobuf
   * instance.
   */
  request_ptr protobuf(const std::string &message_content);
};
} // namespace requests
} // namespace graduation_project
#endif // GRADUATION_PROJECT_MESSAGE_PROCESSOR_HPP
