#include <google/protobuf/util/json_util.h>
#include "graduation_project/requests/protocols/protobuf.hpp"

namespace graduation_project
{
namespace requests
{
namespace protocols
{
//----------------------------------------------------------------------------//
protobuf::protobuf(const ::graduation_project::proto::request_msg& msg):
  request_msg(msg), request_content("")
{
}
//----------------------------------------------------------------------------//
request_id protobuf::get_id()
{
  return request_msg.request_id();
}
//----------------------------------------------------------------------------//
std::string protobuf::to_human_readable()
{
  if( request_content == "" )
  {
    google::protobuf::util::JsonOptions options;
    options.add_whitespace = true;
    options.always_print_primitive_fields = true;
    // By default primitive fields with default values will be omitted in JSON
    // joutput. For example, an int32 field set to 0 will be omitted. Set this
    // flag to true will override the default behavior and print primitive
    // fields regardless of their values. This way, if we have the
    // request_id = 0 it will be printed.
    ::google::protobuf::util::MessageToJsonString(request_msg, &request_content,
      options);
  }
  return request_content;
}
//----------------------------------------------------------------------------//
} // namespace protocols
} // namespace requests
} // namespace graduation_project
