#ifndef GRADUATION_PROJECT_REQUESTS_PROTOCOLS_PROTOBUF_HPP
#define GRADUATION_PROJECT_REQUESTS_PROTOCOLS_PROTOBUF_HPP

#include "graduation_project/requests/request.hpp"
#include "graduation_project/proto/messages.pb.h"

namespace graduation_project
{
namespace requests
{
namespace protocols
{
/**
 * @brief The protocols::protobuf class is an abstract class that implements the
 * get_id() and to_human_readable() which are commons methods among the requests
 * supported by the server.
 */
class protobuf: public virtual graduation_project::requests::request
{

  /**
   * @brief protobuf Default constructor.
   */
  protobuf();
protected:

  /**
   * @brief request_msg Contains the request object in protobuf format.
   */
  const graduation_project::proto::request_msg& request_msg;

  /**
    * @brief request_content String containing the content in JSON format, it is
    * populated in to_human_readable().
    */
  std::string request_content;

public:

   std::string to_human_readable() override;

   request_id get_id() override;

   /**
    * @brief protobuf Constructor.
    * @param[in] request_msg - Reference to the request_msg object.
    */
   protobuf(const ::graduation_project::proto::request_msg& request_msg);
};
} // namespace protocols
} // namespace requests
} // namespace graduation_project
#endif // GRADUATION_PROJECT_REQUESTS_PROTOCOLS_PROTOBUF_HPP
