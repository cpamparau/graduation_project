#include <sstream>
#include "graduation_project/requests/protocols/xml.hpp"

namespace graduation_project
{
namespace requests
{
namespace protocols
{
//----------------------------------------------------------------------------//
xml::xml(rapidxml::xml_node<>& node)
  : obj_request(node)
{
}
//----------------------------------------------------------------------------//
xml::~xml()
{
}
//----------------------------------------------------------------------------//
std::string xml::to_human_readable()
{
  return std::string(obj_request.name() - 1);
}
//----------------------------------------------------------------------------//
request_id xml::get_id()
{
  rapidxml::xml_attribute<>* id = obj_request.first_attribute("id");
  if(id == nullptr || id->value_size() == 0)
  {
    return  req_id_null;
  }
  char *p_id = id->value();
  for(size_t i=0; i<id->value_size(); i++)
  {
    if(!isdigit(p_id[i]))
    {
      return req_id_null;
    }
  }
  // conversion
  std::stringstream val(p_id);
  unsigned int result;
  val>>result;
  return result;
}
//----------------------------------------------------------------------------//
}
}
}
