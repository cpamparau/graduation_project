#ifndef GRADUATION_PROJECT_REQUESTS_PROTOCOLS_XML_HPP
#define GRADUATION_PROJECT_REQUESTS_PROTOCOLS_XML_HPP

#include <rapidxml/rapidxml.hpp>
#include "graduation_project/requests/request.hpp"

namespace graduation_project
{
namespace requests
{
namespace protocols
{
static LS_ATTR_UNUSED const char* NODE_REQUEST = "request";

class xml: public virtual graduation_project::requests::request
{
public:
  /**
   * @brief xml Constructor.
   * @param[in] node - An xml node with the starting point, node request.
   * !!!IMPORTANT - When the refference is setted to the xml_node upcoming
   * calls to the xml_node does not check if the node request or the first child
   * example: authenticate, exists.
   */
  xml(rapidxml::xml_node<>& node);

  /**
   * @brief ~xml Destructor.
   */
  virtual ~xml();

  std::string to_human_readable() override;

  request_id get_id() override;

protected:
  /**
   * @brief request variable which keeps the xml request.
   */
  rapidxml::xml_node<>& obj_request;
};
} // namespace protocols
} // namespace requests
} // namespace graduation_project
#endif // GRADUATION_PROJECT_REQUESTS_PROTOCOLS_XML_HPP
