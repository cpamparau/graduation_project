#ifndef GRADUATION_PROJECT_REQUESTS_REQUEST_FPP
#define GRADUATION_PROJECT_REQUESTS_REQUEST_FPP
#include <memory>
#include "graduation_project/utils.hpp"

namespace graduation_project
{
namespace requests
{

class request
{
public:
  virtual ~request();

  /**
   * @brief get_type This method will return the type of request, which
   * will be a value from an enum with all the possible request types. Examples
   * of request types are: authentication request, email request etc. The
   * request type will be hardcoded into each class derived from this one.
   * @return Will return the request type.
   */
  virtual request_type get_type() = 0;

  /**
   * @brief get_id This method will return the request id. Each request
   * must have a request ID. The request_id is a typedef of unsigned int.
   * @return Will return the request id.
   */
  virtual request_id get_id() = 0;

  /**
   * @brief to_human_readable This method will generate a human readable
   * representation of the request and it will return it as a string.
   * @return Will return the request as a string which will be human readable.
   */
  virtual std::string to_human_readable() = 0;
};
} // namespace requests
/**
 * @brief request_ptr Shared ptr to a request object.
 */
typedef std::shared_ptr<requests::request> request_ptr;
} // namespace graduation_project
#endif // GRADUATION_PROJECT_REQUESTS_INTERF_FPP
