#include <google/protobuf/util/json_util.h>
#include "graduation_project/responses/protobuf.hpp"

namespace graduation_project
{
namespace responses
{
//----------------------------------------------------------------------------//
protobuf::protobuf()
{
  response_msg.set_allocated_response_with_message(
    new ::graduation_project::proto::response_with_message());
}
//----------------------------------------------------------------------------//
protobuf::~protobuf()
{
}
//----------------------------------------------------------------------------//
void protobuf::set_status(const bool b_succes)
{
  response_msg.set_status(b_succes? "ok" : "error");
}
//----------------------------------------------------------------------------//
void protobuf::set_message(const ::std::string& message)
{
  response_msg.mutable_response_with_message()->set_message(message);
}
//----------------------------------------------------------------------------//
void protobuf::set_id(const ::graduation_project::request_id id)
{
  response_msg.set_request_id(id);
}
//----------------------------------------------------------------------------//
request_id protobuf::get_id()
{
  return response_msg.request_id();
}
//----------------------------------------------------------------------------//
bool protobuf::get_status()
{
  return response_msg.status() == "ok" ? true : false;
}
//----------------------------------------------------------------------------//
std::string protobuf::get_message()
{
  return std::string(
    response_msg.release_response_with_message()->message());
}
//----------------------------------------------------------------------------//
std::string protobuf::to_human_readable()
{
  google::protobuf::util::JsonOptions options;
  std::string response_json;
  options.add_whitespace = true;
  options.always_print_primitive_fields = true;
  ::google::protobuf::util::MessageToJsonString(response_msg, &response_json,
    options);
  return std::string(response_json);
}
//----------------------------------------------------------------------------//
std::string protobuf::serialize()
{
  ::std::string response_json;
  response_msg.SerializeToString(&response_json);
  return response_json;
}
//----------------------------------------------------------------------------//
} // namespace responses
} // namespace graduation_project
