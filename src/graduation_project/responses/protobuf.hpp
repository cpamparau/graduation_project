#ifndef GRADUATION_PROJECT_RESPONSES_PROTOBUF_HPP
#define GRADUATION_PROJECT_RESPONSES_PROTOBUF_HPP

#include "graduation_project/responses/response.hpp"
#include "graduation_project/proto/messages.pb.h"
#include "graduation_project/utils.hpp"

namespace graduation_project
{
namespace responses
{
/**
 * @brief The protobuf class implements all methods used to convert a protobuf
 * object into a string.
 */
class protobuf: public responses::response
{
  /**
    * @brief response_msg The object that contains the response content in
    * protobuf format.
    */
  ::graduation_project::proto::response_msg response_msg;

public:
  /**
    * @brief ~protobuf Destructor.
    */
  virtual ~protobuf();

  /**
   * @brief protobuf Constructor
   */
  protobuf();

  std::string to_human_readable() override;

  std::string serialize() override;

  void set_status(const bool b_success) override;

  bool get_status() override;

  void set_id(const ::graduation_project::request_id value) override;

  ::graduation_project::request_id get_id() override;

  void set_message(const ::std::string& message) override;

  std::string get_message() override;
};
} // namespace responses
} // namespace graduation_project
#endif // GRADUATION_PROJECT_RESPONSES_PROTOBUF_HPP
