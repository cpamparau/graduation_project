#ifndef GRADUATION_PROJECT_RESPONSES_RESPONSE_HPP
#define GRADUATION_PROJECT_RESPONSES_RESPONSE_HPP

#include <memory>
#include "graduation_project/utils.hpp"

namespace graduation_project
{
namespace responses
{

class response
{
public:
  /**
   * @brief ~response Default destructor
   */
  virtual ~response();

  /**
   * @brief set_id This method sets response id to match a certain request ID.
   *
   * @note If we call this method with the value of the argument bigger than
   * `ULONG_MAX`,  let's say `ULONG_MAX + x`, we will only get `x-1` in the
   * `value` variable.
   *
   * @param[in] value - The request id.
   */
  virtual void set_id(const graduation_project::request_id value) = 0;

  /**
   * @brief set_status This method sets the response status: true if the request
   * was processed successfully.
   * @param[in] b_success - Status of the processing operation of the request.
   */
  virtual void set_status(const bool status) = 0;

  /**
   * @brief serialize This method will return a string representing the
   * informations in a format asked by the protocol (xml format etc.).
   * @return Returns a string in a custom format.
   */
  virtual std::string serialize() = 0;

  /**
   * @brief get_id This method gets the response id. If the id is not set, it
   * will return req_id_null value.
   * @return The response id if this was set, or req_null_id otherwise.
   */
  virtual graduation_project::request_id get_id() = 0;

  /**
   * @brief get_status This method gets the value of the status parameter, true
   * or false. If the status contains "ok" it will return true, if it's not set,
   * or is contains value "error" it will return false.
   * @return The status. True or false.
   * @throw An exception is thrown (with message "status not set.") if the
   * status was not set
   */
  virtual bool get_status() = 0;

  /**
   * @brief to_human_readable This method will generate a human readable
   * representation of the response and it will return it as a string.
   * @return Will return the response as a string which will be human readable.
   */
  virtual std::string to_human_readable() = 0;

  /**
   * @brief get_message
   * @return Will return the response as a string
   */
  virtual std::string get_message() = 0;

  /**
   * @brief set_message This method sets the response message.
   * @param[in] message - The content of the response message.
   */
  virtual void set_message(const ::std::string& message) = 0;
};
} // namespace responses
/**
 * @brief response_ptr Shared ptr to a response object.
 */
typedef std::shared_ptr<responses::response> response_ptr;
} // namespace graduation_project
#endif // GRADUATION_PROJECT_RESPONSES_RESPONSE_HPP
