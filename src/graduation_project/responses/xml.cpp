#include <sstream>
#include <cstring>
#include <rapidxml/rapidxml_print.hpp>
#include "graduation_project/responses/xml.hpp"

using namespace ::rapidxml;

namespace graduation_project
{
namespace responses
{
//----------------------------------------------------------------------------//
xml::xml() : status(nullptr), message("")
{
  xml_node<>* response = xml_doc.allocate_node(node_element, NODE_RESPONSE);
  xml_attribute<>* attr_id = xml_doc.allocate_attribute(ATTRIBUTE_ID);
  xml_attribute<>* attr_status = xml_doc.allocate_attribute(ATTRIBUTE_STATUS);
  response->append_attribute(attr_id);
  response->append_attribute(attr_status);
  xml_node<>* message = xml_doc.allocate_node(node_element, NODE_MESSAGE);
  xml_attribute<>* attr_err = xml_doc.allocate_attribute(ATTRIBUTE_ERR);
  message->append_attribute(attr_err);
  response->append_node(message);
  xml_doc.append_node(response);
}
//----------------------------------------------------------------------------//
xml::~xml()
{
}
//----------------------------------------------------------------------------//
std::string xml::to_human_readable()
{
  return serialize();
}
//----------------------------------------------------------------------------//
std::string xml::serialize()
{
  ::std::stringstream response_content;
  // set rapidxml::print_no_indenting as 3rd parameter for no identation.
  rapidxml::print(static_cast<std::ostream&>(response_content), xml_doc);
  return response_content.str();
}
//----------------------------------------------------------------------------//
std::string xml::get_message()
{
  return message;
}
//----------------------------------------------------------------------------//
void xml::set_status(const bool b_success)
{
  this->status = b_success == true ? STATUS_OK : STATUS_ERROR;
  xml_doc.first_node(NODE_RESPONSE)->first_attribute(ATTRIBUTE_STATUS)->value(
    status);
}
//----------------------------------------------------------------------------//
bool xml::get_status()
{
  if(status == nullptr)
  {
    throw "status not set.";
  }
  return strcmp(status, STATUS_OK) == 0 ? true : false;
}
//----------------------------------------------------------------------------//
::graduation_project::request_id xml::get_id()
{
  if(request_id == "")
  {
    return requests::req_id_null;
  }
  // conversion
  std::stringstream val(request_id.c_str());
  unsigned int result;
  val>>result;
  return result;
}
//----------------------------------------------------------------------------//
void xml::set_id(const ::graduation_project::request_id value)
{
  request_id = std::to_string(value);
  xml_doc.first_node(NODE_RESPONSE)->first_attribute(ATTRIBUTE_ID)->value(
    request_id.c_str());
}
//----------------------------------------------------------------------------//
void xml::set_message(const ::std::string& message)
{
  this->message = message;
  xml_doc.first_node(NODE_RESPONSE)->first_node(NODE_MESSAGE)->first_attribute(
    ATTRIBUTE_ERR)->value(this->message.c_str());
}
//----------------------------------------------------------------------------//
} // namespace responses
} // namespace graduation_project
