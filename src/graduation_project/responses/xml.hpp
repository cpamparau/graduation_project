#ifndef GRADUATION_PROJECT_RESPONSES_XML_HPP
#define GRADUATION_PROJECT_RESPONSES_XML_HPP

#include <rapidxml/rapidxml.hpp>
#include "graduation_project/responses/response.hpp"

namespace graduation_project
{
namespace responses
{
static LS_ATTR_UNUSED const char* NODE_RESPONSE = "response";
static LS_ATTR_UNUSED const char* NODE_MESSAGE = "message";

static LS_ATTR_UNUSED const char* ATTRIBUTE_ID = "id";
static LS_ATTR_UNUSED const char* ATTRIBUTE_STATUS = "status";
static LS_ATTR_UNUSED const char* ATTRIBUTE_ERR = "err";

static LS_ATTR_UNUSED const char* STATUS_OK = "ok";
static LS_ATTR_UNUSED const char* STATUS_ERROR = "error";

class xml: public graduation_project::responses::response
{
  /**
    * @brief xml_doc The xml document.
    */
  rapidxml::xml_document<> xml_doc;

  /**
   * @brief status This sets the current status attribute value.
   */
  const char* status;

  /**
   * @brief request_id This sets the current id attribute value.
   */
  ::std::string request_id;

  /**
   * @brief message This sets the current message err attribute.
   */
  ::std::string message;

public:

  xml();

  virtual ~xml();

  void set_id(const graduation_project::request_id value) override;

  void set_status(const bool value) override;

  std::string serialize() override;

  graduation_project::request_id get_id() override;

  bool get_status() override;

  std::string to_human_readable() override;

  std::string get_message() override;

  void set_message(const ::std::string& message) override;
};
} // namespace responses
} // namespace graduation_project

#endif // GRADUATION_PROJECT_RESPONSES_XML_HPP
