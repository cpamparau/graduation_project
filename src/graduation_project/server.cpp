#include "graduation_project/server.hpp"
#include <string>
#include <mutex>
#include <functional>
#include <ctime>
#include <string.h>
#include "socket/socket_errors.hpp"
#include "graduation_project/settings/settings.hpp"
#include "graduation_project/request_processor.hpp"
#include <exception>

using namespace graduation_project::socket;
using namespace std;

namespace graduation_project
{
//----------------------------------------------------------------------------//
server::~server()
{
  stop();
}
//----------------------------------------------------------------------------//
server::server(unsigned int _port, std::string _ip,
  graduation_project::database::interf::database_system& mysql_system,
  database::database_connection_info &mysql_info):
  mysql_system(mysql_system), mysql_info(mysql_info)
{
  ip = _ip;
  port = _port;
}
//----------------------------------------------------------------------------//
void server::start()
{
  Socket server_socket;
  InetPort p(port);
  InetHost h(ip);
  InetSocketAddress isa(p, h);
  server_socket.bind(isa);
  server_socket.listen();
  cout << "Server is waiting for a connection..." << endl;
  while (1)
  {
    Socket client_socket = server_socket.accept();
    if (client_socket.is_valid())
    {
      client_threads.push_back(make_shared<::std::thread>(
        ::std::thread(::std::bind(&server::server_client_handler_thread,
          this,client_socket))));
    }
  }
}
//----------------------------------------------------------------------------//
void server::server_client_handler_thread(Socket client_socket)
{
  try{
    cout << "Another client from: " << client_socket.getpeername().id(0)
        << endl;
    //read from client
    char buffer[1024];
    request_processor processor(mysql_info, mysql_system, client_socket);
    do
    {
      //read from client
      int no_bytes_read = client_socket.recv(buffer, 1024);
      buffer[no_bytes_read - 2] = '\0';
      connection.protocol = static_cast<unsigned char>(buffer[0]);
      try
      {
        processor.handle_message(connection, buffer);
      }
      catch (exception &e)
      {
          std::cout << e.what() << endl;
      }
      //strcpy(message, "Command completed successfully \n");
      //cout << "The message for client is :" << message << endl;
      //message[strlen(message)] = '\0';
      //client_socket.send_n(message, strlen(message), strlen(message));
      //LOG(message);
      //client_socket.send_n(response.c_str(), response.size(), response.size());
      //string resp = "Server response: " + response;
      //LOG(resp);

    }
    while (strcmp(buffer, "exit") != 0);

    client_socket.close();
  }catch (SocketError & e)
  {
      cout << "Client Socket Exception : " << e.what() << endl;
  }catch(...)
  {
      cout << "General (unknown) Exception" << endl;
  }
}
//----------------------------------------------------------------------------//
void server::stop()
{
  for(auto &thr : client_threads)
  {
    thr->join();
  }
  client_threads.clear();
  // log the closing of server
}
//----------------------------------------------------------------------------//
} // namespace graduation_project
