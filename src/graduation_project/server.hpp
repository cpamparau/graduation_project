#ifndef GRADUATION_PROJECT_SERVER_HPP
#define GRADUATION_PROJECT_SERVER_HPP

#include <iostream>
#include <atomic>
#include <thread>
#include <vector>
#include "socket/socket.hpp" // Socket
#include "socket/address.hpp" // Inet bind stuff
#include "graduation_project/database/interf/database_system.hpp"
#include "graduation_project/utils.hpp"

namespace graduation_project
{
/**
 * shared pointer to a thread
 */
typedef ::std::shared_ptr<::std::thread> thread_ptr;
/**
 * @brief class that will start a connection between server and clients
 */
class server
{
  /**
   * @brief the port on which the server will listen for connections
   */
  unsigned int port;
  /**
   * @brief the ip on which server will listen for connections
   */
  std::string ip;

  void stop();

public:
  /**
   * @brief The default constructor
   */
  server();

  /**
   * @brief Default destructor
   */
  virtual ~server();

  /**
   * @brief constructor with parameters thats sets the port number for
   * which the socket should listen
   * @param _ip - the ip address on the socket should listen
   * @param _port - the port number on which the socket should listen
   */
  server(unsigned int _port, std::string _ip,
    graduation_project::database::interf::database_system& mysql_system,
    graduation_project::database::database_connection_info& mysql_info);

  /**
   *@brief Instanties the socket that will listen on a port for incoming
   * connections.
   */
  void start();

  /**
   *@brief A method that runs in a separate thread. It will process
   *incoming stream data, and will provide response to client
   *@param client_socket - the socket on which this method will receive
   *the request and will send the response
   */
  void server_client_handler_thread(
    graduation_project::socket::Socket client_socket);

protected:
  /**
   * @brief mysql_system object used to communicate with database.
   */
  graduation_project::database::interf::database_system& mysql_system;

  graduation_project::database::database_connection_info mysql_info;

  std::vector< thread_ptr > client_threads;

  connection_hdl connection;
};
} // namespace graduation_project
#endif // GRADUATION_PROJECT_SERVER_HPP
