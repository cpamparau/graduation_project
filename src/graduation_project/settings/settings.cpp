#include <sstream>
#include <algorithm>
#include "graduation_project/settings/settings.hpp"

using namespace ::std;
using namespace ::graduation_project::file::interf;

namespace graduation_project
{
//----------------------------------------------------------------------------//
settings::settings()
{
}
//----------------------------------------------------------------------------//
settings& settings::get()
{
  static settings instance;
  return instance;
}
//----------------------------------------------------------------------------//
string& settings::operator[](const string &parameter)
{
  return parameters[parameter];
}
//----------------------------------------------------------------------------//
parameters_map &settings::get_param_map()
{
  return parameters;
}
//----------------------------------------------------------------------------//
void settings::save_to_file(const string &file_name, class file &f)
{
  f.open(file_name.c_str(), ::std::ios_base::out);
  if (!f.good())
  {
    return;
  }
  parameters_map::iterator it;
  stringstream interm;
  for (it = parameters.begin(); it != parameters.end(); it++)
  {
    const string &key = string("-") + it->first + string(":\"");
    const string &val = it->second + string("\"");
    interm << key;
    interm << val;
    interm << endl;
  }
  string content = interm.str();
  if (content != "")
  {
    f << interm.str();
  }
  f.close();
}
//----------------------------------------------------------------------------//
string settings::load_param_file(const string &file_name, class file &f)
{
  try
  {
    f.open(file_name.c_str(), ::std::ios_base::in);
  }
  catch (...)
  {
    cerr << "ERROR! File not found!" << endl;
  }

  if (!f.good())
  {
    return "";
  }
  string content = f.read_all_contents();
  f.close();
  return content;
}
//----------------------------------------------------------------------------//
void settings::load_params_from_file(const string &file_name, class file &f)
{
  string content = load_param_file(file_name, f);
  load_params(content);
}
//----------------------------------------------------------------------------//
void settings::load_params(::std::string input)
{
  parse(input, parameters);
}
//----------------------------------------------------------------------------//
void settings::parse(string s_input, parameters_map &params)
{
  size_t pos = 0;
  std::string token = "", key = "", value = "";
  size_t length = 0;
  bool is_value = false;
  while((pos = check_delimiters(s_input, length))!= std::string::npos ||
    !s_input.empty())
  {
    if(key != "")
      is_value = true;
    if(pos == std::string::npos)
      token = s_input;
    else
      token = s_input.substr(0, pos);
    trim_whitespaces(token);
    if(token == "")
    {
      s_input.erase(0,1);
      continue;
    }
    if( !is_value)
    {
      if(token[0] == '-')
        token.erase(0,1);
      key = token;
    }
    else
    {
      size_t pos = token.find("\"");
      if(pos != std::string::npos)
        token.erase(pos, pos+1);
      value = token;
      params[key] = value;
      key = "";
      value = "";
      is_value = false;
    }

    if(pos == std::string::npos)
      s_input = "";
    else
      s_input.erase(0, pos + length);
  }
}
//----------------------------------------------------------------------------//
size_t settings::check_delimiters(string s_input, size_t& length)
{
  string delim1 = ":";
  string delim2 = "\"";
  string delim3 = "\n";
  string delim4 = "-";
  size_t pos_1 = s_input.find(delim1);
  size_t pos_2 = s_input.find(delim2);
  size_t pos_3 = s_input.find(delim3);
  size_t pos_4 = s_input.find(delim4);
  string token = "";
  if((pos_1 == pos_2) && (pos_2 == pos_3) && (pos_3 == pos_4) &&
     (pos_4== std::string::npos))
    return std::string::npos;
  size_t min = std::min(pos_1, std::min(pos_2, std::min(pos_3, pos_4)));
  if(min == pos_1)
  {
    length = delim1.size();
    return pos_1;
  }
  if(min == pos_2)
  {
    length = delim2.size();
    if(pos_2 == 0)
    {
      size_t poss = s_input.find("\"", 1);
      if(poss!=std::string::npos)
      {
        return poss;
      }
    }
    return pos_2;
  }
  if(min == pos_3)
  {
    length = delim3.size();
    return pos_3;
  }
  if(min == pos_4)
  {
    length = delim4.size();
    if(pos_4 == 0)
    {
      size_t poss = s_input.find(":");
      if(poss!=std::string::npos)
      {
        return poss;
      }
    }
    else
    {
      return pos_4;
    }
  }
  length = 0;
  return std::string::npos;
}
//----------------------------------------------------------------------------//
void settings::trim_whitespaces(std::string& input)
{
  size_t pos = 0;
  size_t input_size = input.size();
  while(pos!= input_size)
  {
    if(input[pos] == ' ')
    {
      input.erase(pos, pos+1);
      pos++;
    }
    else
    {
      pos++;
      continue;
    }
  }
}
//----------------------------------------------------------------------------//
}
