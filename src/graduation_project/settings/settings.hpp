#ifndef GRADUATION_PROJECT_SETTINGS_HPP
#define GRADUATION_PROJECT_SETTINGS_HPP

#include <map>
#include <string>
#include "graduation_project/file/interf/file.hpp"

namespace graduation_project
{
/** Map of parameters. */
typedef ::std::map< ::std::string, ::std::string > parameters_map;

/**
 * @brief The settings class holds the general settings for app such
 * as .... (to be completed). This class
 * is a singleton one, so, in order to use it, use the get() method.
 */
class settings
{
  /**
   * @brief Settings constructor is kept private; please use get() method
   * to retrieve the singleton instance of this class
   */
  settings();

  /** Holds a list of parameter-value pairs. */
  parameters_map parameters;

  static size_t check_delimiters(std::string s_input, size_t& length);

  static void trim_whitespaces(std::string& input);

public:
  /**
   * @brief Deletes copy constructor to make sure a singleton instance can not
   * be copied.
   */
  settings(settings const&) = delete;

  /**
   * @brief Deletes assignment operator to make sure a singleton instance can
   * not be copied.
   */
  void operator=(settings const&) = delete;

  /**
   * Load content of a parameter file to string.
   * @note A line must not exceed 1024 characters.
   * @param file_name - path to the file
   * @param f - an reference that handles working with mock object or working
   * with files
   * @return - content of the parameter file as string.
   */
  ::std::string load_param_file(const ::std::string &file_name,
    ::graduation_project::file::interf::file &f);

  /**
   * Get the singleton instance of this class.
   * @return the singleton instance of this class.
   */
  static settings &get();

  /**
   * Load a list of parameters from file
   * @param f - an reference that handles working with mock object or working
   * with files
   * @param file_name - path to the file.
   */
  void load_params_from_file(const ::std::string &file_name,
    ::graduation_project::file::interf::file &f);

  /**
   * Return the value for parameter.
   * @param parameter - parameter name
   * @return value for parameter. if parameter is not defined return
   * empty string.
   */
  ::std::string& operator[](const ::std::string& parameter);

  /**
   * Get param map
   * @return - param map
   */
  parameters_map &get_param_map();

  /**
   * @brief save to file_name file the content of paramMap map
   * @param f - an reference that handles working with mock object or working
   * with files
   * @param file_name - the name of file
   */
  void save_to_file(const ::std::string &file_name,
    ::graduation_project::file::interf::file &f);

  /**
   * Load parameters from command line string.
   * @param[in] input - a string from which you want to load the params.
   */
  void load_params(::std::string input);

  /**
   * @brief Method used to parse command line arguments.
   * Format:
   * -mysqluser:root
   * -max_connections:2
   * -log:"D:\\Projects\\Market Data Proxy\\logs"
   *
   * Usage example:
   * @code{.cpp}
   * command_line_parser c;
   * param_map map;
   * c.parse("-mode:tes102t "
   *  "-gtest_filter:testareee"
   *  "-directory:build"
   *  "-another_style:234"
   *  "-log_directory:logs", map);
   * @endcode
   *
   * Parse the specified input string and fill the map with parameters.
   * @note The params must not contain '-' char!
   * @param s_input - string to parse
   * @param params - map of parameters
   * @return true if string successfully parsed, false in case of error.
   */
  static void parse(::std::string s_input, parameters_map &params);
};
} // namespace graduation_project

#endif // GRADUATION_PROJECT_SETTINGS_HPP
