#include "graduation_project/socket/address.hpp"
#include "graduation_project/socket/socket_errors.hpp"

#include <assert.h>
#include <stdexcept>
#include <stdio.h>

#include <string.h>
#include <stdlib.h>

#define is_digit(x) ((x)>='0' && (x)<='9'?true:false)

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
SocketType::SocketType(int family, int type, int protocol) :
    address_family_(family), type_(type), protocol_(protocol)
{
}
//----------------------------------------------------------------------------//
SocketType::SocketType(const string &family, const string &type,
  int protocol) :
    protocol_(protocol)
{
  address_family_ = str_to_family(family);
  type_ = str_to_type(type);
}
//----------------------------------------------------------------------------//
int SocketType::str_to_type(const string &str)
{
  if (str == "stream")
      return SOCK_STREAM;
  else if (str == "dgram")
      return SOCK_DGRAM;
  else if (str == "rdm")
      return SOCK_RDM;
  else if (str == "seqpacket")
      return SOCK_SEQPACKET;
  else if (str == "raw")
      return SOCK_RAW;
  else
      throw UnknownSocketType(str.c_str());
}
//----------------------------------------------------------------------------//
int SocketType::str_to_family(const string &str)
{
  if (str == "inet")
      return AF_INET;
  else
      UnknownSocketFamily(str); //others family unsuported
  return AF_UNSPEC;
}
//----------------------------------------------------------------------------//
int SocketType::address_family() const
{
  return address_family_;
}
//----------------------------------------------------------------------------//
int SocketType::protocol() const
{
  return protocol_;
}
//----------------------------------------------------------------------------//
int SocketType::type() const
{
    return type_;
}
///////////////////////////////////////////////////////////////////////////////
// InetSocketType
///////////////////////////////////////////////////////////////////////////////

InetSocketType::InetSocketType(const string &type, int protocol) :
  SocketType("inet", type, protocol)
{
}

InetSocketType::InetSocketType(int type, int protocol) :
    SocketType(AF_INET, type, protocol)
{
}

///////////////////////////////////////////////////////////////////////////////
// InetPort
///////////////////////////////////////////////////////////////////////////////

InetPort::InetPort(unsigned int port) :
  port_(port)
{
  port_flag_ = true;
  service_flag_ = false;
}
//----------------------------------------------------------------------------//
InetPort::InetPort(const string &service)
{
  bool nr_flag = true;
  int n = static_cast<int>(service.length());
  const char *str = service.c_str();
  for (int i = 0; i < n; i++)
  {
    if (!is_digit(str[i]))
    {
      nr_flag = false;
      break;
    }
  }

  if (nr_flag)
  {
    port_ = atoi(service.c_str());
    port_flag_ = true;
    service_flag_ = false;
  }
  else
  {
    service_ = service;
    port_flag_ = false;
    service_flag_ = true;
  }
}
//----------------------------------------------------------------------------//
unsigned int InetPort::port()
{
  if (!port_flag_)
  {
    if (service_flag_)
    {
      servent *s = getservbyname(service_.c_str(), 0);
      if (s)
        port_ = ntohs(static_cast<uint16_t>(s->s_port));
      else
        throw ServiceNotAvailable(service_.data());
    }
    port_flag_ = true;
  }
  return port_;
}
//----------------------------------------------------------------------------//
string InetPort::service_name()
{
  if (!service_flag_)
  {
    if (port_flag_)
    {
      servent *s = getservbyport(port_, 0);
      if (s)
      {
        service_ = s->s_name;
      }
    }
    service_flag_ = true;
  }
  return service_;
}
//----------------------------------------------------------------------------//
string InetPort::id(int level)
{
  string ret;
  if (level == 0)
  {
    if (port_flag_)
      ret = _int_to_str(port_);
    else
      ret = service_;
  }
  else
  {
    port();
    service_name();
    ret = _int_to_str(port_);
    ret += "(";
    ret += service_;
    ret += ")";
  }
  return ret;
}
//----------------------------------------------------------------------------//
string InetPort::_int_to_str(int i)
{
  string ret;
  char str[64];
#ifdef WIN32
  _itoa_s(i, str, 10);
#else
  sprintf(str, "%d", i);
#endif
  ret = str;
  return ret;
}

///////////////////////////////////////////////////////////////////////////////
// InetHost
///////////////////////////////////////////////////////////////////////////////

InetHost::InetHost(in_addr_t addr) :
  address_(addr), address_flag_(true), name_flag_(false)
{
  if (address_ == INADDR_ANY)
  {
    name_ = "ANY";
    name_flag_ = true;
  }
  else
    name_ = ip_to_string();
}
//----------------------------------------------------------------------------//
InetHost::InetHost(const string &name) :
        address_(INADDR_ANY), name_(name), address_flag_(false), name_flag_(
            true)
{
  if (name_.empty())
    name_ = "ANY";

  if (name_ == "ANY")
  {
    address_ = INADDR_ANY;
    address_flag_ = true;
    return;
  }
#ifdef WIN32
  //check to see if the name string is an dotted IP address
  address_ = inet_addr(name.c_str());

  if (int (address_)!=-1)
  {
      address_flag_ = true;
      name_flag_ = false;
  }
#else
  //check to see if the name string is an dotted IP address
  in_addr a;
  //a.s_addr = address_;
  int ret = inet_aton(name.c_str(), &a);

  if (ret != 0) //it is an valid address
  {
      address_flag_ = true;
      name_flag_ = false;
      address_ = a.s_addr;
  }
#endif
}
//----------------------------------------------------------------------------//
bool InetHost::is_addr_any() const
{
  return (address_flag_ && address_ == INADDR_ANY) ? true : false;
}
//----------------------------------------------------------------------------//
in_addr_t InetHost::address()
{
  if (!address_flag_)
  {
    if (name_flag_)
    {
#if defined(WIN32) || defined (MAC_OS)
      hostent *h = gethostbyname(name_.c_str());
      if( h )
      {
        if (h->h_addrtype != AF_INET)
        throw UnknownSocketFamily("not inet family");

        address_ = ((in_addr*)(h->h_addr_list[ 0 ]))->s_addr;
        name_ = h->h_name;
      }
      else throw UnknownHost(name_.c_str());
#else
      hostent h;
      char buf[1024];
      hostent *res;
      int errnum;

      int iRet = gethostbyname_r(name_.c_str(), &h, buf, sizeof(buf),
          &res, &errnum);

      if (iRet != 0)
      {
        throw UnknownHost(name_.c_str());
      }


      if (res)
      {
        if (res->h_addrtype != AF_INET)
          throw UnknownSocketFamily("not inet family");

        address_ = (reinterpret_cast<in_addr*>(
          res->h_addr_list[0]))->s_addr;
        name_ = res->h_name;
      }
      else
        throw UnknownHost(name_.c_str());
#endif
    }
    address_flag_ = true;
  }
  return address_;
}
//----------------------------------------------------------------------------//
string InetHost::name()
{
   if (!name_flag_)
   {
     if (address_flag_)
     {
#if defined(WIN32) ||  defined (MAC_OS)
       hostent *h;
       h = gethostbyaddr(reinterpret_cast<char*>(&address_),
           sizeof(in_addr_t), AF_INET);
       if (h)
       {
           if (h->h_addrtype!=AF_INET)
           throw UnknownSocketFamily("not inet family");

           name_ = h->h_name;
       }
       else throw UnknownHost("bad network address");

#else
       hostent h;
       hostent *res;
       char buf[1024];
       int errnum;

       int iRet = gethostbyaddr_r(static_cast<char*>(
           static_cast<void*>(&address_)), sizeof(in_addr_t), AF_INET,
           &h, buf, sizeof(buf), &res, &errnum);

       if (iRet != 0)
       {
           throw UnknownHost("bad network address");
       }

       if (res)
       {
           if (res->h_addrtype != AF_INET)
               throw UnknownSocketFamily("not inet family");

           name_ = res->h_name;
       }
       else
           throw UnknownHost("bad network address");
#endif
     }
     name_flag_ = true;
  }
  return name_;
 }
//----------------------------------------------------------------------------//
string InetHost::id(int level)
{
  string ret;
  if (level == 0)
  {
    ret = name_;
  }
  else
  {
    name();
    address();
    ret = name_;
    ret += "(";
    ret += ip_to_string();
    ret += ")";
  }
  return ret;
}
//----------------------------------------------------------------------------//
string InetHost::ip_to_string()
{
  in_addr addr;
  addr.s_addr = address_;
  return inet_ntoa(addr);
}

///////////////////////////////////////////////////////////////////////////////
// InetSocketAddress
///////////////////////////////////////////////////////////////////////////////

InetSocketAddress::_address_parser::_address_parser(const char *str,
  char c1, char c2, char del1, char del2)
{
  index_ = string_ = new char[strlen(str) + 1];

  //copy the string
  while (*str)
  {
    //skip the comments
    if (*str == del1)
    {
        str++;
        int level = 1;
        while (*str && level > 0)
        {
            if (*str == del1)
                level++;

            if (*str == del2)
                level--;

            str++;
        }
        if (*str == 0)
            break;
      }

    *index_ = *str == c1 ? c2 : *str;
    index_++;
    str++;
  }
  *index_ = 0;
  index_ = string_;
}
// ---------------------------------------------------------------------------//
InetSocketAddress::_address_parser::~_address_parser()
{
  delete[] string_;
}
//----------------------------------------------------------------------------//
string InetSocketAddress::_address_parser::get_token(char del)
{
  //there could be more del at the begining
  while (*index_ == del)
      index_++;

  const char *p = index_;

  while (*index_ && *index_ != del)
      index_++;

  return string(p, index_ - p);
}
//----------------------------------------------------------------------------//
InetSocketAddress::InetSocketAddress(const InetPort &port,
   const InetHost &host, int t, int protocol) :
   host_(host), port_(port), type_(t, protocol), sockaddr_flag_(false)
{
}
//----------------------------------------------------------------------------//
InetSocketAddress::InetSocketAddress(const string &str) :
  sockaddr_flag_(false)
{
  const int max_tokens = 4; //maximum tokens
  string tokens[max_tokens];
  string token;
  int n = 0;

  _address_parser parser(str.c_str(), ':', ' ');
  do
  {
    token = parser.get_token();
    if (token.empty())
    {
      break;
    }
    else
    {
      if (n >= max_tokens)
          throw std::out_of_range(
              "wrong token numbers, "
              "InetSocketAddress::InetSocketAddress( const String& )"
          );

        tokens[n++] = token;
    }
  } while (1);
  string *p = tokens;

  if (tokens[0] == "inet")
  {
    n--;
    p = tokens + 1;
  }

  memset(static_cast<void*>(&sockaddr_), 0, sizeof(sockaddr_in));
  sockaddr_.sin_family = AF_INET;

  if (n == 1)
  {
    port_ = p[0];
  }
  else if (n == 2)
  {
    host_ = p[0];
    port_ = p[1];
  }
  else if (n == 3)
  {
    type_ = p[0];
    host_ = p[1];
    port_ = p[2];
  }
  else
  {
    throw std::out_of_range("wrong token numbers");
  }
}
//----------------------------------------------------------------------------//
InetSocketAddress::InetSocketAddress(const sockaddr_in &addr, int t,
  int protocol) :
  type_(t, protocol), sockaddr_(addr), sockaddr_flag_(true)
{
  port_ = ntohs(sockaddr_.sin_port);
  host_ = sockaddr_.sin_addr.s_addr;
}
//----------------------------------------------------------------------------//
InetSocketAddress::operator const sockaddr_in*() const
{
  if (sockaddr_flag_ == false)
  {
    sockaddr_in *addr = const_cast<sockaddr_in*>(&sockaddr_);
    InetPort *port = const_cast<InetPort*>(&port_);
    InetHost *host = const_cast<InetHost*>(&host_);
    bool *flag = const_cast<bool*>(&sockaddr_flag_);

    memset(static_cast<void*>(addr), 0, sizeof(sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_port = htons(port->port());
    addr->sin_addr.s_addr = host->address();
    *flag = true;
  }
  return &sockaddr_;
}
//----------------------------------------------------------------------------//
InetHost InetSocketAddress::host() const
{
  return host_;
}
//----------------------------------------------------------------------------//
InetPort InetSocketAddress::port() const
{
  return port_;
}
//----------------------------------------------------------------------------//
InetSocketType InetSocketAddress::type() const
{
  return type_;
}
//----------------------------------------------------------------------------//
string InetSocketAddress::id(int level)
{
  string ret;
  ret = host_.id(level);
  ret += ":";
  ret += port_.id(level);
  return ret;
}
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
