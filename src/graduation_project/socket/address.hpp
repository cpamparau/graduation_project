#ifndef GRADUATION_PROJECT_SOCKET_ADDRESS_HPP
#define GRADUATION_PROJECT_SOCKET_ADDRESS_HPP

#include <string>
using namespace std;
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <arpa/inet.h>

#endif

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
class SocketType
{
public:
  SocketType(int family = AF_INET, int type = SOCK_STREAM,
      int protocol = 0);

  SocketType(const string &family, const string &type, int protocol =
      0);

  int address_family() const;
  int type() const;
  int protocol() const;

  static int str_to_type(const string &str);
  static int str_to_family(const string &str);

protected:
  int address_family_, type_, protocol_;
};
//----------------------------------------------------------------------------//
class InetSocketType: public SocketType
{
  public:
    InetSocketType(int type = SOCK_STREAM, int protocol = 0);
    InetSocketType(const string &type, int protocol = 0);
};
//----------------------------------------------------------------------------//
class InetPort
{
  public:
    InetPort(unsigned int port = 0);
    InetPort(const string &service);

    unsigned int port();
    string service_name();

    //string representation of the port
    //level = 0, getxxbyyy functions are not called, string = port
    //level = 1, string = port(service)
    string id(int level = 0);

  private:
    string _int_to_str(int i);
    unsigned int port_;
    string service_;
    bool port_flag_, service_flag_;
};
//----------------------------------------------------------------------------//

#ifdef WIN32
    typedef unsigned int in_addr_t;
#endif
//----------------------------------------------------------------------------//
class InetHost
{
public:
    InetHost(in_addr_t addr = INADDR_ANY);
    InetHost(const string &str_name);

    //unsigned long address();
    in_addr_t address();
    string name();
    bool is_addr_any() const;

    //returns the string representation of the host address
    //if level = 0 the getxxbyyy function are not called,
    //the string = ip or name
    //level = 1, string = name( ip )
    string id(int level = 0);

    string ip_to_string();

private:
    //unsigned long address_;
    in_addr_t address_;
    string name_;
    bool address_flag_, name_flag_;
};
//----------------------------------------------------------------------------//
class InetSocketAddress
{
  class _address_parser
  {
      public:
          _address_parser(const char *str, char c1, char c2,
              char del1 = '(', char del2 = ')');
          ~_address_parser();

          string get_token(char del = ' ');
      private:
          char *string_;
          char *index_;
  };
public:
    InetSocketAddress(const InetPort &port, const InetHost &host,
        int t = SOCK_STREAM, int protocol = 0);
    InetSocketAddress(const string &str);
    InetSocketAddress(const sockaddr_in &addr, int t = SOCK_STREAM,
        int protocol = 0);

    operator const sockaddr_in*() const;

    InetHost host() const;
    InetPort port() const;
    InetSocketType type() const;

    //returns the string representation of the address
    //if level = 0 the getxxbyyy function are not called,
    //the string = ip or name : port
    //level = 1, string = name( ip ):port
    string id(int level = 0);
private:
    InetHost host_;
    InetPort port_;
    InetSocketType type_;
    sockaddr_in sockaddr_;
    bool sockaddr_flag_;
};
//----------------------------------------------------------------------------//

} // namespace socket
} // namespace graduation_project
#endif // GRADUATION_PROJECT_SOCKET_ADDRESS_HPP
