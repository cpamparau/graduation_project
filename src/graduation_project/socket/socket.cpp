#include <errno.h>

#include <sstream>

#ifdef WIN32
#include <winsock2.h>
#endif
#ifndef WSAEWOULDBLOCK
#define WSAEWOULDBLOCK EWOULDBLOCK
#endif

#define POLL_METHOD_SELECT  1
#define POLL_METHOD_POLL    2
#define POLL_METHOD_EPOLL   3

#ifdef WIN32
#define POLL_METHOD POLL_METHOD_SELECT
#else
#define POLL_METHOD POLL_METHOD_POLL
#endif

#if POLL_METHOD==POLL_METHOD_POLL
#include <poll.h>
#endif

#include "graduation_project/socket/socket.hpp"
#include "graduation_project/socket/socket_errors.hpp"

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
Socket::Socket() :
  _socket(INVALID_SOCKET), _state(Socket::normal), _errno(0), _last_op(
      null_op), _err_op(null_op), _exception_flag(false)
{
}
//----------------------------------------------------------------------------//
Socket::Socket(int sd) :
  _socket(sd), _state(Socket::normal), _errno(0), _last_op(null_op),
  _err_op(null_op), _exception_flag(false)
{
  _socket_proxy.atach(HandleProxy::get_instance(sd));
}
//----------------------------------------------------------------------------//
Socket::Socket(const SocketType &type) :
  _state(Socket::normal), _errno(0), _last_op(null_op), _err_op(null_op),
  _exception_flag(false)
{
  _create_socket(type);
}
//----------------------------------------------------------------------------//
Socket::~Socket()
{
}
//----------------------------------------------------------------------------//
void Socket::listen(int backlog /*= 8*/)
{
  _last_op = listen_op;
  if (::listen(_socket, backlog) < 0)
  {
    _throw_error("Socket::listen", listen_op);
  }
}
//----------------------------------------------------------------------------//
Socket Socket::accept(InetSocketAddress *addr /*= NULL*/)
{
  _last_op = accept_op;
  int s;
  if (addr)
  {
    sockaddr_in a;
    socklen_t len = sizeof(sockaddr_in);
    s = ::accept(_socket, reinterpret_cast<sockaddr*>(&a), &len);
    if (s != INVALID_SOCKET)
        *addr = InetSocketAddress(a);
  }
  else
  {
    s = ::accept(_socket, NULL, NULL);
  }
  if (s == INVALID_SOCKET)
    _throw_error("Socket::accept", WSAEWOULDBLOCK, accept_op);

  Socket ret(s);
  ret.setsockopt( SO_REUSEADDR, 1);
  return ret;
}
//----------------------------------------------------------------------------//
void Socket::bind(const InetSocketAddress &address)
{
  if (_socket == INVALID_SOCKET)
     _create_socket(address.type());
  else
    _check_type(address);

  const sockaddr_in *addr = address;
  socklen_t len = sizeof(sockaddr_in);
  //setsockopt( SO_REUSEADDR,1 );
  _last_op = bind_op;
  if (::bind(_socket, reinterpret_cast<sockaddr*>(
    const_cast<sockaddr_in*>(addr)), len) < 0)
  {
    _throw_error("Socket::bind", bind_op);
  }
}
//----------------------------------------------------------------------------//
void Socket::connect(const InetSocketAddress &address)
{
  if (_socket == INVALID_SOCKET)
    _create_socket(address.type());
  else
    _check_type(address);

  const sockaddr_in *addr = address;
  _last_op = connect_op;
  if (::connect(_socket, reinterpret_cast<const sockaddr*>(addr),
    sizeof(sockaddr_in)) < 0)
  {
    _throw_error("Socket::connnect", WSAEWOULDBLOCK, connect_op);
  }
}
//----------------------------------------------------------------------------//
int Socket::send(const char * buf, int len, int flags /*= 0*/)
{
  _last_op = send_op;
  int ret = ::send(_socket, buf, len, flags);
  if (ret < 0)
  {
      _throw_error("Socket::send", WSAEWOULDBLOCK, send_op);
      ret = 0;
  }
  return ret;
}
//----------------------------------------------------------------------------//
//this function throws an exception if it couldn't send exactly n bytes
 int Socket::send_n(const char *buf, int buf_size, int n)
 {
   int m = min(buf_size, n);
   int ret = m;
   while (m > 0)
   {
     _last_op = send_op;
     int n = send(buf, m);
     if (n <= 0)
         _throw_error("Socket::send_n", send_op);
     buf += n;
     m -= n;
   }
   return ret;
 }
//----------------------------------------------------------------------------//
int Socket::sendto(const char *buf, int len, const InetSocketAddress &to,
  int flags /*= 0*/)
{
   const sockaddr_in *addr = to;
   socklen_t tolen = sizeof(sockaddr_in);
   _last_op = sendto_op;
   int ret = ::sendto(_socket, buf, len, flags,
      reinterpret_cast<const sockaddr*>(addr), tolen);
   if (ret < 0)
   {
      _throw_error("Socket::sendto", sendto_op);
      ret = 0;
   }
   return ret;
}
//----------------------------------------------------------------------------//
int Socket::recv(char * buf, int len, int flags /*= 0*/)
{
  _last_op = recv_op;
  int ret = ::recv(_socket, buf, len, flags);
  if (ret < 0)
  {
    _throw_error("Socket::recv", WSAEWOULDBLOCK, recv_op);
    ret = 0;
  }
  else if (ret == 0)
    _state = Socket::eof;

  return ret;
}
//----------------------------------------------------------------------------//
//this function throws an exception if it couldn't read exactly n bytes
int Socket::recv_n(char *buffer, int buf_size, int n)
{
  int m = min(buf_size, n);
  int ret = m;
  while (m > 0)
  {
    _last_op = recv_op;
    int n = recv(buffer, m);
    if (n <= 0) //eof
        _throw_error("Socket::recv_n", recv_op);
    buffer += n;
    m -= n;
  }

  return ret;
}
//----------------------------------------------------------------------------//
int Socket::recvfrom(char *buf, int len, InetSocketAddress *from /*= NULL*/,
  int flags /*= 0*/)
{
  int ret;
  _last_op = recvfrom_op;
  if (from)
  {
    sockaddr_in addr;
    socklen_t fromlen = sizeof(sockaddr_in);
    ret = ::recvfrom(_socket, buf, len, flags,
      reinterpret_cast<sockaddr*>(&addr), &fromlen);
    if (ret > 0)
    {
      //check to see if it is a datagram socket
      if (getsockopt( SO_TYPE) == SOCK_DGRAM)
        *from = InetSocketAddress(addr);
      else
        *from = getpeername();
    }
  }
  else
  {
    ret = ::recvfrom(_socket, buf, len, flags, NULL, NULL);
  }
  if (ret < 0)
  {
    _throw_error("Socket::recvfrom", WSAEWOULDBLOCK, recvfrom_op);
    ret = 0;
  }
  else if (ret == 0)
    _state = Socket::eof;

  return ret;
}
//----------------------------------------------------------------------------//
void Socket::setsockopt(int level, int optname, void *optval,
    socklen_t optlen)
{
  _last_op = setsockopt_op;
#ifdef WIN32
  if( ::setsockopt( _socket,level,optname,(const char*)optval,optlen )<0 )
#else
  if (::setsockopt(_socket, level, optname, optval, optlen) < 0)
#endif
  {
      _throw_error("Socket::setsockopt", setsockopt_op);
  }
}
//----------------------------------------------------------------------------//
void Socket::setsockopt(int optname, int optval)
{
  setsockopt(SOL_SOCKET, optname, static_cast<void*>(&optval), sizeof(int));
}
//----------------------------------------------------------------------------//
void Socket::getsockopt(int level, int optname, char * optval,
  socklen_t *optlen) const
{
  _last_op = getsockopt_op;
  if (::getsockopt(_socket, level, optname, optval, optlen) < 0)
  {
      _throw_error("Socket::getsockopt", getsockopt_op);
  }
}
//----------------------------------------------------------------------------//
int Socket::getsockopt(int optname) const
{
  int optval;
  socklen_t optlen = sizeof(int);
  getsockopt( SOL_SOCKET, optname, static_cast<char*>(static_cast<void*>(
      &optval)), &optlen);
  return optval;
}
//----------------------------------------------------------------------------//
void Socket::ioctl2(long cmd, int *arg)
{
}
//----------------------------------------------------------------------------//
void Socket::ioctl(long cmd, void *arg) const
{
  _last_op = ioctl_op;
#ifdef WIN32
  unsigned long *ularg = (unsigned long *)arg;
  if( ioctlsocket( _socket,cmd,ularg )!=0 )
  _throw_error( "Socket::ioctl",ioctl_op );
#else
  int *iarg = static_cast<int*>(arg);
  if (::ioctl(_socket, cmd, iarg) != 0)
      _throw_error("Socket::ioctl", ioctl_op);

#endif
}
//----------------------------------------------------------------------------//
void Socket::ioctl(long cmd, int arg) const
{
  ioctl(cmd, static_cast<void*>(&arg));
}
//----------------------------------------------------------------------------//
InetSocketAddress Socket::getsockname()
{
  sockaddr_in addr;
  socklen_t len = sizeof(sockaddr_in);
  int t = getsockopt( SO_TYPE);
  _last_op = getsockname_op;
  if (::getsockname(_socket, reinterpret_cast<sockaddr*>(&addr), &len) <0)
  {
      _throw_error("Socket::getsockname", getsockname_op);
  }
  return InetSocketAddress(addr, t);
}
//----------------------------------------------------------------------------//
InetSocketAddress Socket::getpeername()
{
  sockaddr_in addr;
  socklen_t len = sizeof(sockaddr_in);
  int t = getsockopt( SO_TYPE);
  _last_op = getpeername_op;
  if (::getpeername(_socket, reinterpret_cast<sockaddr*>(&addr), &len) <0)
  {
      _throw_error("Socket::getpeername", getpeername_op);
  }
  return InetSocketAddress(addr, t);
}
//----------------------------------------------------------------------------//
SocketType Socket::get_type() const
{
  int type = getsockopt( SO_TYPE);
  SocketType ret( AF_INET, type);
  return ret;
}
//----------------------------------------------------------------------------//
void Socket::close()
{
  if (_socket_proxy == NULL)
      return;
  _socket = INVALID_SOCKET;
  _last_op = close_op;
  if (_socket_proxy->close() < 0)
  {
      _throw_error("Socket::close", WSAEWOULDBLOCK, close_op);
  }
}
//----------------------------------------------------------------------------//
void Socket::shutdown(int how /*= 2*/)
{
  _last_op = shutdown_op;
  if (::shutdown(_socket, how) < 0)
  {
    _throw_error("Socket::shutdown", WSAEWOULDBLOCK, shutdown_op);
  }
}
//----------------------------------------------------------------------------//
bool Socket::is_valid() const
{
  return _socket != INVALID_SOCKET;
}
//----------------------------------------------------------------------------//
//timeout - milliseconds
bool Socket::can_read(unsigned long timeout)
{
  return _test_rw(true, timeout);
}
//----------------------------------------------------------------------------//
//timeout - milliseconds
bool Socket::can_write(unsigned long timeout)
{
  return _test_rw(false, timeout);
}
//----------------------------------------------------------------------------//
bool Socket::is_eof() const
{
  return _state == Socket::eof;
}
//----------------------------------------------------------------------------//
#if POLL_METHOD==POLL_METHOD_SELECT

bool Socket::_test_rw( bool test_read,unsigned long timeout )
{
  fd_set s;
  timeval t;
  t.tv_sec = timeout/1000;
  t.tv_usec = (timeout%1000)*1000;
  FD_ZERO( &s );
  FD_SET( _socket,&s );
  int ret;

  _last_op = select_op;
  if( test_read ) ret = select( _socket+1,&s,NULL,NULL,&t );
  else ret = select( _socket+1,NULL,&s,NULL,&t );

  if( ret<0 )
  _throw_error( "Socket::_test_rw",select_op );
  else if( ret==0 )
  return false;
  else if( FD_ISSET( _socket,&s ) )
  return true;

  return false;
}
#elif POLL_METHOD==POLL_METHOD_POLL

bool Socket::_test_rw(bool test_read, unsigned long timeout)
{
  const int SIZE = 1;
  struct pollfd fd[SIZE];

  fd[0].fd = _socket;

  if (test_read)
  {
    fd[0].events = POLLIN; //|POLLPRI
  }
  else
  {
    fd[0].events = POLLOUT; //|POLLWRBAND
  }

  int n = poll(fd, SIZE, timeout);

  if (n > 0) //success
  {
    if (test_read)
    {
      if ((fd[0].revents & POLLIN) || (fd[0].revents & POLLERR))
      { //POLLHP POLLNVAL //fd[ 0 ].revents&POLLRDNORM ||
        return true;
      }
      else if ((fd[0].revents & POLLHUP)
          || (fd[0].revents & POLLNVAL))
      {
        _throw_error("Socket::_test_rw", select_op);
      }
    }
    else
    {
      if (fd[0].revents & POLLOUT) //POLLHP POLLNVAL POLLWRBAND
      {
        return true;
      }
      else if ((fd[0].revents & POLLERR) || (fd[0].revents & POLLHUP)
          || (fd[0].revents & POLLNVAL))
      {
        _throw_error("Socket::_test_rw", select_op);
      }
    }
  }
  else if (n == 0) //timeout
  {
    return false;
  }
  else //error
  {
    _throw_error("Socket::_test_rw", select_op);
  }

  return false;
}
#elif POLL_METHOD==POLL_METHOD_EPOLL

bool Socket::_test_rw( bool test_read,unsigned long timeout )
{
  return false;
}

#else

bool Socket::_test_rw( bool test_read,unsigned long timeout )
{
  return false;
}

#endif

//----------------------------------------------------------------------------//
void Socket::_create_socket(const SocketType &t)
{
  _last_op = socket_op;
  _socket = ::socket(t.address_family(), t.type(), t.protocol());
  if (_socket == INVALID_SOCKET)
  {
      _throw_error("Socket::_create_socket", socket_op);
  }
  _socket_proxy.atach(HandleProxy::get_instance(_socket));
  _state = Socket::normal;
  //setsockopt( SO_REUSEADDR,1 );
}
//----------------------------------------------------------------------------//
void Socket::_check_type(const InetSocketAddress &addr)
{
}
//----------------------------------------------------------------------------//
void Socket::_throw_error(const char *where, socket_op_id op) const
{
  _errno = get_last_error();
  _err_op = op;
  _exception_flag = true;
  throw SocketError(_errno, where);
}
//----------------------------------------------------------------------------//
void Socket::_throw_error(const char *where, int exclude,
    socket_op_id op) const
{
  int err = get_last_error();
  _errno = err;
  _err_op = op;

  if (err != exclude)
  {
    _exception_flag = true;
    throw SocketError(err, where);
  }
  else
  {
    _exception_flag = false;
  }
}
//----------------------------------------------------------------------------//
int Socket::get_last_error()
{
#ifdef WIN32
  return WSAGetLastError();
#else
  return errno;
#endif
}
//----------------------------------------------------------------------------//
int Socket::get_last_err_op(string &op)
{
  ostringstream ostr;

  ostr << "last socket op = [" << _get_op_name(_last_op)
    << "], last sock error = [" << _errno
    << "], last operation that has generated the error = ["
    << _get_op_name(_err_op) << "]";

  if (_exception_flag)
    ostr << "( exception was thrown )";
  else
    ostr << "( exception wasn't thrown )";

  op = ostr.str();

  return _errno;
}
//----------------------------------------------------------------------------//
string Socket::_get_op_name(socket_op_id op) const
{
  ostringstream ostr;

  switch (op)
  {
    case null_op:
      ostr << "null_op";
      break;
    case recv_op:
      ostr << "recv_op";
      break;
    case send_op:
      ostr << "send_op";
      break;
    case sendto_op:
      ostr << "sendto_op";
      break;
    case recvfrom_op:
      ostr << "recvfrom_op";
      break;
    case bind_op:
      ostr << "bind_op";
      break;
    case connect_op:
      ostr << "connect_op";
      break;
    case accept_op:
      ostr << "accept_op";
      break;
    case listen_op:
      ostr << "listen_op";
      break;
    case setsockopt_op:
      ostr << "setsockopt_op";
      break;
    case getsockopt_op:
      ostr << "getsockopt_op";
      break;
    case ioctl_op:
      ostr << "ioctl_op";
      break;
    case socket_op:
      ostr << "socket_op";
      break;
    case close_op:
      ostr << "close_op";
      break;
    case getsockname_op:
      ostr << "getsockname_op";
      break;
    case getpeername_op:
      ostr << "getpeername_op";
      break;
    case shutdown_op:
      ostr << "shutdown_op";
      break;
    case select_op:
      ostr << "select_op";
      break;
    default:
      ostr << "undefined_op";
      break;
  }

  return ostr.str();
}
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
