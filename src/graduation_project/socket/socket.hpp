#ifndef GRADUATION_PROJECT_SOCKET_SCOCKET_HPP
#define GRADUATION_PROJECT_SOCKET_SCOCKET_HPP

#include <atomic>
#include "graduation_project/socket/address.hpp"
#include "graduation_project/socket/socket_utils.hpp"

#ifndef WIN32
#include <sys/ioctl.h>
#include <unistd.h>
const int INVALID_SOCKET = -1;  // match Winsock
// Close is called closesocket in winsock land
static inline int closesocket(int fd)
{
    return close(fd);
}
#endif

#ifdef WIN32
typedef int socklen_t;
#endif

#ifndef WSAEWOULDBLOCK
#define WSAEWOULDBLOCK EWOULDBLOCK
#endif


namespace graduation_project
{
namespace socket
{
class HandleProxy: public RefCounter
{
  HandleProxy(int handle = -1) :
      _handle(handle)
  {
  }
  HandleProxy(const HandleProxy&);
  HandleProxy& operator=(const HandleProxy&);
public:
  ~HandleProxy()
  {
    close();
  }
  int close()
  {
    int ret = 0;
    if (_handle != -1)
    {
      ret = closesocket(_handle);
      _handle = -1;
    }
    return ret;
  }
  static HandleProxy* get_instance(int handle = -1)
  {
      return new HandleProxy(handle);
  }
private:
  int _handle;
};

template class RefPtrProxy<HandleProxy> ;

class Socket
{
protected:
  typedef enum
  {
      normal, eof
  } state;
  typedef enum
  {
    null_op = 0,
    recv_op,
    send_op,
    sendto_op,
    recvfrom_op,
    bind_op,
    connect_op,
    accept_op,
    listen_op,
    setsockopt_op,
    getsockopt_op,
    ioctl_op,
    socket_op,
    close_op,
    getsockname_op,
    getpeername_op,
    shutdown_op,
    select_op
  } socket_op_id;
public:
  //std::atomic<bool> is_authenticated{false};
  Socket();
  Socket(int sd);
  Socket(const SocketType &type);
  virtual ~Socket();

  int send(const char *buf, int len, int flags = 0);
  // this function throws an exception if it couldn't
  // send exactly n bytes
  int send_n(const char *buf, int buf_size, int n);
  int sendto(const char *buf, int len, const InetSocketAddress &to,
      int flags = 0);

  int recv(char * buf, int len, int flags = 0);
  // this function throws an exception if it couldn't
  // read exactly n bytes
  int recv_n(char *buffer, int buf_size, int n);
  int recvfrom(char *buf, int len, InetSocketAddress *from = NULL,
      int flags = 0);

  Socket accept(InetSocketAddress *addr = NULL);
  void bind(const InetSocketAddress &address);
  void connect(const InetSocketAddress &address);
  void listen(int backlog = 32);

  void setsockopt(int level, int optname, void *optval,
      socklen_t optlen);
  void setsockopt(int optname, int optval);
  void getsockopt(int level, int optname, char * optval,
      socklen_t *optlen) const;
  int getsockopt(int optname) const;

  void ioctl(long cmd, void *arg) const;
  void ioctl(long cmd, int arg) const;

  virtual void ioctl2(long cmd, int *arg);

  InetSocketAddress getsockname();
  InetSocketAddress getpeername();
  int socket() const
  {
      return _socket;
  }

  void close();
  void shutdown(int how = 2);

  SocketType get_type() const;
  bool is_valid() const;

  //timeout - milliseconds
  bool can_read(unsigned long timeout);
  bool can_write(unsigned long timeout);

  bool is_eof() const;

  static int get_last_error();

  int get_last_err_op(string &op);
private:
  string _get_op_name(socket_op_id op) const;
protected:
  bool _test_rw(bool test_read, unsigned long timeout);
  virtual void _create_socket(const SocketType &t);
  void _throw_error(const char *where, socket_op_id op) const;
  void _throw_error(const char *where, int exclude,
      socket_op_id op) const;
  void _check_type(const InetSocketAddress &addr);

protected:
  RefPtrProxy<HandleProxy> _socket_proxy;
  int _socket;
  state _state;

  mutable int _errno;
  mutable socket_op_id _last_op, _err_op;
  mutable bool _exception_flag;
};

#define CVS_SET_BLOCKING(cvssocket)         cvssocket.ioctl( FIONBIO,0 );
#define CVS_SET_NON_BLOCKING(cvssocket)     cvssocket.ioctl( FIONBIO,1 );

} // namespace socket
} // namespace graduation_project
#endif // GRADUATION_PROJECT_SOCKET_SCOCKET_HPP
