#ifdef WIN32
#include <windows.h>
#endif

#include <string.h>
#include "graduation_project/socket/socket_errors.hpp"
namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
UnknownHost::UnknownHost(const char *what_) :
  NetError(what_, "")
{
}
//----------------------------------------------------------------------------//
ServiceNotAvailable::ServiceNotAvailable(const char *what_) :
  NetError(what_, "")
{
}
//----------------------------------------------------------------------------//
UnknownSocketType::UnknownSocketType(const char *what_) :
  NetError(what_, "")
{
}
//----------------------------------------------------------------------------//
UnknownSocketFamily::UnknownSocketFamily(const char *what_) :
  NetError(what_)
{
}
//----------------------------------------------------------------------------//
SocketError::SocketError(long error_nr, const char *where) :
  _err_nr(error_nr)
{
  what_ = _translate_err_nr(error_nr);
  where_ = where;
}
//----------------------------------------------------------------------------//
string SocketError::_translate_err_nr(long err) const
{
  string ret;

#ifdef WIN32
    LPVOID buffer;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        GetLastError(),
        0,
        (LPTSTR) &buffer,
        0,
        NULL
    );
    ret = (const char*)buffer;
    LocalFree( buffer );
#else
    ret = strerror(static_cast<int>(err));
#endif
  return ret;
}
//----------------------------------------------------------------------------//
long SocketError::get_errno() const
{
  return _err_nr;
}
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
