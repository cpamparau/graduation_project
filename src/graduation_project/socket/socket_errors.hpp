#ifndef GRADUATION_PROJECT_SOCKET_SOCKET_ERRORS_HPP
#define GRADUATION_PROJECT_SOCKET_SOCKET_ERRORS_HPP

#include <string>
#include <exception>
using namespace std;


namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
class NetError: public exception
{
protected:
  NetError(const string& what = "", const string& where = "")
  {
      what_ = what;
      where_ = where;
  }
public:
  virtual ~NetError() noexcept
  {
  }

  virtual const char *what() const noexcept
  {
      return what_.c_str();
  }

  virtual const char *where() const
  {
      return where_.c_str();
  }

protected:
    string what_;
    string where_;
};
//----------------------------------------------------------------------------//
class SocketError: public NetError
{
  public:
    SocketError(long error_nr, const char *where_);
    long get_errno() const;
  private:
    string _translate_err_nr(long err) const;
  private:
    long _err_nr;
};
//----------------------------------------------------------------------------//
class ServiceNotAvailable: public NetError
{
  public:
    ServiceNotAvailable(const char *what_ = NULL);
};
//----------------------------------------------------------------------------//
class UnknownHost: public NetError
{
  public:
    UnknownHost(const char *what_ = NULL);
};
//----------------------------------------------------------------------------//
class UnknownSocketType: public NetError
{
  public:
    UnknownSocketType(const char *what_ = NULL);
};
//----------------------------------------------------------------------------//
class UnknownSocketFamily: public NetError
{
  public:
    UnknownSocketFamily(const char *what_ = NULL);
};
//----------------------------------------------------------------------------//
class WinSockInitFailed: public NetError
{
  public:
};
//----------------------------------------------------------------------------//
}
}
#endif // GRADUATION_PROJECT_SOCKET_SOCKET_ERRORS_HPP
