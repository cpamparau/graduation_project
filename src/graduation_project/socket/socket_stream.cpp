#include "graduation_project/socket/socket_stream.hpp"
#include <exception>
using namespace std;
#define CVSCPP_SOCKET_STREAM_BUFFER_SIZE 1024

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
SocketStreamBuffer::SocketStreamBuffer()
{
  init_buffer();
}
//----------------------------------------------------------------------------//
SocketStreamBuffer::SocketStreamBuffer(Socket *socket)
{
  socket_ = socket;
  init_buffer();
}
//----------------------------------------------------------------------------//
SocketStreamBuffer::~SocketStreamBuffer()
{
  try
  {
      sync();
  } catch (exception&)
  {
  }
  delete[] buffer_;
}
//----------------------------------------------------------------------------//
int SocketStreamBuffer::overflow(int c)
{
  if (sync() == EOF)
      return EOF;

  if (c != EOF)
  {
    if (unbuffered())
        write(static_cast<char*>(static_cast<void*>(&c)), sizeof(char));
    else
        sputc(c);
  }
  return 0;
}
//----------------------------------------------------------------------------//
int SocketStreamBuffer::underflow()
{
  if (in_avail())
    return *gptr();

  if (!unbuffered())
  {
    int n = pbase() - buffer_;

    if (n)
    {
      int count = read(buffer_, n);
      if (count)
      {
        setg(buffer_, buffer_, buffer_ + count);
        return static_cast<unsigned char>(*gptr());
      }
    }
  }
  return EOF;
}
//----------------------------------------------------------------------------//
int SocketStreamBuffer::sync()
{
  int n = pptr() - pbase();
  int ret = 0;
  if (n)
  {
    write(pbase(), n);
    pbump(-n);
  }
  //don't empty the get area
  return ret;
}
//----------------------------------------------------------------------------//
void SocketStreamBuffer::init_buffer()
{
  buffer_ = new char[CVSCPP_SOCKET_STREAM_BUFFER_SIZE];

  setg(buffer_, buffer_, buffer_);
  setp(buffer_ + CVSCPP_SOCKET_STREAM_BUFFER_SIZE / 2,
      buffer_ + CVSCPP_SOCKET_STREAM_BUFFER_SIZE);
}
//----------------------------------------------------------------------------//
void SocketStreamBuffer::set_sink(Socket *socket)
{
  sync();
  socket_ = socket;
}
//----------------------------------------------------------------------------//
bool SocketStreamBuffer::unbuffered() const
{
  return buffer_ == 0;
}
//----------------------------------------------------------------------------//
int SocketStreamBuffer::read(char *buffer, int size)
{
  return socket_->recv(buffer, size);
}
//----------------------------------------------------------------------------//
int SocketStreamBuffer::write(const char *buffer, int size)
{
  return socket_->send_n(buffer, size, size);
}
//----------------------------------------------------------------------------//
SocketInputOutputStream::SocketInputOutputStream()
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketInputOutputStream::SocketInputOutputStream(Socket * socket) :
  _buffer(socket)
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketInputOutputStream::~SocketInputOutputStream()
{
}
//----------------------------------------------------------------------------//
SocketInputStream::SocketInputStream() :
  istream(0)
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketInputStream::SocketInputStream(Socket *socket) :
  istream(0), _buffer(socket)
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketInputStream::~SocketInputStream()
{
}
//----------------------------------------------------------------------------//
SocketOutputStream::SocketOutputStream() :
  ostream(0)
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketOutputStream::SocketOutputStream(Socket *socket) :
  ostream(0), _buffer(socket)
{
  init(&_buffer);
}
//----------------------------------------------------------------------------//
SocketOutputStream::~SocketOutputStream()
{
}
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
