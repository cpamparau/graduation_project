#ifndef GRADUATION_PROJECT_SOCKET_SOCKET_STREAM_HPP
#define GRADUATION_PROJECT_SOCKET_SOCKET_STREAM_HPP

#include <stdio.h>

#include <iostream>
using namespace std;

#include "graduation_project/socket/socket.hpp"


namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
class SocketStreamBuffer: public streambuf
{
public:
  SocketStreamBuffer();
  SocketStreamBuffer(Socket* socket);
  ~SocketStreamBuffer();

  int overflow(int c = EOF);
  int underflow();
  int sync();
  void set_sink(Socket *socket);

private:
  void init_buffer();
  bool unbuffered() const;
  int read(char *buffer, int n);
  int write(const char *buffer, int n);
  SocketStreamBuffer(const SocketStreamBuffer&);
  SocketStreamBuffer& operator =(const SocketStreamBuffer&);
private:
  Socket *socket_;
  char *buffer_;
};
//----------------------------------------------------------------------------//
class SocketInputOutputStream: virtual public ios
{
  SocketInputOutputStream(const SocketInputOutputStream&);
  SocketInputOutputStream& operator=(const SocketInputOutputStream&);
public:
  SocketInputOutputStream();
  SocketInputOutputStream(Socket* socket);
  virtual ~SocketInputOutputStream();
protected:
  SocketStreamBuffer* _get_buffer()
  {
      return &_buffer;
  }
private:
  SocketStreamBuffer _buffer;
};
//----------------------------------------------------------------------------//
class SocketInputStream: public istream
{
  SocketInputStream(const SocketInputStream&);
  SocketInputStream& operator=(const SocketInputStream&);
public:
  SocketInputStream();
  SocketInputStream(Socket* socket);
  ~SocketInputStream();
private:
  SocketStreamBuffer _buffer;
};
//----------------------------------------------------------------------------//
class SocketOutputStream: public ostream
{
  SocketOutputStream(const SocketOutputStream&);
  SocketOutputStream& operator=(const SocketOutputStream&);
public:
  SocketOutputStream();
  SocketOutputStream(Socket *socket);
  ~SocketOutputStream();
private:
  SocketStreamBuffer _buffer;
};
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
#endif // GRADUATION_PROJECT_SOCKET_SOCKET_STREAM_HPP
