#include "graduation_project/socket/socket_utils.hpp"

#ifdef WIN32
#include <windows.h>
#else
#include <assert.h>
#endif

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
RefCounter::RefCounter()
#ifdef WIN32
:_count( 1 )
#endif
{
#ifndef WIN32
    atomic_set(&_count, 1);
#endif
}
//----------------------------------------------------------------------------//
RefCounter::~RefCounter()
{
}
//----------------------------------------------------------------------------//
void RefCounter::add_ref()
{
#ifdef WIN32
    InterlockedIncrement( &_count );
#else
    atomic_inc(&_count);
#endif
}
//----------------------------------------------------------------------------//
void RefCounter::release()
{
#ifdef WIN32
  if( InterlockedDecrement( &_count )==0 )
  {
      delete this;
  }
#else
  /* Atomically decrements @v by 1 and
   * returns true if the result is 0, or false for all other
   * cases.  Note that the guaranteed
   * useful range of an atomic_t is only 24 bits.
   */
  //if(_count--==0){
  //  delete this;
  //  }
  if (atomic_dec_and_test(&_count))
  {
      delete this;
  }
  //assert( NULL );
#endif
}
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
