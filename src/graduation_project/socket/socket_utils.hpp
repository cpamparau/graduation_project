#ifndef GRADUATION_PROJECT_SOKCET_SOCKET_UTILS_HPP
#define GRADUATION_PROJECT_SOKCET_SOCKET_UTILS_HPP

#ifndef WIN32
extern "C"
{
#include "graduation_project/socket/atomic.h"
}
#endif

#ifndef NULL
#define NULL 0
#endif

namespace graduation_project
{
namespace socket
{
//----------------------------------------------------------------------------//
class RefCounter
{
private:
  RefCounter(const RefCounter&);
  RefCounter& operator=(const RefCounter&);
protected:
  RefCounter();
public:
  virtual ~RefCounter();

  void add_ref();
  void release();
private:
#ifdef WIN32
  long _count;
#else
  atomic_t _count;
#endif
};
//----------------------------------------------------------------------------//
template<class ref_ptr>
class RefPtrProxy
{
public:
  RefPtrProxy(ref_ptr *p = NULL) :
    _ptr(p)
  {
    if (_ptr)
      _ptr->add_ref();
  }

  RefPtrProxy(const RefPtrProxy &p) :
    _ptr(p._ptr)
  {
    if (_ptr)
      _ptr->add_ref();
  }

  ~RefPtrProxy()
  {
    if (_ptr)
      _ptr->release();
  }

  RefPtrProxy& operator=(const RefPtrProxy &p)
  {
    if (this != &p)
    {
      if (_ptr)
        _ptr->release();
      _ptr = p._ptr;
      if (_ptr)
        _ptr->add_ref();
    }
    return *this;
  }

  RefPtrProxy& operator=(ref_ptr *p)
  {
    if (_ptr)
      _ptr->release();
    _ptr = p;
    if (_ptr)
      _ptr->add_ref();
    return *this;
  }

  void atach(ref_ptr *p)
  {
    if (_ptr)
      _ptr->release();
    _ptr = p;
  }

  void detach()
  {
    if (_ptr)
      _ptr->release();
    _ptr = NULL;
  }

  ref_ptr* operator->()
  {
    return _ptr;
  }
  const ref_ptr* operator->() const
  {
    return _ptr;
  }

  operator const ref_ptr*() const
  {
    return _ptr;
  }
private:
  ref_ptr *_ptr;
};
//----------------------------------------------------------------------------//
template<class C>
class CVSBuffer
{
private:
  CVSBuffer(const CVSBuffer&);
  CVSBuffer& operator=(const CVSBuffer&);
public:
  CVSBuffer(unsigned long dim = 0) :
    _buffer( NULL), _size(dim), _delete_flag(false)
  {
    if (dim)
    {
      _buffer = new C[dim];
      _delete_flag = true;
    }
  }

  void resize(unsigned long new_size)
  {
    if (new_size > _size)
    {
      if (_delete_flag)
          delete[] _buffer;
      _buffer = new char[new_size];
      _delete_flag = true;
      _size = new_size;
    }
  }

  void attach(C *buffer, unsigned long size, bool delete_it = true)
  {
    if (_delete_flag)
      delete[] _buffer;

    _buffer = buffer;
    _size = size;
    _delete_flag = delete_it;
  }

  ~CVSBuffer()
  {
    if (_delete_flag && _buffer)
      delete[] _buffer;
  }

  operator C*()
  {
    return _buffer;
  }
private:
  C *_buffer;
  unsigned long _size;
  bool _delete_flag;
};
//----------------------------------------------------------------------------//
} // namespace socket
} // namespace graduation_project
#endif // GRADUATION_PROJECT_SOKCET_SOCKET_UTILS_HPP
