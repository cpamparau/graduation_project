#include <sstream>
#include "graduation_project/utils.hpp"
#include "graduation_project/settings/settings.hpp"

namespace graduation_project
{
//----------------------------------------------------------------------------//
void init_default_values()
{
  settings& settings = settings.get();

  settings["port"] = "8000";
  settings["host"] = "127.0.0.1";

  settings["db_host"] = "127.0.0.1";
  settings["db_port"] = "3306";
  settings["db_name"] = "graduation_project";
  settings["db_user"] = "root";
  settings["db_passwd"] = "licenta";
  settings["config_file"] = "./params.prm";
}
//----------------------------------------------------------------------------//
void init_database_info(database::database_connection_info &mysql_info)
{
  settings& settings = settings.get();

  mysql_info.db = settings["db_name"];
  mysql_info.host = settings["db_host"];

  std::stringstream port(settings["db_port"]);
  port >> mysql_info.port;

  mysql_info.passwd = settings["db_passwd"];
  mysql_info.user = settings["db_user"];
}
//----------------------------------------------------------------------------//
}
