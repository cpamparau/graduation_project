#ifndef GRADUATION_PROJECT_UTILS_HPP
#define GRADUATION_PROJECT_UTILS_HPP

#include <atomic>
#include "graduation_project/database/database_connection_info.hpp"

#ifndef _WIN32
  #define LS_ATTR_UNUSED __attribute__((unused))
#else
  #define LS_ATTR_UNUSED
#endif

namespace graduation_project
{

void init_default_values();

void init_database_info(database::database_connection_info& mysql_info);

typedef unsigned int request_id;

/**
 * @brief protocol The protocol that will be used to transfer the data. Ex: xml
 * protobuf, flatbuf, etc.
 */
typedef char protocol;

namespace requests
{

static LS_ATTR_UNUSED unsigned int req_id_null = UINT32_MAX;

enum request_type{
  AUTHENTICATION = 0,
  EMAIL = 1
};

} // namespace requests

struct connection
{
  /**
   * Counter used for assigning a unique connection id to every connection.
   */
  unsigned int user_id{0};

  /**
   * This flag indicates that a client has been authenticated sucessfully
   */
  std::atomic<bool> is_authenticated{false};

  /**
   * Protocol of communication saved at authentication, used to define the
   * communication channel.
   */
  graduation_project::protocol protocol;

  /**
   * The username string object is populated with the database username.
   */
  ::std::string user_name{""};
};

/**
 * @brief connection_hdl A typedef for struct connection in order to be easier
 * to use it
 */
typedef struct connection connection_hdl;

} // namespace graduation_project
#endif // GRADUATION_PROJECT_UTILS_HPP
