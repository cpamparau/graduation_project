#include <climits>
#include <stdlib.h> // for strtoull
#include <gtest/gtest.h>
#include "graduation_project/database/result/cell.hpp"
using namespace ::std;
using namespace ::graduation_project::database::result;

#define CHAR_PTR(value) const_cast<char*>(value)

namespace test
{
namespace graduation_project
{
namespace database
{
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for char
 */
TEST(cell, char_data_type)
{
  // The signed range is -128 to 127
  signed char r1 = cell(CHAR_PTR("-128"), 3);// MIN
  EXPECT_EQ(-128, r1);

  r1 = cell(CHAR_PTR("127"), 3); // MAX
  EXPECT_EQ(127, r1);

  r1 = cell(CHAR_PTR("-129"), 3); // MIN-1 = MAX
  EXPECT_EQ(127, r1);

  r1 = cell(CHAR_PTR("128"), 3); // MAX+1 = MIN
  EXPECT_EQ(-128, r1);


  // The unsigned range is 0 to 255
  unsigned char r2=cell(CHAR_PTR("0"), 3); // MIN
  EXPECT_EQ(0, r2);

  r2=cell(CHAR_PTR(
  "255"), 3); // MAX
  EXPECT_EQ(255, r2);

  r2=cell(CHAR_PTR("-1"), 3); // MIN - 1 = MAX
  EXPECT_EQ(255, r2);

  r2=cell(CHAR_PTR("256")
, 3); // MAX + 1 = MIN
  EXPECT_EQ(0, r2);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for short int
 */
TEST(cell, short_int_data_type)
{
  // The signed range -32768 to 32767
  signed short int r1 = cell(CHAR_PTR("-32768"), 5);// MIN
  EXPECT_EQ(-32768, r1);

  r1 = cell(CHAR_PTR("32767"), 5); // MAX
  EXPECT_EQ(32767, r1);

  r1 = cell(CHAR_PTR("-32769"), 5); // MIN-1 = MAX
  EXPECT_EQ(32767, r1);

  r1 = cell(CHAR_PTR("32768"), 5); // MAX+1 = MIN
  EXPECT_EQ(-32768, r1);

  // The unsigned range 0 to 65535
  unsigned short int r2=cell(CHAR_PTR(
  "0"), 5); // MIN
  EXPECT_EQ(0, r2);

  r2=cell(CHAR_PTR("65535"), 3); // MAX
  EXPECT_EQ(65535, r2);

  r2=cell(CHAR_PTR("-1"), 3); // MIN - 1 = MAX
  EXPECT_EQ(65535, r2);

  r2=cell(CHAR_PTR("65536")
, 3); // MAX + 1 = MIN
  EXPECT_EQ(0, r2);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for int
 */
TEST(cell, int_data_type)
{
  // The signed range -2147483648 to 2147483647
  signed int r1 = cell(CHAR_PTR("-2147483648"), 10);// MIN
  EXPECT_EQ(-2147483648, r1);

  r1 = cell(CHAR_PTR("2147483647"), 10); // MAX
  EXPECT_EQ(2147483647, r1);

  r1 = cell(CHAR_PTR("-2147483649"), 10); // MIN-1
  EXPECT_EQ(2147483647, r1); // MIN-1 = MAX

  r1 = cell(CHAR_PTR("2147483648"), 10); // MAX+1 = MIN
  EXPECT_EQ(-2147483648, r1); // MAX+1 = MIN

  // The unsigned range 0 to 4294967295
  unsigned int r2=cell(CHAR_PTR("0")
, 1); // MIN
  EXPECT_EQ(static_cast<unsigned int>(0), r2);

  r2=cell(CHAR_PTR("4294967295"), 10); // MAX
  EXPECT_EQ(static_cast<unsigned int>(4294967295), r2);

  r2=cell(CHAR_PTR("-1"), 1); // MIN - 1 = MAX
  EXPECT_EQ(static_cast<unsigned int>(4294967295), r2);

  r2=cell(CHAR_PTR("4294967296"), 10); // MAX + 1 = MIN
  EXPECT_EQ(static_cast<unsigned int>(0), r2);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for long long int
 */
TEST(cell, long_long_int_data_type)
{
  // The signed range -9223372036854775808 to 9223372036854775807
  signed long long int r1 = cell(CHAR_PTR("-9223372036854775808"), 19);// MIN
  EXPECT_EQ(LLONG_MIN, r1);

  r1 = cell(CHAR_PTR("9223372036854775807"), 19); // MAX
  EXPECT_EQ(9223372036854775807, r1);

  r1 = cell(CHAR_PTR(
  "-9223372036854775809"), 19); // MIN-1 = MIN, just here
  EXPECT_EQ(LLONG_MIN, r1);

  r1 = cell(CHAR_PTR("9223372036854775808"), 19); // MAX+1 = MAX, just here
  EXPECT_EQ(9223372036854775807, r1);

  // The unsigned range 0 to 18446744073709551615
  unsigned long long int r2 = cell(CHAR_PTR("0"), 1);// MIN
  EXPECT_EQ(static_cast<unsigned long long int>(0), r2);

  r2 = cell(CHAR_PTR(
  "18446744073709551615"), 20); // MAX
  EXPECT_EQ(ULLONG_MAX, r2);

  r2 = cell(CHAR_PTR("-1"), 1); // MIN-1 = MAX, just here
  EXPECT_EQ(ULLONG_MAX, r2);

  r2 = cell(CHAR_PTR("18446744073709551616")
, 20); // MAX+1 = MAX, just here
  EXPECT_EQ(ULLONG_MAX, r2);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for string
 */
TEST(cell, string_data_type)
{
  string str=cell(CHAR_PTR("1995-8-18 10:10:10"), 18);
  EXPECT_EQ("1995-8-18 10:10:10", str);

  string null_chars;
  null_chars.push_back(115);
  null_chars.push_back(115);
  null_chars.push_back(115);
  null_chars.push_back(0);
  null_chars.push_back(84);
  null_chars.push_back(101);
  null_chars.push_back(115);
  null_chars.push_back(116);
  null_chars.push_back(0);
  null_chars.push_back(32);
  null_chars.push_back(116);
  string str2 = cell(CHAR_PTR(null_chars.c_str())
, null_chars.size());
  EXPECT_EQ(null_chars, str2);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for float
 */
TEST(cell, float_data_type)
{
  // float is for maximum 6 digits
  // INCREMENT MINIM = 0.0001

  float x = cell(CHAR_PTR("9.000001"), 18); // > 6 digits
  EXPECT_NE(9.000001, x);

  x = cell(CHAR_PTR("100.0001"), 18); // > 6 digits
  EXPECT_NE(100.0001, x);

  // MIN PARTE INTREAGA ( -999999)
  x = cell(CHAR_PTR("-999999"), 18);
  EXPECT_EQ(static_cast<float>(-999999), x);

  // MAX PARTE INTREAGA ( 999999 )
  x = cell(CHAR_PTR("999999"), 18);
  EXPECT_EQ(999999, x);

  x = cell(CHAR_PTR(
  "1e+06")
, 18); // 1e+06 = MAX PARTE INTREAGA + 1 = 1.000.000
  EXPECT_EQ(static_cast<float>(1000000), x);

  //cout<<std::numeric_limits<float>::min()<<endl;
  //cout<<std::numeric_limits<short int>::infinity()<<endl;
  //cout<<std::numeric_limits<float>().digits10<<endl;
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for double
 */
TEST(cell, double_data_type)
{
  double d=cell(CHAR_PTR("1196156250.00000000"), 18);
  EXPECT_EQ(1196156250.00000000, d);

  d=cell(CHAR_PTR("1196156250.1234000"), 18);
  EXPECT_EQ(static_cast<double>(1196156250.1234000), d);

  d=cell(CHAR_PTR(
  "1196156250.1234123456789"), 18);
  EXPECT_EQ(static_cast<double>(1196156250.1234123456789), d);

  d=cell(CHAR_PTR("998850.0000000011111111")
, 18);
  EXPECT_EQ(static_cast<double>(998850.0000000011111111), d);
}
//----------------------------------------------------------------------------//
/**
 * @brief testing cast operators for long double.
 * THIS TEST IS A TEST THAT PROVING THAT DOUBLE
 * IS BETTER THAN LONG DOUBLE IN THIS SITUATION.
 */
TEST(cell, ldouble_data_type)
{
  long double d = cell(CHAR_PTR("1196156250.00000000")
, 18);
EXPECT_EQ(1196156250.00000000, d);

  d=cell(CHAR_PTR("1196156250.12340000000"),18);
  EXPECT_NE(static_cast<long double>(1196156250.12340000000),d);


  d=cell(CHAR_PTR("1196156250.1234123456789"),18);
  EXPECT_NE(static_cast<long double>(1196156250.1234123456789),d);


  d=cell(CHAR_PTR("998850.0000000011111111"),18);
  EXPECT_NE(static_cast<long double>(998850.0000000011111111),d);

}
//----------------------------------------------------------------------------//
/**
 * @brief Test to_ascii_code method of cell.
 */
TEST(cell, to_ascii_code)
{
char ch = cell(CHAR_PTR("ABC"), 3).to_ascii_code();
EXPECT_EQ('A', ch);

ch = cell(CHAR_PTR("123"), 3).to_ascii_code();
EXPECT_EQ('1', ch);

ch = cell(CHAR_PTR("\0"), 1).to_ascii_code();
EXPECT_EQ('\0', ch);

ch = cell(nullptr, 0).to_ascii_code();
EXPECT_EQ('\0', ch);
}
//----------------------------------------------------------------------------//
} // namespace database
} // namespace graduation_project
} // namespace test
