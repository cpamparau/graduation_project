#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "graduation_project/database/mock/database_connection.hpp"
#include "graduation_project/database/mock/result_set.hpp"
#include "graduation_project/database/fake/result_set.hpp"

using namespace ::std;
using namespace ::testing;
using namespace graduation_project::database;

namespace test
{
namespace graduation_project
{
namespace database
{
/**
 * An example unit test
 */
namespace example
{
/**
 * @brief Example of a database user.
 */
class mysql_user
{
  /**
   * @brief MySQL connection
   */
  interf::database_connection &c;

  /**
   * @brief MySQL Result set used to hold db results
   */
  interf::result_set &rs;

  /**
   * @brief DB result is moved to this vector for unit testing later
   */
  vector< pair< string, string > > v;

public:
  /**
   * @brief mysql_user constructor
   * @param[in] c - connection to use
   * @param[in] rs - result set to use (mocked by unit test)
   */
  mysql_user(interf::database_connection &c, interf::result_set &rs) :
    c(c), rs(rs)
  {
  }

  /**
   * @brief get db result
   * @return returns data from db structured in this vector
   */
  vector< pair< string, string > > get_data()
  {
    return v;
  }

  /**
   * @brief connect and get list values from mocked result set, then store them
   * in vector v. To be checked later in unit test.
   */
  void do_something()
  {
    bool is_connected = c.connect("127.0.0.1", "root", "graduation", nullptr, 0,
      nullptr, 0);
//    cout << "c.connect=" << ( is_connected ? "true" : "false") << endl;
    int query_code = c.query("show databases");
//    cout << "c.query=" << query_code << endl;
    bool got_result = c.store_result(rs);
//    cout << "c.connect=" << ( got_result ? "true" : "false") << endl;
    if (is_connected && query_code && got_result) // avoid unused warnings
    {
    }

    char **row;
    for (;;)
    {
      row = rs.fetch_row();
      if (row == nullptr)
        break;
      v.push_back(make_pair(string(row[0]), string(row[1])));
//      cout << row[0] << "," << row[1] << endl;
    }
  }
  //--------------------------------------------------------------------------//
};
//----------------------------------------------------------------------------//
/**
 * @brief interate over a fake result set and test that it is stored in a vector
 * correctly.
 */
TEST(Example, Layer1_mock)
{
  ::mock::database_connection c;
  mock::result_set rs;
  example::mysql_user usr(c, rs);

  EXPECT_CALL(c, connect("127.0.0.1", "root", "graduation", nullptr, 0, nullptr,
    0)).Times(1).WillOnce(Return(true));
  EXPECT_CALL(c, query(_)).Times(1).WillOnce(Return(0));

  EXPECT_CALL(c, store_result(_)).Times(1);

  char const *rrr[3][2] =
  {
    { "r1c1", "r1c2"},
    { "r2c1", "r2c2"},
    { "r3c1", "r3c2"},
  };
  size_t rows = sizeof(rrr) / sizeof(rrr[0]);

  Sequence s1;
  for(size_t i = 0; i < rows; i++)
  {
    EXPECT_CALL(rs, fetch_row()).InSequence(s1)
    .WillOnce(Return(const_cast<char**>(rrr[i])));
  }

  EXPECT_CALL(rs, fetch_row()).InSequence(s1).WillRepeatedly(Return(
      static_cast<char**>(nullptr)));

  usr.do_something();
  vector<pair<string, string> > v = usr.get_data();
  EXPECT_EQ(string(rrr[0][0]), v[0].first);
  EXPECT_EQ(string(rrr[0][1]), v[0].second);
  EXPECT_EQ(string(rrr[1][0]), v[1].first);
  EXPECT_EQ(string(rrr[1][1]), v[1].second);
  EXPECT_EQ(string(rrr[2][0]), v[2].first);
  EXPECT_EQ(string(rrr[2][1]), v[2].second);
}
//----------------------------------------------------------------------------//
TEST(Example, Layer1_fake)
{
  ::mock::database_connection c;
  ::fake::result_set rs;
  rs.initialize({
    { "r1c1", "r1c2"},
    { "r2c1", "r2c2"},
    { "r3c1", "r3c2"},
    { "r4c1", "r4c2"}
  });

  example::mysql_user usr(c, rs);

  EXPECT_CALL(c, connect("127.0.0.1", "root", "graduation", nullptr, 0, nullptr,
    0)).Times(1).WillOnce(Return(true));
  EXPECT_CALL(c, query(_)).Times(1).WillOnce(Return(0));

  EXPECT_CALL(c, store_result(_)).Times(1);

  usr.do_something();
  vector<pair<string, string> > v = usr.get_data();
  // the do_something() call increments the index to it maximum value( 4 rows),
  // so, in order to extract the data and check them with v vector, we must
  // to set the initial position of rs; otherwise, the app will crash
  rs.seek(0);
  ::graduation_project::database::result::row row = rs.get_row();
  EXPECT_EQ(string(row[0]), v[0].first);
  EXPECT_EQ(string(row[1]), v[0].second);
  row = rs.get_row();
  EXPECT_EQ(string(row[0]), v[1].first);
  EXPECT_EQ(string(row[1]), v[1].second);
  row=rs.get_row();
  EXPECT_EQ(string(row[0]), v[2].first);
  EXPECT_EQ(string(row[1]), v[2].second);
}
//----------------------------------------------------------------------------//
} // namespace example
} // namespace database
} // namespace graduation_project
} // namespace test
