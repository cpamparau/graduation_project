#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "graduation_project/database/mock/database_system.hpp"
#include "graduation_project/database/mock/database_connection.hpp"
#include "graduation_project/database/mock/result_set.hpp"
#include "graduation_project/database/fake/result_set.hpp"

using namespace ::std;
using namespace ::testing;
using namespace ::graduation_project::database;

namespace test
{
namespace graduation_project
{
namespace database
{
/**
 * An example unit test
 */
namespace example
{
/**
 * @brief Example of a database user.
 */
class mysql_user_test
{
  /**
   * @brief mysql_system used to interact with database
   */
  interf::database_system &mysql_sys;

  /**
   * @brief vector to store data from DB.
   */
  vector< pair< string, string > > v;

public:

  /**
   * @brief mysql_user_test
   * @param[in] m - mysql system used to connect to db
   */
  mysql_user_test(interf::database_system &m) :
    mysql_sys(m)
  {
  }

  /**
   * @brief get_data
   * @return vector of data structured from db
   */
  vector< pair< string, string > > get_data()
  {
    return v;
  }

  /**
   * @brief run query and iterate over result using layer2.
   */
  void do_something()
  {
    database_connection_info mysql_info;
    mysql_info.host = "127.0.0.1"; //host
    mysql_info.user = "root"; //user
    mysql_info.passwd = "licenta"; //password

    {
      // get a new connection. This is never used later
      ptr_quick_connection conn = mysql_sys.get_new_connection(
        mysql_info);
      // but you could use it to run a query on it or something.
      conn.use_count(); // something to avoid warning of unused variable
    }

    interf::ptr_result_set rs = mysql_sys.query_with_result(
      mysql_info, "show databases");

//    cout << "Showing all the rows : " << endl;
    result::row r(nullptr, nullptr);
    for (;;)
    {
      r = rs->get_row();
      if (r.is_null())
        break;
//      cout << r[0] << " | " << r[1] << endl;
      v.push_back(make_pair(string(r[0]), string(r[1])));
    }
  }
  //--------------------------------------------------------------------------//
};
//----------------------------------------------------------------------------//
/**
 * @brief iterate over fake result set, using layer2 and set data in a vector.
 * Later, compare vector values with expected values.
 */
TEST(Example, Layer2_mock)
{
  mock::database_system m;
  mysql_user_test my_system(m);
  EXPECT_CALL(m, get_new_connection(_)).Times(1).WillOnce(Return(
      ptr_quick_connection(nullptr)));

  ::std::shared_ptr< mock::result_set> spm(new mock::result_set());
  EXPECT_CALL(m, query_with_result(
      Matcher<const database_connection_info&>(_),_)).Times(1).
  WillOnce(Return(spm));
  char const *rrr[3][2] =
  {
    { "Indrumator1", "Student1"},
    { "Indrumator2", "Student2"},
    { "Indrumator3", "Student3"}
  };
  size_t rows = sizeof(rrr) / sizeof(rrr[0]);
  unsigned long *columns = new unsigned long[2];
  columns[0] = 11;
  columns[1] = 8;

  Sequence s1;
  for(size_t i = 0; i < rows; i++)
  {
    ::graduation_project::database::result::row row(const_cast<char**>(rrr[i]),
      columns);
    EXPECT_CALL(*spm, get_row()).InSequence(s1)
    .WillOnce(Return(row));
  }
  EXPECT_CALL(*spm, get_row()).InSequence(s1).WillRepeatedly(Return(
      static_cast<char**>(nullptr)));
  my_system.do_something();
  vector<pair<string, string> > v = my_system.get_data();
  EXPECT_EQ("Indrumator1", v[0].first);
  EXPECT_EQ("Student1", v[0].second);
  EXPECT_EQ("Indrumator2", v[1].first);
  EXPECT_EQ("Student2", v[1].second);
  EXPECT_EQ("Indrumator3", v[2].first);
  EXPECT_EQ("Student3", v[2].second);
}
//----------------------------------------------------------------------------//
TEST(Example, Layer2_fake)
{
  ::std::shared_ptr< fake::result_set > spm(
    new fake::result_set());
  spm->initialize(
    {
      { "graduation", "Student1"},
      { "graduation", "Student2"},
      { "graduation", "Student3"}
    });

  mock::database_system m;
  mysql_user_test my_system(m);
  EXPECT_CALL(m, get_new_connection(_)).Times(1).WillOnce(Return(
      ptr_quick_connection(nullptr)));
  EXPECT_CALL(m, query_with_result(
      Matcher<const database_connection_info&>(_),_)).Times(1).
  WillOnce(Return(spm));

  my_system.do_something();
  vector<pair<string, string> > v = my_system.get_data();
  EXPECT_EQ("graduation", v[0].first);
  EXPECT_EQ("Student1", v[0].second);
  EXPECT_EQ("graduation", v[1].first);
  EXPECT_EQ("Student2", v[1].second);
  EXPECT_EQ("graduation", v[2].first);
  EXPECT_EQ("Student3", v[2].second);
}
//----------------------------------------------------------------------------//
/**
 * @brief Check row with some null cells if are empty
 */
TEST(Example, null_cell)
{
  ::std::shared_ptr< fake::result_set > spm(
    new fake::result_set());
  spm->initialize(
    {
      { NULL, "non-empty string", 0, "", nullptr}
    });

  mock::database_system m;
  EXPECT_CALL(m, query_with_result(
      Matcher<const database_connection_info&>(_),_)).Times(1).
  WillOnce(Return(spm));

  database_connection_info mysql_info;
  interf::ptr_result_set rs = m.query_with_result(
    mysql_info, "whatever query :-)");
  result::row row = rs->get_row();

  EXPECT_TRUE(row[0].is_null());
  EXPECT_FALSE(row[1].is_null());
  EXPECT_TRUE(row[2].is_null());
  EXPECT_FALSE(row[3].is_null());
  EXPECT_TRUE(row[4].is_null());
}
//----------------------------------------------------------------------------//
} // namespace example
} // namespace database
} // namespace graduation_project
} // namespace test
