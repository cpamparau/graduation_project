#include <gmock/gmock.h>
#include <graduation_project/file/fake/file.hpp>
#include "test/graduation_project/file/file_user.hpp"

using namespace ::std;
using namespace ::testing;

namespace test
{
namespace graduation_project
{
namespace fake
{
//----------------------------------------------------------------------------//
/**
 * @brief do_something test
 * Simple example of how to check that a class calls the right methods on a fake
 * object.
 */
TEST(fake_file_user, do_something)
{
  ::graduation_project::file::fake::file file;
  ::test::graduation_project::file::file_user user(file);
  user.do_something();
  ASSERT_TRUE(user.was_called);
  ASSERT_FALSE(file.is_open());
  ASSERT_EQ("first message: hello world of GTest & "
    "GMock\nsecond message: hello world of GTest & GMock\n",
    file.read_all_contents());
  ASSERT_TRUE(file.good());
}
//----------------------------------------------------------------------------//
} // namespace fake
} // namespace graduation_project
} // test
