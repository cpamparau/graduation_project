#include <string>
#include <iostream>
#include <test/graduation_project/file/file_user.hpp>

using namespace ::std;
namespace test
{
namespace graduation_project
{
namespace file
{
//----------------------------------------------------------------------------//
file_user::file_user(::graduation_project::file::interf::file& f) :
  file(f)
{
}
//----------------------------------------------------------------------------//
void file_user::do_something()
{
  file.open("just_a.txt", ios_base::out);
  string msg = "hello world of GTest & GMock";
  file << "first message: " << msg << endl;
  file << "second message: " << msg << endl;
  file.close();
  was_called = true;
}
//----------------------------------------------------------------------------//
} // namespace file
} // namespace graduation_project
} // namespace test
