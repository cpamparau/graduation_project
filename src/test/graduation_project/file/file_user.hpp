#ifndef TEST_GRADUATION_PROJECT_FILE_FILE_USER_HPP
#define TEST_GRADUATION_PROJECT_FILE_FILE_USER_HPP

#include <graduation_project/file/interf/file.hpp>

namespace test
{
namespace graduation_project
{
namespace file
{
/**
 * @brief The file_user class demonstrates how to use the interf::file in one
 * of your classes.
 */
class file_user
{
  /**
   * file used to simulate a real file (an object mock)
   */
  ::graduation_project::file::interf::file& file;

public:
  /**
   * @brief file_user Constructor. Used to create a file_user instance, and to
   * give it a file instance reference, that it should use.
   * @param f - file reference. This reference will be saved in a class member
   * and will be used later in method do_something.
   */
  file_user(::graduation_project::file::interf::file& f);

  /**
   * @brief do_something Will write a string into the file.
   */
  void do_something();

  /**
   * @brief was_called Flag that indicates if do_something method was called
   */
  bool was_called = false;
};
} // namespace file
} // namespace graduation_project
} // namespace test
#endif // TEST_GRADUATION_PROJECT_FILE_FILE_USER_HPP
