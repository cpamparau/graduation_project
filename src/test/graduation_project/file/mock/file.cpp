#include <gmock/gmock.h>
#include <graduation_project/file/mock/file.hpp>

#include "test/graduation_project/file/file_user.hpp"

using namespace ::std;
using namespace ::testing;

namespace test
{
namespace graduation_project
{
namespace mock
{
//----------------------------------------------------------------------------//
/**
 * @brief do_something test
 * Simple example of how to check that a class calls the right methods on a mock
 * object.
 */
TEST(mock_file_user, do_something)
{
  ostream mock_out_stream(cout.rdbuf());
  DefaultValue<ostream&>::Set(mock_out_stream);

  ::graduation_project::file::mock::file file;
  ::test::graduation_project::file::file_user user(file);
  EXPECT_CALL(file, out_op_const_char_ptr(_)).Times(2);// print whatever char*
  EXPECT_CALL(file, out_op_ostream_fnc_ostream(endl)).Times(2);// print 2x endl
  EXPECT_CALL(file, out_op_string("hello world of GTest & GMock")).Times(2);
  EXPECT_CALL(file, open("just_a.txt", ios_base::out)).Times(1);
  EXPECT_CALL(file, close()).Times(1);
  user.do_something();
}
//----------------------------------------------------------------------------//
} // namespace mock
} // namespace graduation_project
} // namespace test
