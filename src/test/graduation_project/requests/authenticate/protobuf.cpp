#include <gtest/gtest.h>
#include <graduation_project/requests/authenticate/protobuf.hpp>

using namespace ::testing;
using namespace ::std;

namespace test
{
namespace graduation_project
{
namespace requests
{
namespace authenticate
{
//----------------------------------------------------------------------------//
TEST(authenticate_protobuf, get_token)
{
  ::graduation_project::proto::request_msg request_msg;
  ::std::string token = "1234567890";
  ::graduation_project::proto::authenticate_request* authenticate_request =
    new ::graduation_project::proto::authenticate_request();
  // Do not delete this pointer, the destructor of request_msg will do it,
  // otherwise the program will crash.
  // Token not alocated
  {
    ::graduation_project::requests::authenticate::protobuf
      protobuf_request(request_msg);
    EXPECT_EQ("", static_cast<::std::string>(
      protobuf_request.get_token()));
  }
  request_msg.set_allocated_authenticate_request(authenticate_request);
  // Token not set
  {
    ::graduation_project::requests::authenticate::protobuf
      protobuf_request(request_msg);
    EXPECT_EQ("", static_cast<::std::string>(
      protobuf_request.get_token()));
  }
  // Token set
  {
    authenticate_request->set_token(token);
    ::graduation_project::requests::authenticate::protobuf
      protobuf_request(request_msg);
    EXPECT_EQ("1234567890", static_cast<::std::string>(
      protobuf_request.get_token()));
  }
}
//----------------------------------------------------------------------------//
TEST(authenticate_protobuf, get_id)
{
  ::graduation_project::proto::request_msg request_msg;
  ::graduation_project::request_id id = 123;
  // Id not set.
  {
    ::graduation_project::requests::authenticate::protobuf
      protobuf_request(request_msg);
    EXPECT_EQ(0, static_cast<long>(protobuf_request.get_id()));
  }
  // Id set.
  {
    request_msg.set_request_id(id);
    ::graduation_project::requests::authenticate::protobuf
      protobuf_request(request_msg);
    EXPECT_EQ(id, protobuf_request.get_id());
  }
}
//----------------------------------------------------------------------------//
TEST(authenticate_protobuf, get_type)
{
  ::graduation_project::proto::request_msg request_msg;
  ::graduation_project::requests::authenticate::protobuf
    protobuf_request(request_msg);
  EXPECT_EQ(::graduation_project::requests::AUTHENTICATION,
    protobuf_request.get_type());
}
//----------------------------------------------------------------------------//
TEST(authenticate_protobuf, to_human_readable)
{
  string request_content =
    "{\n"
    " \"requestId\": 10,\n"
    " \"authenticateRequest\": {\n"
    "  \"token\": \"1234567890\"\n"
    " }\n"
    "}\n";
  ::graduation_project::proto::request_msg request_msg;
  ::std::string token = "1234567890";
  request_msg.set_request_id(10);
  ::graduation_project::proto::authenticate_request* authenticate_request =
    new ::graduation_project::proto::authenticate_request();
  authenticate_request->set_token(token);
  request_msg.set_allocated_authenticate_request(authenticate_request);
  ::graduation_project::requests::authenticate::protobuf
    protobuf_request(request_msg);
  EXPECT_EQ(request_content, protobuf_request.to_human_readable().data());
}
//----------------------------------------------------------------------------//
} // namespace authentication
} // namespace requests
} // namespace graduation_project
} // namespace test
