#include <gtest/gtest.h>
#include <rapidxml/rapidxml.hpp>
#include <graduation_project/requests/authenticate/xml.hpp>

using namespace std;
using namespace ::testing;
using namespace graduation_project::requests::protocols;
using namespace graduation_project;

namespace test
{
namespace graduation_project
{
namespace requests
{
namespace authenticate
{
//----------------------------------------------------------------------------//
TEST(authenticate_xml, get_token)
{
  string request_content = "<request id=\"123\">\n"
    " <authenticate token=\"1234567890\"/>\n"
    "</request>\n";
  ::rapidxml::xml_document<> xml_doc;
  xml_doc.parse<0>(const_cast<char*>(&request_content.c_str()[0]));
  ::requests::authenticate::xml xml_request(*xml_doc.first_node(NODE_REQUEST));
  EXPECT_EQ("1234567890", static_cast<string>(xml_request.get_token()));
}
//----------------------------------------------------------------------------//
TEST(authenticate_xml, get_id)
{
  string request_content = "<request id=\"123\">\n"
    " <authenticate token=\"1234567890\"/>\n"
    "</request>\n";
  unsigned int id = 123;
  ::rapidxml::xml_document<> xml_doc;
  xml_doc.parse<0>(const_cast<char*>(&request_content.c_str()[0]));
  ::requests::authenticate::xml xml_request(*xml_doc.first_node(
    NODE_REQUEST));
  EXPECT_EQ(id, xml_request.get_id());
}
//----------------------------------------------------------------------------//
TEST(authenticate_xml, to_human_readable)
{
  string request_content = "<request id=\"123\">\n <authenticate"
    " token=\"1234567890\"/>\n</request>\n";
  // We create a copy of that string, because the string may be modified after
  // is parsed. We want to make sure that it wasn't modified.
  string copy_string = request_content;
  ::rapidxml::xml_document<> xml_doc;
  xml_doc.parse<::rapidxml::parse_trim_whitespace |
    ::rapidxml::parse_non_destructive>(const_cast<char*>(
      &request_content.c_str()[0]));
  ::requests::authenticate::xml xml_request(*xml_doc.first_node(
    NODE_REQUEST));
  EXPECT_EQ(copy_string, xml_request.to_human_readable());
}
//----------------------------------------------------------------------------//
TEST(authenticate_xml, get_type)
{
  string request_content = "<request id=\"123\">\n"
    " <authenticate token=\"1234567890\"/>\n"
    "</request>\n";
  ::rapidxml::xml_document<> xml_doc;
  xml_doc.parse<0>(const_cast<char*>(&request_content.c_str()[0]));
  ::requests::authenticate::xml xml_request(*xml_doc.first_node(
    NODE_REQUEST));
  EXPECT_EQ(::graduation_project::requests::AUTHENTICATION,
    xml_request.get_type());
}
//----------------------------------------------------------------------------//
} // namespace authenticate
} // namespace requests
} // namespace graduation_project
} // namespace test
