#include <gtest/gtest.h>

#include <graduation_project/requests/message_processor.hpp>
#include <graduation_project/requests/authenticate/interf/authenticate.hpp>
#include <graduation_project/requests/authenticate/xml.hpp>
#include <graduation_project/requests/authenticate/protobuf.hpp>

using namespace ::testing;
using namespace ::std;
using namespace ::graduation_project::proto;

namespace auth_interf = ::graduation_project::requests::authenticate::interf;

namespace test
{
namespace graduation_project
{
namespace requests
{
//----------------------------------------------------------------------------//
TEST(message_processor, parse_all_protocols)
{
  // We do not support this protocol
  ::graduation_project::protocol protocol = 'L';
  string request_content = "content_requests";

  ::graduation_project::requests::message_processor parser;
  EXPECT_EQ(nullptr, parser.parse(protocol, request_content));
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_no_request_node)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;

  request_content =  "<authentication token=\"1234567890\"/>\n";
  ::graduation_project::request_ptr request = parser.parse(protocol,
    request_content);
  EXPECT_EQ(request, nullptr);

}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_valid_request_without_id)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;
  request_content = "<request ";
  request_content += ">\n"
    "<authenticate ";
  request_content += "/>\n"
    "</request>\n";
  ::graduation_project::request_authenticate_ptr request = dynamic_pointer_cast<
    ::graduation_project::requests::authenticate::interf::authenticate>(
    parser.parse(protocol, request_content));
  EXPECT_NE(request, nullptr);
  EXPECT_EQ(request->get_type(),
    ::graduation_project::requests::AUTHENTICATION);
  EXPECT_EQ(request->get_id(), ::graduation_project::requests::req_id_null);
  EXPECT_EQ(request->get_token().size(), static_cast<size_t>(0));
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_unknown_request)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;

  request_content = "<request id=\"123\">\n"
    "</request>\n";
  ::graduation_project::request_ptr request = parser.parse(protocol,
    request_content);
  EXPECT_EQ(request, nullptr);
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_valid_authenticate_request)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;

  request_content = "<request id=\"";
  request_content += ::std::to_string(request_id);
  request_content += "\">\n"
    "<authenticate token=\"";
  request_content += static_cast<string>(token_basic);
  request_content += "\"/>\n"
    "</request>\n";
  ::graduation_project::request_authenticate_ptr request = dynamic_pointer_cast<
    ::graduation_project::requests::authenticate::interf::authenticate>(
    parser.parse(protocol, request_content));
  EXPECT_NE(request, nullptr);
  EXPECT_EQ(request->get_type(),
    ::graduation_project::requests::AUTHENTICATION);
  EXPECT_EQ(request->get_id(), request_id);
  EXPECT_EQ(request->get_token(), token_basic);
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_valid_authenticate_request_without_token)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;

  request_content = "<request id=\"";
  request_content += ::std::to_string(request_id);
  request_content += "\">\n"
    "<authenticate ";
  request_content += "/>\n"
    "</request>\n";
  ::graduation_project::request_authenticate_ptr request = dynamic_pointer_cast<
    ::graduation_project::requests::authenticate::interf::authenticate>(
    parser.parse(protocol, request_content));
  EXPECT_NE(request, nullptr);
  EXPECT_EQ(request->get_type(), ::graduation_project::requests::AUTHENTICATION);
  EXPECT_EQ(request->get_id(), request_id);
  EXPECT_EQ(request->get_token().size(), static_cast<size_t>(0));
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_xml_invalid_authentication_request_type)
{
  ::graduation_project::requests::message_processor parser;

  ::graduation_project::protocol protocol = 'X';
  string request_content;
  std::string token_basic = "1234567890";
  ::graduation_project::request_id request_id = 123;

  request_content = "<request id=\"123\">\n"
    "<authentication token=\"1234567890\"/>\n"
    "</request>\n";
  ::graduation_project::request_ptr request = parser.parse(protocol,
    request_content);
  EXPECT_EQ(request, nullptr);
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_protobuf_unknown_request_type)
{
  ::graduation_project::requests::message_processor parser;

  ::std::shared_ptr<::auth_interf::authenticate> result;
  request_msg protobuf_request;
  string request_content = "";

  protobuf_request.set_request_id(16);
  protobuf_request.SerializeToString(&request_content);
  EXPECT_EQ(nullptr, parser.parse('P',request_content));
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_protobuf_request_could_not_be_parsed)
{
  ::graduation_project::requests::message_processor parser;

  ::std::shared_ptr<::auth_interf::authenticate> result;
  request_msg protobuf_request;
  string request_content = "abcdef";
  EXPECT_EQ(nullptr, parser.parse('P',request_content));
}
//----------------------------------------------------------------------------//
TEST(message_processor, parse_protobuf_valid_authenticate_request)
{
  ::graduation_project::requests::message_processor parser;

  ::std::shared_ptr<::auth_interf::authenticate> result;
  request_msg protobuf_request;
  string request_content = "";

  string token = "12345678901234567890123456789012345678901234567890123456789"
    "01234567890123456789012345678";
  authenticate_request* authenticate_request = nullptr;
  authenticate_request =
    new ::graduation_project::proto::authenticate_request();
  // ^ Will be deleted by the destructor of protobuf_request
  protobuf_request.set_request_id(16);
  authenticate_request->set_token(token);
  protobuf_request.set_allocated_authenticate_request(authenticate_request);
  protobuf_request.SerializeToString(&request_content);
  result = ::std::dynamic_pointer_cast<::auth_interf::authenticate>(
    parser.parse('P', request_content));
  ASSERT_NE(nullptr, result);
  EXPECT_EQ(protobuf_request.request_id(), result->get_id());
  EXPECT_EQ(authenticate_request->token(), result->get_token());
}
//----------------------------------------------------------------------------//
} // namespace requests
} // namespace graduation_project
} // namespace test
