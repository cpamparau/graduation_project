#include <string>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "graduation_project/file/mock/file.hpp"
#include "graduation_project/file/fake/file.hpp"
#include "graduation_project/settings/settings.hpp"

using namespace ::std;
using namespace ::testing;
using namespace ::graduation_project;

namespace app
{
namespace test
{
//----------------------------------------------------------------------------//
TEST(settings_mock, load_param_file)
{
  file::mock::file file;
  settings& setting_obj = settings::get();
  string file_name="test";
  string file_content = string(
    "-mysqlpass:\"licenta\"" "\n"
    "-log:\"logs\"" "\n"
    "-work:\"build\"" "\n"
  );
  EXPECT_CALL(file, good()).Times(1).WillOnce(Return(true));
  EXPECT_CALL(file, open(file_name.c_str(),
    ::std::ios_base::in)).Times(1);
  EXPECT_CALL(file,read_all_contents()).Times(1).WillOnce(Return(file_content));
  EXPECT_CALL(file, close());
  EXPECT_EQ(setting_obj.load_param_file(file_name,file), file_content);
}
//----------------------------------------------------------------------------//
TEST(settings_fake, load_param_file)
{
  file::fake::file file;
  settings& settings_obj = settings::get();
  string file_name = "test";
  string sir = string("-mysqlpass:\"licenta\"" "\n"
        "-log:\"logs\"" "\n"
        "-work:\"build\"" "\n");
  file << sir;
  string result = settings_obj.load_param_file(file_name, file);
  ASSERT_EQ(sir, result);
}
//----------------------------------------------------------------------------//
TEST(settings_mock, save_to_file_with_content)
{
  ostream mock_out_stream(cout.rdbuf());
  DefaultValue<ostream&>::Set(mock_out_stream);

  file::mock::file file;
  settings& setting_obj=settings::get();
  setting_obj["what"]="ever";
  setting_obj["say"]="something";
  string file_name="test_with";
  string content_to_save="-say:\"something\"\n-what:\"ever\"\n";

  EXPECT_CALL(file, good()).Times(1).WillOnce(Return(true));
  EXPECT_CALL(file, open(file_name.c_str(),
    ::std::ios_base::out)).Times(1);
  EXPECT_CALL(file, out_op_string(content_to_save)).Times(1);

  EXPECT_CALL(file, close());
  setting_obj.save_to_file(file_name, file);
}
//----------------------------------------------------------------------------//
TEST(settings, parse)
{
  parameters_map parameters;
  settings::parse(
    "-mode:tes102t "
    "-gtest_filter:testareee"
    "-directory:build"
    "-another_style:234"
    "-log_directory:logs"
    , parameters);

  EXPECT_EQ("tes102t", parameters["mode"]);
  EXPECT_EQ("testareee", parameters["gtest_filter"]);
  EXPECT_EQ("logs", parameters["log_directory"]);
  EXPECT_EQ("build", parameters["directory"]);
  EXPECT_EQ("234", parameters["another_style"]);
}
//----------------------------------------------------------------------------//
TEST(settings, parse_key_value)
{
  parameters_map parameters;
  settings::parse("-thisIsTheKey:thisIsTheValue", parameters);
  EXPECT_EQ("thisIsTheValue", parameters["thisIsTheKey"]);
}
//----------------------------------------------------------------------------//
TEST(settings, parse_key_with_underscore)
{
  parameters_map parameters;
  settings::parse("-key_with_underscore:\"value\""
                  "-key:\"val\"", parameters);
  EXPECT_EQ("value", parameters["key_with_underscore"]);
  EXPECT_EQ("val", parameters["key"]);
}
//----------------------------------------------------------------------------//
TEST(settings, parse_value_with_underscore)
{
  parameters_map parameters;
  settings::parse("-key_with_underscore:value_with_underscore", parameters);
  EXPECT_EQ("value_with_underscore", parameters["key_with_underscore"]);
}
//----------------------------------------------------------------------------//
} // namespace test
}// namespace app
